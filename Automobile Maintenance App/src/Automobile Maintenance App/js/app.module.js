(function () {
  'use strict';

  angular.module('app', [
    // Angular modules 
    'ngRoute',
    'ngResource',
    'ui.bootstrap',
    "checklist-model"

      // Custom modules 

      // 3rd Party Modules

  ]);
})();