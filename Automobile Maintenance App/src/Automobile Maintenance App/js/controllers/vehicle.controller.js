(function () {
  'use strict';

  angular
    .module('app')
    .controller('vehicleController', vehicleController);

  vehicleController.$inject = ['$log', 'appService', 'vehiclesFactory']; 

  function vehicleController($log, appService, vehiclesFactory) {
    var vm = this;
    vm.title = 'Add Vehicle';
    vm.vehicles;
    var maintenanceRecords = [];
    vm.deleteVehicle = deleteVehicle;

    function activate(){
      loadVehicles();
    }

    activate();
    
    //load vehicles
    function loadVehicles()
    {
       vehiclesFactory.getVehicles(function(data){
       vm.vehicles = data;
       maintenanceRecords = [];
       for(var i = 0; i < data.length; i++)
       {
         loadMaintenanceRecord(data[i].VehicleId);
       }      
       vm.maintenanceRecords = maintenanceRecords;
       }, function (error) {
         appService.displayError("Unable to load vehicles from database");
         $log.error(error);
       });
    }

    //load maintenance records for the selected vehicle
    function loadMaintenanceRecord(vehicleId){
        vehiclesFactory.getMaintenanceRecords(vehicleId, function(recordData) {
           recordData.sort(dynamicSort("Mileage"));
           maintenanceRecords[vehicleId] = recordData;
         });
    }
    
    //delete vehicle from the database and remove from display
    function deleteVehicle(vehicleId) {
      vehiclesFactory.deleteVehicle(vehicleId, function() {
        removeVehicleFromList(vehicleId);
        appService.displaySuccess("Successfully removed vehicle from database");
      }, function (error) {
        appService.displayError("An error occured whlie removing vehicle from database.");
        $log.error(error);
      });
    }
    
    //Helper function to remove deleted vehicle from view without reloading data
    function removeVehicleFromList(vehicleId){
      for(var i = 0; i < vm.vehicles.length; i++)
        {
          if(vm.vehicles[i].VehicleId === vehicleId)
            {
              vm.vehicles.splice(i,1);
              break;
            }
        }
    }
    
    //helper function to sort records by mileage
    function dynamicSort(property) {
      var sortOrder = 1;
      if(property[0] === "-") {
          sortOrder = -1;
          property = property.substr(1);
      }
      return function (a,b) {
          var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
          return result * sortOrder;
      }
    }
  };
})();