(function () {
  'use strict';

  angular
    .module('app')
    .controller('homeController', homeController);

  homeController.$inject = ['vehiclesFactory', '$log', 'appService', 'maintenanceTasksFactory', 'maintenanceRecordsFactory']; 

  function homeController(vehiclesFactory, $log, appService, maintenanceTasksFactory, maintenanceRecordsFactory) {
    var vm = this;
    vm.title = 'vehicle';
    vm.vehicle;
    vm.vehicles;
    vm.selectedVehicleId;
    vm.showVehicle = showVehicle;
    vm.deleteMaintenanceRecord = deleteMaintenanceRecord;

    function activate() {
      loadVehicles();
    }

    activate();

    //load vehicles;
    function loadVehicles() {
      vehiclesFactory.getVehicles(function(data){
        vm.vehicles = data;
        var previousSelection = appService["home"]["selectedVehicle"];
        if (previousSelection && vehicleExists(previousSelection)) {
          angular.forEach(data, function(value) {
            if (value.VehicleId === previousSelection) {
              loadVehicle(value);
            }
          });
        } else {
          loadVehicle(data[0]);
        }
        loadMaintenanceTasks();
      }, function(error) {
        appService.displayError("Failed loading vehicles from database");
        $log.error(error);
      });
    }

    //display selected vehicle on page
    function showVehicle(id)
    {
      vehiclesFactory.getVehicle(id, function(vehicleData){
        loadVehicle(vehicleData);
        appService["home"]["selectedVehicle"] = id;
      });
    }
    
    //dropdown open status
    vm.status = {
      isopen: false
    };
    
    //load maintenance tasks
    function loadMaintenanceTasks()
    {
       maintenanceTasksFactory.getMaintenanceTasks(function(maintenanceTaskData){
         var maintananceTasks = [];
         
         for(var i = 0; i < maintenanceTaskData.length; i++)
          {
            //convert array into easily queryable array based on the maintenance task id
            maintananceTasks[maintenanceTaskData[i].MaintenanceTaskId] = maintenanceTaskData[i];               
          }
         
         //replace the current maintenance task list with the updated one
         vm.maintenanceTasks = maintananceTasks;
       }, function(error) {
         $log.error(error);
         appService.displayError("Failed to load maintenance tasks");
       });
    }
    
    //delete maintenance record
    function deleteMaintenanceRecord(id) {
      maintenanceRecordsFactory.deleteMaintenanceRecord(id, function(data) {
        loadMaintenanceRecordForVehicle(data.VehicleId);
        appService.displaySuccess("Successfully deleted maintenance record");
      }, function(error) {
        $log.error(error);
        appService.displayError("Failed to delete maintenance record for vehicle");
      });
    }
    
    //load vehicle data
    function loadVehicle(vehicle){
      vm.vehicle = vehicle;
     
      vm.selectedVehicleId = vehicle.VehicleId;
      vm.vehicleDropdownName = vm.vehicle.Name;
      loadMaintenanceRecordForVehicle(vehicle.VehicleId);
    }
    
    //load maintenance records for vehicle
    function loadMaintenanceRecordForVehicle(vehicleId)
    {
      vehiclesFactory.getMaintenanceRecords(vehicleId, function(recordData) {
        vm.maintenanceRecords = recordData;
      }, function(error) {
        $log.error(error);
        appService.displayError("Unable to load maintenance records for vehicle");
      });
    }

    //helper function to detect if vehicle exists
    function vehicleExists(id) {
      var found = false;
      angular.forEach(vm.vehicles, function(value) {
        if (value.VehicleId === id) {
          found = true;
        }
      });
      return found;
    }}
})();