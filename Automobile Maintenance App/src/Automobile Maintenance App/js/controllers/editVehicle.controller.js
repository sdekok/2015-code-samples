(function() {
  "use strict";

  angular
    .module("app")
    .controller("editVehicleController", editVehicleController);

  editVehicleController.$inject = ["$log", "$location", "$routeParams", "appService", "vehiclesFactory", "vehicleTypesFactory"];

  function editVehicleController($log, $location, $routeParams, appService, vehiclesFactory, vehicleTypesFactory) {
    /* jshint validthis:true */
    var vm = this;
    vm.title = "Edit Vehicle";

    vm.vehicle = {
      VehicleId: "",
      Name: "",
      Make: "",
      Model: "",
      Year: "",
      VehicleTypeId: ""
    };

    vm.vehicleTypeDropdownName;
    vm.vehicleTypes;

    vm.setVehicleType = setVehicleType;
    vm.submit = submit;

    //status of the dropdown menu
    vm.status = {
      isopon: false
    };

    activate();

    //setup
    function activate() {
      vm.vehicle.VehicleId = $routeParams.id;
      loadVehicleTypes();
      loadVehicle(vm.vehicle.VehicleId);
    }

    //load existing vehilce info
    function loadVehicle(id) {
      vehiclesFactory.getVehicle(id, function(data) {
        vm.vehicle = data;
        setVehicleType(vm.vehicle.VehicleTypeId);

      }, function(error) {
        appService.displayError("Failed to load vehicle.  Please try again later");
        $log.error(error);
      });
    }

    function submit(close) {
      vehiclesFactory.updateVehicle(vm.vehicle, function() {
        //display banner indicating success.  Auto hide after 5 seconds
        appService.displaySuccess("Vehicle type saved");
        if (close) {
          $location.path(appService.previousPage);
        }
      }, function(error) {
        //display banner indicating saving failed.  Hide after 30 seconds.
        appService.displayError("Could not save vehicle type.  Please try again");
        $log.error(error);
      });

    }

    //set the vehicle type when dropdown selection changed
    function setVehicleType(vehicleTypeId) {
      vm.vehicle.VehicleTypeId = vehicleTypeId;
      vm.vehicleTypeDropdownName = vm.vehicleTypes[vehicleTypeId].VehicleTypeName;
    }

    //load vehicle types
    function loadVehicleTypes() {
      vehicleTypesFactory.getVehicleTypes(function(data) {
        var vehicleTypes = {};

        //arrance as key value pairs for easy lookup
        for (var i = 0; i < data.length; i++) {
          vehicleTypes[data[i].VehicleTypeId] = data[i];
        }

        vm.vehicleTypes = vehicleTypes;
        vm.vehicleTypeDropdownName = data[0].VehicleTypeName;
        vm.vehicle.VehicleTypeId = data[0].VehicleTypeId;
      }, function(error) {
        appService.displayError("Failed to load vehicle types");
        $log.error(error);
      });
    }
  }
})();