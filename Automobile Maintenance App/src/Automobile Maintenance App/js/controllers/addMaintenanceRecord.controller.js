(function () {
  'use strict';

  angular
      .module('app')
      .controller('addMaintenanceRecordController', addMaintenanceRecordController);

  addMaintenanceRecordController.$inject = ['$location', '$routeParams', '$log', 'appService', 'vehiclesFactory', 'maintenanceRecordsFactory']; 

  function addMaintenanceRecordController($location, $routeParams, $log, appService, vehiclesFactory, maintenanceRecordsFactory) {
    /* jshint validthis:true */
    var vm = this;

    //expose primitives and objects to view
    vm.title = 'Add Maintenance Record';
    vm.record = {
      VehicleId: $routeParams.vehicleId,
      Mileage: '',
      Notes: '',
      Date: ''
   }

    vm.status = {
      isopen: false
    };

    //expose functions to view
    vm.submit = submit;
    vm.selectMaintenanceTask = selectMaintenanceTask;

    activate();

    function activate() { 
      loadMaintenanceTasks();
    }
    
    //Load valid maintenance tasks for the vehicle type
    function loadMaintenanceTasks()
    {
       vehiclesFactory.getMaintenanceTasks(vm.record.VehicleId,function(maintenanceTaskData){
         var maintananceTasks = [];
         
         $log.info(maintenanceTaskData);
         
         for(var i = 0; i < maintenanceTaskData.length; i++)
          {
            //convert array into easily queryable array based on the maintenance task id
            maintananceTasks[maintenanceTaskData[i].MaintenanceTaskId] = maintenanceTaskData[i];
                        
          }
         
         vm.record.MaintenanceTaskId = maintenanceTaskData[0].MaintenanceTaskId;
         vm.SelectedName = maintenanceTaskData[0].Name;
         
         //replace the current maintenance task list with the updated one
         vm.maintenanceTasks = maintananceTasks;
         vm.rawMaintenanceTasks = maintenanceTaskData;
         
       });
    }
    
    //handle form submission
    function submit()
    {
      
      maintenanceRecordsFactory.addMaintenanceRecord(vm.record, function () {
        //notify user of success.
        appService.displaySuccess("Successfully added maintenance record");
        $location.path(appService.previousPage);
      }, function (error) {
        //notify uer of failure.
        appService.displayError("Failed to add maintenance record.  Please try again");
        $log.error("Error:" + error);
      });
    }
    
    //handle the selection of an item from the Maintenance Task dropdown
    function selectMaintenanceTask(id)
    {
      vm.record.MaintenanceTaskId = id;
      vm.SelectedName = vm.maintenanceTasks[id].Name;
    }
  }
})();
