(function () {
  'use strict';

  angular
    .module('app')
    .controller('mainController', mainController);

  mainController.$inject = ['$rootScope', '$location', '$log', 'appService'];

  function mainController($rootScope, $location, $log, appService) {
    var vm = this;
    //contents of the menu bar
    vm.menubar = [
      { label: 'Home', route: '/' },
      { label: 'Vehicles', route: '/vehicles' },
      { label: 'Maintenance Tasks', route: '/maintenanceTasks' },
      { label: 'VehicleTypes', route: '/vehicleTypes' }
    ];

    vm.navCollapsed = true;

    //toggle menu
    vm.toggleMenu = toggleMenu;

    //Active location.  Used to enable menu bar highlighting
    vm.menuActive = '/';

    //location back button should point to.
    vm.previous = appService.previousPage;

    //list of alerts to show;
    vm.alerts = appService.messages;

    vm.closeAlert = closeAlert;

    //When the route changes, update the menu sand collapse the navigation
    $rootScope.$on('$routeChangeSuccess', function (e, curr, prev) {
      if(prev)
      {
        if (appService.previousPage !== prev.$$route.originalPath) {
          appService.previousPage = prev.$$route.originalPath;
        }
      }
      vm.menuActive = $location.path();
      vm.navCollapsed = true;
    });
    
    function toggleMenu() {
      vm.navCollapsed = !vm.navCollapsed;
    }

    //remove alert from list of alert
    function closeAlert(index) {
      appService.messages.splice(index, 1);
    }

    function activate() {
    }

    activate();
  };
})();