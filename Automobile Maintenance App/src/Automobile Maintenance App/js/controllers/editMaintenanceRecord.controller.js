﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('editMaintenanceRecordController', editMaintenanceRecordController);

    editMaintenanceRecordController.$inject = [
    '$location', '$routeParams', '$log', 'appService', 'vehiclesFactory', 'maintenanceRecordsFactory']; 

    function editMaintenanceRecordController($location, $routeParams, $log, appService, vehiclesFactory, maintenanceRecordsFactory) {
      /* jshint validthis:true */
      var vm = this;
      vm.title = 'Edit Maintenance Record';

      vm.record = {
        MaintenanceRecordId: $routeParams.id,
        VehicleId: '',
        Mileage: '',
        Notes: '',
        Date: '',
        MaintenanceTaskId: ''
      }

      vm.status = {
        isopen: false
      }

      vm.calendar = {
        isopen: false,
        dateFormat: 'dd-MMMM-yyyy',
        openCalendar: function () {
          vm.calendar.isopen = true;
        },
        dateOptions: {
          formatyear: 'yy',
          startingDay: 1
        },
        altInputFormats: ['M!/d!/yyyy']
      }

      vm.submit = submit;
      vm.selectMaintenanceTask = selectMaintenanceTask;

      activate();

      function activate() {
        //load maintenance record
        loadMaintenanceRecord();

      }

      //Load valid maintenance tasks for the vehicle type
      function loadMaintenanceTasks() {
        vehiclesFactory.getMaintenanceTasks(vm.record.VehicleId, function (maintenanceTaskData) {
          var maintananceTasks = [];

          $log.info(maintenanceTaskData);

          for (var i = 0; i < maintenanceTaskData.length; i++) {
            //convert array into easily queryable array based on the maintenance task id
            maintananceTasks[maintenanceTaskData[i].MaintenanceTaskId] = maintenanceTaskData[i];

          }

          vm.record.MaintenanceTaskId = maintenanceTaskData[0].MaintenanceTaskId;
          vm.SelectedName = maintenanceTaskData[0].Name;

          //replace the current maintenance task list with the updated one
          vm.maintenanceTasks = maintananceTasks;
          vm.rawMaintenanceTasks = maintenanceTaskData;

        });
      }

      //load maintenance record.
      function loadMaintenanceRecord() {
        maintenanceRecordsFactory.getMaintenanceRecord(vm.record.MaintenanceRecordId, function (data) {
          vm.record = data;

          //load maintenance tasks
          loadMaintenanceTasks();
        }, function(error) {
          appService.displayError("Failed to load maintenance record");
          $log.error(error);
        });
      }

      //handle the selection of an item from the Maintenance Task dropdown
      function selectMaintenanceTask(id) {
        vm.record.MaintenanceTaskId = id;
        vm.SelectedName = vm.maintenanceTasks[id].Name;
      }

      function submit(redirect) {
        maintenanceRecordsFactory.updateMaintenanceRecord(vm.record, function () {
          //notify user of success.
          appService.displaySuccess("Successfully saved changes to maintenance record");
          if (redirect) {
            $location.path(appService.previousPage);
          }
        }, function (error) {
          //notify uer of failure.
          appService.displayError("Failed to save maintenance record.  Please try again");
          $log.error("Error:" + error);
        });
      }


    }
})();
