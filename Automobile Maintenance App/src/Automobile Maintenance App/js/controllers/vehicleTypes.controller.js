(function () {
    'use strict';

    angular
        .module('app')
        .controller('vehicleTypesController', vehicleTypes);

    vehicleTypes.$inject = ['$location', '$log', 'appService', 'vehicleTypesFactory']; 

    function vehicleTypes($location, $log, appService, vehicleTypesFactory) {
        /* jshint validthis:true */
      var vm = this;
      vm.title = 'vehicleTypes';
      var maintenanceTasks = [];
      
      vm.deleteVehicleType = deleteVehicleType;

      activate();

      function activate() {
        loadVehicleTypes();
      }
      
      //load vehicle types from the databse
      function loadVehicleTypes()
      {
        vehicleTypesFactory.getVehicleTypes(function(data){
          vm.vehicleTypes = data;
          vm.MaintenanceTasks = [];
          
          for (var i = 0; i < data.length;  i++) {
            loadMaintenanceTasks(vm.vehicleTypes[i].VehicleTypeId);
          }
          
        }, function(error){
          $log.error(error);
          appService.displayError("Failed to load vehicle types from the database");
        });
      }
      
      //Load maintenance tasks for a vehicle type
      function loadMaintenanceTasks(vehicleTypeId)
      {
        vehicleTypesFactory.getMaintenanceTasks(vehicleTypeId, function(data){
         vm.MaintenanceTasks[vehicleTypeId] = data;
        }, function(error){
          $log.error(error);
          appService.displayError("Failed to load maintenance tasks for vehicle from the database");
          vm.MaintenanceTasks[vehicleTypeId] = null;
        });
      }
      
      //delete vehicle type
      function deleteVehicleType(vehicleTypeId) {
        vehicleTypesFactory.deleteVehicleType(vehicleTypeId, function (data) {
          //remove vehilce from list, so full list doesn't need to be reloaded
          for (var i = 0; i < vm.vehicleTypes.length; i++) {
            if (vm.vehicleTypes[i].VehicleTypeId === data.VehicleTypeId) {
              vm.vehicleTypes.splice(i, 1);
              break;
            }
          }
          appService.displaySuccess("Successfuly removed vehicle type from the database");
        },function(error) {
          $log.error(error);
          appService.displayError("Failed to remove vehicle type from the database");
        });
      }
    }
})();
