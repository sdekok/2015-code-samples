﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('maintenanceRecords', maintenanceRecords);

    maintenanceRecords.$inject = ['$location']; 

    function maintenanceRecords($location) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'maintenanceRecords';

        activate();

        function activate() { }
    }
})();
