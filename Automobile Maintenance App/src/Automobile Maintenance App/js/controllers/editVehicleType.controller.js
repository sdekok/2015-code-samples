(function () {
    'use strict';

    angular
        .module('app')
        .controller('editVehicleTypeController', editVehicleTypeController);

    editVehicleTypeController.$inject = ['$scope','$routeParams' ,'$location', '$log', 'appService','vehicleTypesFactory', 'maintenanceTasksFactory']; 

    function editVehicleTypeController($scope, $routeParams, $location, $log, appService, vehicleTypesFactory, maintenanceTasksFactory) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'Edit Vehicle Type';
        vm.maintenanceTasks = [];
        vm.selected = [];
        vm.vehicleType;  
      
        var vehicleTypeId;
        var originalMaintenanceTasks = [];
      
        activate();

        function activate() { 
          vehicleTypeId = $routeParams.id;
          loadAllMaintenanceTasks();
          loadVehicleType();
        }
      
      function loadVehicleType()
      {
        //load vehicle type info
        vehicleTypesFactory.getVehicleType(vehicleTypeId, function(data){
          vm.vehicleType=data;
        }, function (error) {
          appService.displayError("Failed to load vehicle type.  Please try again.");
          $log.error(error);
        });
        
        vehicleTypesFactory.getMaintenanceTasks(vehicleTypeId, function(data) {
          var maintenanceTask;
          angular.forEach(data, function(value)
          {
            vm.selected.push(value.MaintenanceTaskId);
          });
          originalMaintenanceTasks = angular.copy(vm.selected);
        }, function(error) {
          appService.displayError("Failed to load maintenance tasks for vehicle type.  Please try again later");
          $log.error(error);
        });
        
      }
      
      function loadAllMaintenanceTasks()
      {
        maintenanceTasksFactory.getMaintenanceTasks(function(data){
          vm.maintenanceTasks = data;
        },
        function(error){
          $log.error(error);
        });
      }
      
      vm.submit = function submit()
      {
        var toAdd = [];
        var toRemove = [];
        
        angular.forEach(originalMaintenanceTasks, function(value){
          if(!inArray(value, vm.selected))
            {
              toRemove.push(value);
            }
        });
        
        angular.forEach(vm.selected, function(value){
          if(!inArray(value, originalMaintenanceTasks))
            {
              toAdd.push(value);
            }
        });
        
        var success=true;
        
        vehicleTypesFactory.updateVehicleType(vm.vehicleType, function(){
          
        }, function (error) {
          appService.displayError("Failed to update vehicle type.  Please try again later...");
          $log.error(error);
          success=false;
        });
        
        
        angular.forEach(toAdd, function(value){
          vehicleTypesFactory.addMaintenanceTask(vehicleTypeId, value, function(){
            
          }, function (error) {
            appService.displayError("Failed to add maintenance task to vehicle type");
            log.error(error);
            success=false;
          });
            
        });
        
        angular.forEach(toRemove,function(value){
          vehicleTypesFactory.deleteMaintenanceTask(vehicleTypeId, value, function(data){
          }, function (error) {
            appService.displayError("Failed to remove maintenance task from vehicle type");
            log.error(error);
            success=false;
          });
        });
        
        if(success)
          {
          originalMaintenanceTasks = angular.copy(vm.selected);
          appService.displaySuccess("Changes to vehicle type successfully saved");
          $location.path(appService.previousPage);
        }
      }
      
      function inArray(needle, haystack) {
      for (var i = 0, maxi = haystack.length; i < maxi; ++i) {
        if (haystack[i] == needle) {
          return true;
        }
      }
      return false;
    }
    
    }
})();
