(function () {
    'use strict';

    angular
        .module('app')
        .controller('addVehicleController', addVehicle);

    addVehicle.$inject = ['$log', '$location', 'appService', 'vehiclesFactory', 'vehicleTypesFactory']; 

  function addVehicle($log, $location, appService, vehiclesFactory, vehicleTypesFactory)      {
    /* jshint validthis:true */
    var vm = this;
    vm.title = 'Add Vehicle';
    vm.vehicle={
      Name:'',
      Make:'',
      Model: '',
      Year: '',
      VehicleTypeId:  ''
    };
    vm.vehicleTypeDropdownName;
    vm.setVehicleType = setVehicleType;
    vm.submit = submit;
    vm.vehicleTypes;

    vm.status = {
      isopon: false
    }

    activate();

    function activate() {
      vm.oldUrl = document.referrer;
      loadVehicleTypes();
    }

    //handle submission
    function submit()
    {
      vehiclesFactory.addVehicle(vm.vehicle, function () {
        //notify user that vehicle successfully added
        appService.displaySuccess("Sucessfully added new vehicle");
        //redirect to calling page
        $location.path(appService.previousPage);
      }, function(error) {
        appService.displayError("An error occurred while adding new vehicle.  Please try again.")
        $log.error(error);
      });
    }
    
    function setVehicleType(vehicleTypeId)
    {
      vm.vehicle.VehicleTypeId = vehicleTypeId;
      vm.vehicleTypeDropdownName = vm.vehicleTypes[vehicleTypeId].VehicleTypeName;
    }
    
    function loadVehicleTypes()
    {
      vehicleTypesFactory.getVehicleTypes(function(data){
        var vehicleTypes = {};
        
        for (var i = 0; i < data.length; i++)
          {
            vehicleTypes[data[i].VehicleTypeId] = data[i];
          }
        
          vm.vehicleTypes = vehicleTypes;
          vm.vehicleTypeDropdownName = data[0].VehicleTypeName
          vm.vehicle.VehicleTypeId = data[0].VehicleTypeId;
      }, function(error) {
        appService.displayError("An error occurred while loading vehicle types.");
        $log.error(error);
      });
    }
  }
})();
