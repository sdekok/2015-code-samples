(function () {
  'use strict';

  angular
    .module('app')
    .config(config);

  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider
      .when('/',
      {
        templateUrl: 'partials/home.htm',
        controller: 'homeController',
        controllerAs: 'hc'
      })
    
    .when('/vehicles',
    {
      templateUrl: 'partials/vehicles.htm',
      controller: 'vehicleController',
      controllerAs: 'vc'
    })
    
    .when('/vehicles/edit/:id',
    {
      templateUrl: '/partials/editVehicle.htm',
      controller: 'editVehicleController',
      controllerAs: 'vc'
    })
    
    .when('/vehicles/add',
    {
      templateUrl: '/partials/addVehicle.htm',
      controller: 'addVehicleController',
      controllerAs: 'vc'
    })
    
    .when('/vehicleTypes',
    {
      templateUrl: '/partials/vehicleTypes.htm',
      controller: 'vehicleTypesController',
      controllerAs: 'vt'
    })
    
    .when('/vehicleTypes/add',{
      templateUrl: '/partials/addVehicleType.htm',
      controller: 'addVehicleTypeController',
      controllerAs: 'vt'
    })
    
    .when('/vehicleTypes/edit/:id',{
      templateUrl: '/partials/editVehicleType.htm',
      controller: 'editVehicleTypeController',
      controllerAs: 'vt'
    })
    
    .when('/maintenanceTasks',
    {
      templateUrl: '/partials/maintenanceTasks.htm',
      controller: 'maintenanceTasksController',
      controllerAs: 'tc'
    })
    
    .when('/maintenanceTasks/add/',{
      templateUrl: '/partials/addMaintenanceTask.htm',
      controller: 'addMaintenanceTaskController',
      controllerAs: 'tc'
    })
    
    .when('/maintenanceTasks/edit/:id',{
      templateUrl: '/partials/editMaintenanceTask.htm',
      controller: 'editMaintenanceTaskController',
      controllerAs: 'tc'
    })
    
    .when('/maintenanceRecords/add/:vehicleId',{
      templateUrl: '/partials/addMaintenanceRecord.htm',
      controller: 'addMaintenanceRecordController',
      controllerAs: 'mc'
    })
    
    .when('/maintenanceRecords/edit/:id',
    {
      templateUrl: '/partials/editMaintenanceRecord.htm',
      controller: 'editMaintenanceRecordController',
      controllerAs: 'mc'
    });
  }
})();
