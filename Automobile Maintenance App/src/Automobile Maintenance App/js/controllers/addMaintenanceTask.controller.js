(function () {
    'use strict';

    angular
        .module('app')
        .controller('addMaintenanceTaskController', addMaintenanceTaskController);

    addMaintenanceTaskController.$inject = ['$location', '$log', 'appService', 'maintenanceTasksFactory']; 

    function addMaintenanceTaskController($location, $log, appService, maintenanceTasksFactory) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'Add Maintenance Task';
        vm.task = {
          Name:'',
          Description:''
        }

        vm.submit = submit;
        
        activate();

        function activate() { }
      
      function submit()
      {
        $log.info("submit clicked");
        maintenanceTasksFactory.addMaintenanceTask(vm.task, function () {
          //notify user of success
          appService.displaySuccess("Successfully added new maintenance task");
          $location.path(appService.previousPage);
        }, function (error) {
          //notify user of failure
          appService.displayError("Failed to add new maintenance task.  Please try again.");
          $log.error(error);
        });
      }
    }
})();
