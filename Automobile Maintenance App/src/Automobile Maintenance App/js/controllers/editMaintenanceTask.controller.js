(function() {
  "use strict";

  angular
    .module("app")
    .controller("editMaintenanceTaskController", editMaintenanceTaskController);

  editMaintenanceTaskController.$inject = ["$location", "$log", "$routeParams", "appService", "maintenanceTasksFactory"];

  function editMaintenanceTaskController($location, $log, $routeParams, appService, maintenanceTasksFactory) {
    /* jshint validthis:true */
    var vm = this;
    vm.title = "Edit Maintenance Task";
    vm.submit = submit;
    var maintenanceTaskId;
    vm.task = {};

    activate();

    function activate() {
      maintenanceTaskId = $routeParams.id;
      loadMaintenanceTask(maintenanceTaskId);
    }


    //load maintenance task for id passed in from route params
    function loadMaintenanceTask(id) {
      maintenanceTasksFactory.getMaintenanceTask(id, function(data) {
        vm.task = data;
      }, function(error) {
        appService.displayError("Failed to load maintenance tasks");
        $log.error(error);
      });
    }

    //handle form submission.  Submit changes
    function submit() {
      maintenanceTasksFactory.updateMaintenanceTask(vm.task, function () {
        appService.displaySuccess("Changes to maintenance task successfuly saved");
        $location.path(appService.previousPage);
      }, function(error) {
        appService.displayError("Failed to save changes to maintenance task");
        $log.error(error);
      });
    }
  }
})();