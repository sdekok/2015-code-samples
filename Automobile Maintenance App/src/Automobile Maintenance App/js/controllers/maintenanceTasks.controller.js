(function () {
    'use strict';

    angular
        .module('app')
        .controller('maintenanceTasksController', maintenanceTasksController);

    maintenanceTasksController.$inject = ['$location', '$log', 'appService', 'maintenanceTasksFactory']; 

    function maintenanceTasksController($location, $log, appService, maintenanceTasksFactory) {
      /* jshint validthis:true */
      var vm = this;
      vm.title = 'Maintenance Tasks';

      vm.maintenanceTasks = {};

      activate();

      function activate() {
        loadMaintenanceTasks();
      }
      
      //load maintenance tasks
      function loadMaintenanceTasks(){
        maintenanceTasksFactory.getMaintenanceTasks(function(data){
          vm.maintenanceTasks = data;
        }, function(error){
          $log.error(error);
          appService.displayError("Failed to load maintenance tasks");
        });
      }
      
      //delete a maintenance task
      vm.deleteMaintenanceTask = function deleteMaintenanceTask(maintenanceTaskId){    
        maintenanceTasksFactory.deleteMaintenanceTask(maintenanceTaskId, function(data) {
          appService.displaySuccess("Successfully removed maintenance task");
          removeMaintenanceTaskFromCollection(data);
        }, function(error){
          $log.error(error);
          appService.displayError("Failed to remove maintenance task");
        });
      }
      
      //helper function that removes deleted maintenance task without reloading data
      function removeMaintenanceTaskFromCollection(maintenanceTask)
      {
        if(maintenanceTask == null)
          {
            return;
          }
        for(var i = 0; i < vm.maintenanceTasks.length; i++)
        {
          if(vm.maintenanceTasks[i].MaintenanceTaskId === maintenanceTask.MaintenanceTaskId)
          {
            vm.maintenanceTasks.splice(i, 1);
            break;
          }
        }
      }
      
    }
})();
