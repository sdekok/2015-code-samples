(function() {
  "use strict";

  angular
    .module("app")
    .controller("addVehicleTypeController", addVehicleTypeController);

  addVehicleTypeController.$inject = ["$scope", "$location", "$log", "appService", "vehicleTypesFactory", "maintenanceTasksFactory"];

  function addVehicleTypeController($scope, $location, $log, appService, vehicleTypesFactory, maintenanceTasksFactory) {
    /* jshint validthis:true */
    var vm = this;
    vm.title = "addVehicleType";
    vm.vehicleType = {
      VehicleTypeName: ""
    };
    vm.selected = {};

    vm.submit = submit;
    
    var maintenanceTasks = [];

    activate();

    function activate() {
      loadMaintenanceTasks();
    }
    
    //add vehicle type
    function submit() {
      vehicleTypesFactory.addVehicleType(vm.vehicleType, function(data) {
        var vehicleTypeId = data.VehicleTypeId;

        angular.forEach(maintenanceTasks, function(value) {
          vehicleTypesFactory.addMaintenanceTask(vehicleTypeId, value, function() {}, function(error) {
            //display error message
            appService.displayError("An error occurred when adding maintenance task to new vehicle type");
            $log.error(error);
          });
        });
        //go back to previous page
        $location.path(appService.previousPage);
      }, function(error) {
        appService.displayError("An error occurred when adding new vehicle type.  Please try again");
        $log.error(error);
      });
    };

    //load maintenance tasks 
    function loadMaintenanceTasks() {
      maintenanceTasksFactory.getMaintenanceTasks(function(success) {
          vm.maintenanceTasks = success;
        },
        function (error) {
          appService.displayError("Unable to load maintenance tasks.  Try again later");
          $log.error(error);
        });
    }

    $scope.$watchCollection(angular.bind(vm, function() { return vm.selected; }), function() {
      maintenanceTasks = [];
      angular.forEach(vm.selected, function(value, key) {
        if (value) {
          maintenanceTasks.push(key);
        }
      });
      vm.maintenanceTaskList = maintenanceTasks;
    });

  }
})();