﻿(function() {
  "use strict";

  angular
    .module("app")
    .directive("calendar", calendar);

  calendar.$inject = ["$window"];

  function calendar($window) {
    // Usage:
    //     <directive></directive>
    // Creates:
    // 
    var directive = {
      restrict: "E",
      templateUrl: 'templates/calendar.htm',
      replace: true,
      scope: {
        dateModel: "="
      },
      controller: function($scope) {
        $scope.calendar = {
          isopen: false,
          dateFormat: 'dd-MMMM-yyyy',
          openCalendar: function () {
            $scope.calendar.isopen = true;
          },
          dateOptions: {
            formatyear: 'yy',
            startingDay: 1
          },
          altInputFormats: ['M!/d!/yyyy']
        }
      }
    };
    return directive;
  }

})();