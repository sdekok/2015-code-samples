﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('appService', appService);

    appService.$inject = ['$log'];

    function appService($log) {
      var vm = this;
      //success timeout is 5 seconds by default
      var defaultSuccessTimeout = 5000;

      //failure timeout is 30 seconds by default
      var defaultErrorTimeout = 30000;

      //messages to show the user
      vm.messages = [];

      //state info for home page
      vm.home = {};

      //previous page.  Should be set by the mainController
      vm.previousPage = null;

      //expose functions
      vm.displayError = displayError;
      vm.displayInfo = displayInfo;
      vm.displaySuccess = displaySuccess;

      //add an error message to the messages array
      function displayError(message, timeout) {
        if (!timeout) {
          timeout = defaultErrorTimeout;
        }
        addMessage(message, timeout, 'danger');
        $log.error(message);
      }

      //add a info message to the messages array
      function displayInfo(message, timeout) {
        if (!timeout) {
          timeout = defaultSuccessTimeout;
        }

        addMessage(message, timeout, 'info');
      }

      //add a success message to the messages array
      function displaySuccess(message, timeout) {
        if (!timeout) {
          timeout = defaultSuccessTimeout;
        }
        addMessage(message, timeout, 'success');
      }

      function addMessage(message, timeout, type) {
        //if timeout set to boolean true, show without timeout
        var messageToAdd;
        if (timeout === true) {
          messageToAdd = { msg: message, type: type };
        } else {
          messageToAdd = { msg: message, timeout: timeout, type: type };
        }

        vm.messages.push(messageToAdd);
      }
    }
})();