(function () {
  'use strict';

  angular
    .module('app')
    .factory('maintenanceRecordsFactory', maintenanceRecords);

  maintenanceRecords.$inject = ['$http'];

  function maintenanceRecords($http) {

    var baseUrl = '/api/MaintenanceRecords';
    
    var service = {
      getMaintenanceRecords: getMaintenanceRecords,
      getMaintenanceRecord: getMaintenanceRecord,
      addMaintenanceRecord: addMaintenanceRecord,
      deleteMaintenanceRecord: deleteMaintenanceRecord,
      updateMaintenanceRecord: updateMaintenanceRecord
    };

    return service;
          
    function getMaintenanceRecords(successCallback, failureCallback) {
      
      $http({
        method:'GET',
        url: baseUrl
        
      }).then(function(data) { successCallback(data.data); },failureCallback);
    }

    
    function getMaintenanceRecord(maintenanceRecordId, successCallback, failureCallback) {
      $http.get(baseUrl + '/' + maintenanceRecordId).then(function(data) { successCallback(data.data) }, failureCallback);
    }

    function addMaintenanceRecord(maintenanceRecord, successCallback, failureCallback) {
      $http({
        method:'POST',
        url: baseUrl,
        data: maintenanceRecord}).then(successCallback, failureCallback);
    }

    function deleteMaintenanceRecord(maintenanceRecordId, successCallback, failureCallback) {    
      $http.delete(baseUrl + '/' + maintenanceRecordId).then(successCallback, failureCallback);
    }

    function updateMaintenanceRecord(maintenanceRecord, successCallback, failureCallback) {
      $http.put(baseUrl+ '/' + maintenanceRecord.MaintenanceRecordId, maintenanceRecord).then(successCallback, failureCallback);
    }

  }
})();