(function () {
  'use strict';

  angular
    .module('app')
    .factory('maintenanceTasksFactory', maintenanceTasks);

  maintenanceTasks.$inject = ['$resource'];

  function maintenanceTasks($resource) {
    var MaintenanceTask = $resource('api/MaintenanceTasks/:id', {}, {'update':{method:'PUT'}});

    var service = {
      getMaintenanceTasks: getMaintenanceTasks,
      getMaintenanceTask: getMaintenanceTask,
      addMaintenanceTask: addMaintenanceTask,
      deleteMaintenanceTask: deleteMaintenanceTask,
      updateMaintenanceTask: updateMaintenanceTask
    };

    return service;

    function getMaintenanceTasks(successCallback, failureCallback) {
      MaintenanceTask.query(null,successCallback,failureCallback);
    }

    function getMaintenanceTask(maintenanceTaskId, successCallback, failureCallback) {
      console.log(maintenanceTaskId);
      MaintenanceTask.get({id:maintenanceTaskId}, successCallback, failureCallback);
    }

    function addMaintenanceTask(maintenanceTask, successCallback, failureCallback) {
      var toAdd = new MaintenanceTask(maintenanceTask);
      toAdd.$save().then(successCallback, failureCallback);
    }

    function deleteMaintenanceTask(maintenanceTaskId, successCallback, failureCallback) {
      MaintenanceTask.delete({id:maintenanceTaskId}, successCallback, failureCallback);
    }

    function updateMaintenanceTask(maintenanceTask, successCallback, failureCallback) {
      MaintenanceTask.update({id: maintenanceTask.MaintenanceTaskId}, maintenanceTask, successCallback, failureCallback);
    }

  }
})();