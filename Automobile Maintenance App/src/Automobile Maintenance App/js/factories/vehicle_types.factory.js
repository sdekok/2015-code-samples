(function () {
  'use strict';

  angular
    .module('app')
    .factory('vehicleTypesFactory', vehicleTypesFactory);

  vehicleTypesFactory.$inject = ['$http'];

  function vehicleTypesFactory($http) {
    var baseUrl = '/api/VehicleTypes';
    var maintenanceTaskAppendix = '/MaintenanceTasks';

    var service = {
      getVehicleTypes: getVehicleTypes,
      getVehicleType: getVehicleType,
      addVehicleType: addVehicleType,
      deleteVehicleType: deleteVehicleType,
      updateVehicleType: updateVehicleType,
      getMaintenanceTasks: getMaintenanceTasks,
      addMaintenanceTask: addMaintenanceTask,
      deleteMaintenanceTask: deleteMaintenanceTask
    };

    return service;

    function getVehicleTypes(successCallback, failureCallback) {
      $http.get(baseUrl).then(function(data){successCallback(data.data)}, failureCallback);
    }

    function getVehicleType(vehicleTypeId, successCallback, failureCallback) {
      $http.get(baseUrl + '/' + vehicleTypeId).then(function(data){successCallback(data.data)}, failureCallback);
    }

    function addVehicleType(vehicleType, successCallback, failureCallback) {
      $http.post(baseUrl, vehicleType).then(function(data){successCallback(data.data)}, failureCallback);
    }

    function deleteVehicleType(vehicleTypeId, successCallback, failureCallback) {
      $http.delete(baseUrl + '/' + vehicleTypeId).then(function(data){successCallback(data.data)}, failureCallback);
    }

    function updateVehicleType(vehicleType, successCallback, failureCallback) {
      $http.put(baseUrl + '/' + vehicleType.VehicleTypeId, vehicleType).then(successCallback, failureCallback);
    }
    
    function getMaintenanceTasks(vehicleTypeId, successCallback, failureCallback)
    {
      $http.get(baseUrl + '/' + vehicleTypeId + maintenanceTaskAppendix).then(function(data){successCallback(data.data)}, failureCallback);
    }
    
    function addMaintenanceTask(vehicleTypeId, maintenanceTaskId, successCallback, failureCallback)
    {
      $http.post(baseUrl + '/' + vehicleTypeId + maintenanceTaskAppendix + '/' + maintenanceTaskId).then(function(data){if(successCallback != null){successCallback(data.data)}}, failureCallback);
    }
    
    function deleteMaintenanceTask(vehicleTypeId, maintenanceTaskId, successCallback, failureCallback)
    {
      $http.delete(baseUrl + '/' + vehicleTypeId + maintenanceTaskAppendix + '/' + maintenanceTaskId).then(function(data){successCallback(data.data)}, failureCallback);
    }

  }
})();