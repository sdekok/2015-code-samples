/**
 * @namespace Factories
 */
(function () {
  'use strict';

  angular
    .module('app')
    .factory('vehiclesFactory', vehiclesFactory);

  vehiclesFactory.$inject = ['$resource', '$log'];
  
  /**
   * @namespace Vehicle Factory
   * @param {} $resource 
   * @param {} $log 
   * @returns {} 
   */
  function vehiclesFactory($resource, $log) {
   
    var Vehicle = $resource('/api/Vehicles/:id', {}, {'update':{method:'PUT'}});
    var MaintenanceRecord = $resource('/api/Vehicles/:id/MaintenanceRecords/', {}, {});
    var MaintenanceTask = $resource('/api/Vehicles/:id/MaintenanceTasks/', {}, {});

    var service = {
      getVehicles: getVehicles,
      getVehicle: getVehicle,
      updateVehicle: updateVehicle,
      addVehicle: addVehicle,
      deleteVehicle: deleteVehicle,
      getMaintenanceRecords: getMaintenanceRecords,
      getMaintenanceTasks: getMaintenanceTasks
    };

    return service;

    /**
     * @name getVehicles
     * @desc  Get a array of all vehicle objects
     * @returns {array} 
     */
    function getVehicles(successCallback, failureCallback) {

      Vehicle.query(null, successCallback, failureCallback);

    }

    /**
     * @name getVehicle
     * @desc get the vehicle that matches the id provided
     * @param {number} vehicleId 
     * @returns {object} 
     */
    function getVehicle(vehicleId, successCallback, failureCallback) {
      Vehicle.get({ id: vehicleId }, successCallback, failureCallback);
    }

    /**
     * 
     * @param {object} vehicle 
     * @returns {object} 
     */
    function updateVehicle(vehicle, successCallback, failureCallback) {
      $log.info("vehicle:");
      $log.info(vehicle);
      Vehicle.update({ id: vehicle.VehicleId }, vehicle, successCallback, failureCallback);
    }

    /**
     * 
     * @param {object} vehicle 
     * @returns {object} 
     */
    function addVehicle(vehicle, successCallback, failureCallback) {
      var vehicleToSend = new Vehicle(vehicle);
      vehicleToSend.$save().then(successCallback, failureCallback);
    }

    /**
     * 
     * @param {Number} vehicleId 
     * @param {Function} successCallback 
     * @param {Function} failureCallback 
     * @returns {object} deleted vehicle
     */
    function deleteVehicle(vehicleId, successCallback, failureCallback) {
      Vehicle.delete({ id: vehicleId }, successCallback, failureCallback);
    }

    /**
     * 
     * @param {Number} vehicleId 
     * @param {Function} successCallback 
     * @param {Function} failureCallback 
     * @returns {Object} collection of maintenance records
     */
    function getMaintenanceRecords(vehicleId, successCallback, failureCallback) {
      MaintenanceRecord.query({ id: vehicleId }, successCallback, failureCallback);
    }

    /**
     * 
     * @param {Number} vehicleId 
     * @returns {Object} collection of maintenance records
     */
    function getMaintenanceTasks(vehicleId, successCallback, failureCallback) {
      MaintenanceTask.query({ id: vehicleId },successCallback, failureCallback);
    }
  }
})();