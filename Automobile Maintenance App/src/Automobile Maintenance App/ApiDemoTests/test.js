﻿(function() {
  var expect = chai.expect;
  var should = chai.should();

  //Note:  These tests deliberately change data so they may have side effects
  describe('Demo tests', function () {
    it.timeout = 10000;

    //created records
    var vehicleTypeCreated;
    var vehicleCreated;
    var maintenanceTaskCreated;
    var maintenanceRecordCreated;

    //all vehicles
    var vehicles;

    it('Should create a vehicle type', function (done) {
      this.timeout(10000);
      var vehicleTypeToCreate = {
        Name: "Test Vehicle Type 1"
      }
      $.post('/api/VehicleTypes', vehicleTypeToCreate, function(data) {
        expect(0).to.be.lessThan(data.VehicleTypeId);
        vehicleTypeCreated = data;
        done();
      });
    });

    it('Should create a vehicle', function (done) {
      this.timeout(10000);
      var vehicleToAdd = {
        Name: "Test",
        Make: "Ford",
        Model: "Focus",
        Year: 2015,
        VehicleTypeId: vehicleTypeCreated.VehicleTypeId
      }
      $.post('/api/Vehicles', vehicleToAdd, function(data) {
        expect(0).to.be.lessThan(data.VehicleId);
        vehicleCreated = data;
        done();
      });
    });

    it('Should create a maintenance task', function (done) {
      this.timeout(10000);
      var maintenanceTaskToAdd = {
        Name: "Test Task 1",
        Description: "This is a test task"
      }
      $.post('/api/MaintenanceTasks', maintenanceTaskToAdd, function(data) {
        maintenanceTaskCreated = data;
        expect(0).to.be.lessThan(maintenanceTaskCreated.MaintenanceTaskId);
        expect(maintenanceTaskToAdd.Name).to.equal(maintenanceTaskCreated.Name);
        done();
      });
    });

    it('Should associate maintenance task with vehicle type so it can be used', function (done) {
      this.timeout(10000);
      $.post('/api/VehicleTypes/' + vehicleTypeCreated.VehicleTypeId + '/MaintenanceTasks/' + maintenanceTaskCreated.MaintenanceTaskId, function () {
        done();
      });
    });

    it('Should create a maintenance record', function (done) {
      this.timeout(10000);
      var maintenanceRecordToAdd = {
        Date: "January 3, 2016",
        Notes: "Maintenance Record Description",
        MaintenanceTaskId: maintenanceTaskCreated.MaintenanceTaskId,
        VehicleId: vehicleCreated.VehicleId,
        Mileage: 124535
      }

      $.post('/api/MaintenanceRecords', maintenanceRecordToAdd, function (data) {
        maintenanceRecordCreated = data;
        expect(0).to.be.lessThan(maintenanceRecordCreated.MaintenanceRecordId);
        done();
      });
    });

    it('Should modify a vehicle type', function (done) {
      this.timeout(10000);
      vehicleTypeCreated.Name = "Updated Vehicle Type Name";

      $.ajax({
        url: '/api/VehicleTypes/' + vehicleTypeCreated.VehicleTypeId,
        type: 'PUT',
        data: vehicleTypeCreated,
        success: function() {
        done();
      }});
    });

    it('Should get all vehicles', function (done) {
      this.timeout(10000);
      $.getJSON('/api/Vehicles', null, function (data) {
        expect(1).to.be.lessThan(data.length);
        vehicles = data;
        done();
      });
    });

    it('Should get a specific vehicle', function(done) {
      this.timeout(10000);
      $.get('/api/Vehicles/' + vehicleCreated.VehicleId, function (vehicle) {
        expect(vehicleCreated.Name).to.be.equal(vehicle.Name);
        done();
      });
    });

    it('Should get related maintenance tasks for a specific vehicle', function (done) {
      this.timeout(10000);
      $.get('/api/Vehicles/' + vehicleCreated.VehicleId + '/MaintenanceTasks', function (tasks) {
        expect(0).to.be.lessThan(tasks.length);
        done();
      });
    });

    it('Should get related maintenance records for specific vehicle', function (done) {
      this.timeout(10000);
      $.get('/api/Vehicles/' + vehicleCreated.VehicleId + '/MaintenanceRecords', function (records) {
        expect(0).to.be.lessThan(records.length);
        done();
      });
    });

    it('Should get all maintenance tasks', function (done) {
      this.timeout(10000);
      $.get('/api/MaintenanceTasks', function(tasks) {
        expect(0).to.be.lessThan(tasks.length);
        done();
      });
    });

    it('Should get a specific maintenance task', function (done) {
      this.timeout(10000);
      $.get('/api/MaintenanceTasks/' + maintenanceTaskCreated.MaintenanceTaskId, function (task) {
        expect(maintenanceTaskCreated.Name).to.be.equal(task.Name);
        done();
      });
    });

    it('Should get all maintenance records', function (done) {
      this.timeout(10000);
      $.get('/api/MaintenanceRecords', function(records) {
        expect(0).to.be.lessThan(records.length);
        done();
      });
    });

    it('Should get a specific maintenance record', function (done) {
      this.timeout(10000);
      $.get('/api/MaintenanceRecords/' + maintenanceRecordCreated.MaintenanceRecordId, function(record) {
        expect(maintenanceRecordCreated.Notes).to.equal(record.Notes);
        done();
      });
    });

    it('Should get all vehicle types', function(done) {
      this.timeout(10000);
      $.get('/api/VehicleTypes', function(types) {
        expect(0).to.be.lessThan(types.length);
        done();
      });
    });

    it('Should get a specific vehicle type', function(done) {
      this.timeout(10000);
      $.get('/api/VehicleTypes/' + vehicleTypeCreated.VehicleTypeId, function(type) {
        expect(vehicleTypeCreated.Name).to.be.equal(type.Name);
        done();
      });
    });

    it('Should get valid maintenance tasks for a specific vehicle type', function(done) {
      this.timeout(10000);
      $.get('/api/VehicleTypes/' + vehicleTypeCreated.VehicleTypeId + '/MaintenanceTasks', function(maintenanceTasks) {
        expect(0).to.be.lessThan(maintenanceTasks.length);
        done();
      });
    });

    it('Should delete maintenance record', function(done) {
      this.timeout(10000);
      $.ajax({
        url: '/api/MaintenanceRecords/' + maintenanceRecordCreated.MaintenanceRecordId,
        type: 'DELETE',
        success: function() {
          done();
        }
      });
    });

    it('Should remove maintenance task from list of valid maintenanced task for vehicle type', function(done) {
      this.timeout(10000);
      $.ajax({
        url: '/api/VehicleTypes/' + vehicleTypeCreated.VehicleTypeId + '/MaintenanceTasks/' + maintenanceTaskCreated.MaintenanceTaskId,
        type: 'DELETE',
        success: function () {
          done();
        }
      });
    });

    it('Should delete vehicle, and if not already deleted, associated maintenance records', function(done) {
      this.timeout(10000);
      $.ajax({
        url: '/api/Vehicles/' + vehicleCreated.VehicleId,
        type: 'DELETE',
        success: function() {
          done();
        }
      });
    });

    it('Should delete vehicletype and, if not already deleted, associated vehicles and maintenance records', function (done) {
      this.timeout(10000);
      $.ajax({
        url: '/api/VehicleTypes/' + vehicleTypeCreated.VehicleTypeId,
        type: 'DELETE',
        success: function() {
          done();
        }
      });
    });

    it('Should delete maintenance task', function (done) {
      this.timeout(10000);
      $.ajax({
        url: '/api/MaintenanceTasks/' + maintenanceTaskCreated.MaintenanceTaskId,
        type: 'DELETE',
        success: function() {
          done();
        }
      });
    });
  });

})();


