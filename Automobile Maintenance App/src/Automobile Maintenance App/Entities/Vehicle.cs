﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Vehicle.cs" company="Stephen de Kok">
//   Stephen de Kok
// </copyright>
// <summary>
//   Vehicle Specific Information
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Entities
{
  using System.Collections.Generic;
  using System.ComponentModel.DataAnnotations;

  /// <summary>
  /// Vehicle Specific Information
  /// </summary>
  public class Vehicle
  {
    #region Properties

    /// <summary>
    /// Gets or sets the <see cref="MaintenanceRecords"/>
    /// </summary>
    public ICollection<MaintenanceRecord> MaintenanceRecords { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Make"/> of vehicle
    /// </summary>
    public string Make { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Model"/> of vehicle
    /// </summary>
    public string Model { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Name"/> to identify vehicle
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the vehicle Id
    /// </summary>
    [Key]
    public int VehicleId { get; set; }

    /// <summary>
    /// Gets or sets the Engine Type Navigation Property
    /// </summary>
    public virtual VehicleType VehicleType { get; set; }

    /// <summary>
    /// Gets or sets the Engine Type Id
    /// </summary>
    public int VehicleTypeId { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Year"/> vehicle was built
    /// </summary>
    public int Year { get; set; }

    #endregion Properties
  }
}