﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceTask.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   A Maintenance <c>Task</c>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Entities
{
  using System.Collections.Generic;

  /// <summary>
  /// A Maintenance <c>Task</c>
  /// </summary>
  public class MaintenanceTask
  {
    #region Properties

    /// <summary>
    /// Gets or sets the description of the maintenance to be performed
    /// </summary>
    public string MaintenanceDescription { get; set; }

    /// <summary>
    /// Gets or sets the name of the maintenance to be performed
    /// </summary>
    public string MaintenanceName { get; set; }

    /// <summary>
    /// Gets or sets the maintenance id. Must be unique. Used to reduce performance hit for some database queries.
    /// </summary>
    public int MaintenanceTaskId { get; set; }

    /// <summary>
    /// Gets or sets the maintenance tasks related to this vehicle type.
    /// </summary>
    public ICollection<VehicleType> VehicleTypes { get; set; }

    #endregion Properties
  }
}