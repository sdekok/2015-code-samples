﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceRecord.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Maintenance Record for a vehicle
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Entities
{
  using System;

  /// <summary>
  /// Maintenance Record for a vehicle
  /// </summary>
  public class MaintenanceRecord
  {
    #region Properties

    /// <summary>
    /// Gets or sets the <see cref="Date"/> of maintenance
    /// </summary>
    public DateTime Date { get; set; }

    /// <summary>
    /// Gets or sets the Maintenance Record Id
    /// </summary>
    public int MaintenanceRecordId { get; set; }

    /// <summary>
    /// Gets or sets the maintenance task that was performed
    /// </summary>
    public MaintenanceTask MaintenanceTask { get; set; }

    /// <summary>
    /// Gets or sets the id of the maintenance task performed
    /// </summary>
    public virtual int MaintenanceTaskId { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Mileage"/> at time of maintenance
    /// </summary>
    public int Mileage { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Notes"/> about maintenance event
    /// </summary>
    public string Notes { get; set; }

    /// <summary>
    /// Gets or sets the vehicle maintenance was performed on
    /// </summary>
    public virtual Vehicle Vehicle { get; set; }

    /// <summary>
    /// Gets or sets the id of the vehicle maintenance record is for
    /// </summary>
    public virtual int VehicleId { get; set; }

    #endregion Properties
  }
}