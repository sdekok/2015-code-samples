﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleType.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Car <c>Type</c> . This is present to avoid duplication of car types, making it easier to
//   extend the application in the future if necessary (ie, by adding engine model, fluid amounts,
//   etc, that are beyond the scope of this project).
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Entities
{
  using System.Collections.Generic;

  /// <summary>
  /// Car <c>Type</c> . This is present to avoid duplication of car types, making it easier to
  /// extend the application in the future if necessary (for example, by adding engine model, fluid amounts,
  /// etc, that are beyond the scope of this project).
  /// </summary>
  public class VehicleType
  {
    #region Properties

    /// <summary>
    /// Gets or sets the maintenance tasks related to this vehicle type.
    /// </summary>
    public ICollection<MaintenanceTask> MaintenanceTasks { get; set; }

    /// <summary>
    /// Gets or sets the unique engine identifier. Auto incremented key.
    /// </summary>
    public int VehicleTypeId { get; set; }

    /// <summary>
    /// Gets or sets the name of engine type. This is a unique key, but that cannot be specified here until EF 7.
    /// Unique constraint set in Migrations.Configurations.cs
    /// </summary>
    public string VehicleTypeName { get; set; }

    #endregion Properties
  }
}