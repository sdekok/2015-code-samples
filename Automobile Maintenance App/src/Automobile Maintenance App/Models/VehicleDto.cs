﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleDto.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   This is the data transfer object for Vehicles
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Models
{
  using System.ComponentModel.DataAnnotations;
  using System.Globalization;

  using AutoMapper;

  using AutomobileMaintenance.Entities;

  /// <summary>
  /// This is the data transfer object for Vehicles
  /// </summary>
  public class VehicleDto
  {
    #region Properties

    /// <summary>
    /// Gets or sets the <see cref="Make"/> of vehicle
    /// </summary>
    [Required]
    public string Make { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Model"/> of vehicle
    /// </summary>
    [Required]
    public string Model { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Name"/> to identify vehicle
    /// </summary>
    [Required]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the Vehicle Id
    /// </summary>
    public int? VehicleId { get; set; }

    /// <summary>
    /// Gets or sets the Engine Type Id
    /// </summary>
    [Required]
    public int VehicleTypeId { get; set; }

    /// <summary>
    /// Gets or sets the Name
    /// </summary>
    public string VehicleTypeName { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Year"/> vehicle was built
    /// </summary>
    [Required]
    public int Year { get; set; }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Overload of not equals <see langword="operator"/> for vehicle data transfer objects
    /// </summary>
    /// <param name="left">data transfer <c>object</c> to the <paramref name="left"/> of the <see langword="operator"/></param>
    /// <param name="right">data transfer <c>object</c> to the <paramref name="right"/> of the <see langword="operator"/></param>
    /// <returns>true if the objects are not equal, false otherwise</returns>
    public static bool operator !=(VehicleDto left, VehicleDto right)
    {
      return !(left == right);
    }

    /// <summary>
    /// Overload of equals <see langword="operator"/> for vehicle data transfer objects
    /// </summary>
    /// <param name="left">data transfer <c>object</c> to the <paramref name="left"/> of the <see langword="operator"/></param>
    /// <param name="right">data transfer <c>object</c> to the <paramref name="right"/> of the <see langword="operator"/></param>
    /// <returns>true if the objects are equal, false otherwise</returns>
    public static bool operator ==(VehicleDto left, VehicleDto right)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(left, right))
      {
        return true;
      }

      // If both are not null, but left is, return false
      return !ReferenceEquals(left, null) && left.Equals(right);
    }

    /// <summary>
    /// Checks if <see langword="this"/> <see langword="object"/> is equal to another object
    /// </summary>
    /// <param name="obj">object to compare</param>
    /// <returns>true if objects are equal, false otherwise</returns>
    public override bool Equals(object obj)
    {
      if (obj == null)
      {
        return false;
      }

      var maintenanceTask = obj as VehicleDto;

      return this.Equals(maintenanceTask);
    }

    /// <summary>
    /// Checks if this item is equal to the <paramref name="other"/> MaintenanceTask Object
    /// </summary>
    /// <param name="other">Other Maintenance Task</param>
    /// <returns><see langword="true"/> if equal, <see langword="false"/> otherwise</returns>
    public bool Equals(VehicleDto other)
    {
      if (other == null)
      {
        return false;
      }

      return other.VehicleId == this.VehicleId && other.Make == this.Make && other.Model == this.Model
             && other.Name == this.Name && other.Year == this.Year && other.VehicleTypeId == this.VehicleTypeId;
    }

    /// <summary>
    /// Get Unique Hash Code
    /// </summary>
    /// <returns>Hash Code</returns>
    public override int GetHashCode()
    {
      return this.Name.GetHashCode() ^ this.Make.GetHashCode() ^ this.Model.GetHashCode() ^ this.Year.GetHashCode();
    }

    /// <summary>
    /// Get vehicle entity for this DTO
    /// </summary>
    /// <returns>Vehicle entity</returns>
    public Vehicle GetVehicle()
    {
      return Mapper.Map<Vehicle>(this);
    }

    /// <summary>
    /// Get string of contents
    /// </summary>
    /// <returns>formatted string of contents</returns>
    public override string ToString()
    {
      return string.Format(
        CultureInfo.InvariantCulture,
        "Name: {0}, Make: {1}, Model: {2}, Year: {3}, VehicleTypeID {4}, VehicleID {5}",
        this.Name,
        this.Make,
        this.Model,
        this.Year,
        this.VehicleTypeId,
        this.VehicleId);
    }

    #endregion Methods
  }
}