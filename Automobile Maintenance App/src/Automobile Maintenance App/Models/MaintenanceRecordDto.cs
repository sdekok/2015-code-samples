﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceRecordDto.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Maintenance Record data transfer object
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Models
{
  using System;
  using System.ComponentModel.DataAnnotations;
  using System.Globalization;

  /// <summary>
  /// Maintenance Record data transfer object
  /// </summary>
  public class MaintenanceRecordDto
  {
    #region Properties

    /// <summary>
    /// Gets or sets the <see cref="Date"/> of maintenance
    /// </summary>
    [Required]
    public DateTime Date { get; set; }

    /// <summary>
    /// Gets or sets the Maintenance Record Id
    /// </summary>
    public int? MaintenanceRecordId { get; set; }

    /// <summary>
    /// Gets or sets the Id of the maintenance task performed
    /// </summary>
    [Required]
    public virtual int MaintenanceTaskId { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Mileage"/> at time of maintenance
    /// </summary>
    [Required]
    public int Mileage { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Notes"/> about maintenance event
    /// </summary>
    public string Notes { get; set; }

    /// <summary>
    /// Gets or sets the id of the vehicle maintenance record is for
    /// </summary>
    [Required]
    public virtual int VehicleId { get; set; }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Not Equal Operator
    /// </summary>
    /// <param name="left">Maintenance record to the left side of the operator</param>
    /// <param name="right">Maintenance record to the right of the operator</param>
    /// <returns><see langword="true"/> if maintenance records are not equal, false otherwise</returns>
    public static bool operator !=(MaintenanceRecordDto left, MaintenanceRecordDto right)
    {
      return !(left == right);
    }

    /// <summary>
    /// Equal <see langword="operator"/>
    /// </summary>
    /// <param name="left">Maintenance record to the left side of the operator</param>
    /// <param name="right">Maintenance record to the right of the operator</param>
    /// <returns>true if the maintenance records are equal, false otherwise</returns>
    public static bool operator ==(MaintenanceRecordDto left, MaintenanceRecordDto right)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(left, right))
      {
        return true;
      }

      // If both are not null, but left is, return false
      return !ReferenceEquals(left, null) && left.Equals(right);
    }

    /// <summary>
    /// Checks if this object is equal to another object
    /// </summary>
    /// <param name="obj">object to compare</param>
    /// <returns><see langword="true"/> if this object and object to compare match, false otherwise</returns>
    public override bool Equals(object obj)
    {
      if (obj == null)
      {
        return false;
      }

      var maintenanceTask = obj as MaintenanceRecordDto;

      return this.Equals(maintenanceTask);
    }

    /// <summary>
    /// Checks if this item is equal to the <paramref name="other"/> MaintenanceTask Object
    /// </summary>
    /// <param name="other">Other Maintenance Task</param>
    /// <returns><see langword="true"/> if equal, <see langword="false"/> otherwise</returns>
    public bool Equals(MaintenanceRecordDto other)
    {
      if (other == null)
      {
        return false;
      }

      return other.MaintenanceRecordId == this.MaintenanceRecordId && other.MaintenanceTaskId == this.MaintenanceTaskId
             && other.VehicleId == this.VehicleId && other.Date == this.Date && other.Mileage == this.Mileage
             && other.Notes == this.Notes;
    }

    /// <summary>
    /// Get Unique Hash Code
    /// </summary>
    /// <returns>Hash Code</returns>
    public override int GetHashCode()
    {
      return this.Date.GetHashCode() ^ this.Mileage.GetHashCode() ^ this.VehicleId.GetHashCode()
             ^ this.MaintenanceTaskId.GetHashCode();
    }

    /// <summary>
    /// Get string of contents
    /// </summary>
    /// <returns>formatted string of contents</returns>
    public override string ToString()
    {
      return string.Format(
        CultureInfo.InvariantCulture,
        "Date: {0}, Mileage: {1}, VehicleId: {2}, MaintenanceId: {3}, Id: {4}, Notes: {5}",
        this.Date,
        this.Mileage,
        this.VehicleId,
        this.MaintenanceTaskId,
        this.MaintenanceRecordId,
        this.Notes);
    }

    #endregion Methods
  }
}