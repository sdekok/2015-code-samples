﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceTaskDto.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the MaintenanceTaskDto type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Models
{
  using System.ComponentModel.DataAnnotations;
  using System.Globalization;

  /// <summary>
  /// The maintenance task data transfer object.
  /// </summary>
  public class MaintenanceTaskDto
  {
    #region Properties

    /// <summary>
    /// Gets or sets the description of the maintenance to be performed
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Gets or sets the name of the maintenance to be performed
    /// </summary>
    [Required]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the maintenance Id. Must be unique. Used to reduce performance hit for some database queries.
    /// </summary>
    public int? MaintenanceTaskId { get; set; }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Check if maintenance tasks are not equal
    /// </summary>
    /// <param name="left">data transfer <c>object</c> to the <paramref name="left"/> of the <see langword="operator"/></param>
    /// <param name="right">data transfer <c>object</c> to the <paramref name="right"/> of the <see langword="operator"/></param>
    /// <returns>true if the objects are not equal, false otherwise</returns>
    public static bool operator !=(MaintenanceTaskDto left, MaintenanceTaskDto right)
    {
      return !(left == right);
    }

    /// <summary>
    /// Check if maintenance tasks are equal using <see langword="operator"/> overload
    /// </summary>
    /// <param name="left">data transfer <c>object</c> to the <paramref name="left"/> of the <see langword="operator"/></param>
    /// <param name="right">data transfer <c>object</c> to the <paramref name="right"/> of the <see langword="operator"/></param>
    /// <returns>true if maintenance tasks are equal, false otherwise</returns>
    public static bool operator ==(MaintenanceTaskDto left, MaintenanceTaskDto right)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(left, right))
      {
        return true;
      }

      // If both are not null, but left is, return false
      return !ReferenceEquals(left, null) && left.Equals(right);
    }

    /// <summary>
    /// Checks if <see langword="this"/> <see langword="object"/> is equal to another object
    /// </summary>
    /// <param name="obj">object to compare</param>
    /// <returns>true if objects are equal, false otherwise</returns>
    public override bool Equals(object obj)
    {
      if (obj == null)
      {
        return false;
      }

      var maintenanceTask = obj as MaintenanceTaskDto;

      return this.Equals(maintenanceTask);
    }

    /// <summary>
    /// Checks if this item is equal to the <paramref name="other"/> MaintenanceTask Object
    /// </summary>
    /// <param name="other">Other Maintenance Task</param>
    /// <returns><see langword="true"/> if equal, <see langword="false"/> otherwise</returns>
    public bool Equals(MaintenanceTaskDto other)
    {
      if (other == null)
      {
        return false;
      }

      return other.MaintenanceTaskId == this.MaintenanceTaskId
             && other.Description == this.Description
             && other.Name == this.Name;
    }

    /// <summary>
    /// Get Unique Hash Code
    /// </summary>
    /// <returns>Hash Code</returns>
    public override int GetHashCode()
    {
      return this.Name.GetHashCode() ^ this.Description.GetHashCode();
    }

    /// <summary>
    /// Get string of contents
    /// </summary>
    /// <returns>formatted string of contents</returns>
    public override string ToString()
    {
      return string.Format(
        CultureInfo.InvariantCulture,
        "Name: {0}, Description: {1}, Id: {2}",
        this.Name,
        this.Description,
        this.MaintenanceTaskId);
    }

    #endregion Methods
  }
}