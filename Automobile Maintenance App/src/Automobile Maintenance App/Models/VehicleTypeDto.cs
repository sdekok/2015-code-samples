﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleTypeDto.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the VehicleTypeDto type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Models
{
  using System.ComponentModel.DataAnnotations;
  using System.Globalization;

  using AutoMapper;

  using AutomobileMaintenance.Entities;

  /// <summary>
  /// The vehicle type data transfer object.
  /// </summary>
  public class VehicleTypeDto
  {
    #region Properties

    /// <summary>
    /// Gets or sets the unique engine identifier. Auto incremented key.
    /// </summary>
    public int? VehicleTypeId { get; set; }

    /// <summary>
    /// Gets or sets the name of engine type. This is a unique value, but that cannot be specified here until EF 7.
    /// Unique constraint set in Migrations.Configurations.cs
    /// </summary>
    [Required]
    public string Name { get; set; }

    #endregion Properties

    #region Methods

    /// <summary>
    /// not equals <see langword="operator"/> overload for vehicle type data transfer object
    /// </summary>
    /// <param name="left">data transfer <see langword="object"/> to the <paramref name="left"/> of the <see langword="operator"/></param>
    /// <param name="right">data transfer <c>object</c> to the <paramref name="right"/> of the <see langword="operator"/></param>
    /// <returns>true if the objects are not equal, false otherwise</returns>
    public static bool operator !=(VehicleTypeDto left, VehicleTypeDto right)
    {
      return !(left == right);
    }

    /// <summary>
    /// equal <see langword="operator"/> overload for vehicle data transfer object
    /// </summary>
    /// <param name="left">data transfer <see langword="object"/> to the <paramref name="left"/> of the <see langword="operator"/></param>
    /// <param name="right">data transfer <c>object</c> to the <paramref name="right"/> of the <see langword="operator"/></param>
    /// <returns>true if the objects are equal, false otherwise</returns>
    public static bool operator ==(VehicleTypeDto left, VehicleTypeDto right)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(left, right))
      {
        return true;
      }

      // If both are not null, but left is, return false
      return !ReferenceEquals(left, null) && left.Equals(right);
    }

    /// <summary>
    /// Checks if <see langword="this"/> <see langword="object"/> is equal to another object
    /// </summary>
    /// <param name="obj">object to compare to</param>
    /// <returns>true if objects match, false otherwise</returns>
    public override bool Equals(object obj)
    {
      if (obj == null)
      {
        return false;
      }

      var maintenanceTask = obj as VehicleTypeDto;

      return this.Equals(maintenanceTask);
    }

    /// <summary>
    /// Checks if this item is equal to the <paramref name="other"/><see cref="VehicleTypeDto"/> Object
    /// </summary>
    /// <param name="other">Other Vehicle Type</param>
    /// <returns><see langword="true"/> if equal, <see langword="false"/> otherwise</returns>
    public bool Equals(VehicleTypeDto other)
    {
      if (other == null)
      {
        return false;
      }

      return other.VehicleTypeId == this.VehicleTypeId && other.Name == this.Name;
    }

    /// <summary>
    /// Get Unique Hash Code
    /// </summary>
    /// <returns>Hash Code</returns>
    public override int GetHashCode()
    {
      return this.Name.GetHashCode();
    }

    /// <summary>
    /// Get VehicleType for VehicleTypeDTO
    /// </summary>
    /// <returns>VehicleType object that matches the vehicle type data transfer object</returns>
    public VehicleType GetVehicleType()
    {
      return Mapper.Map<VehicleType>(this);
    }

    /// <summary>
    /// Get string of contents
    /// </summary>
    /// <returns>formatted string of contents</returns>
    public override string ToString()
    {
      return string.Format(
        CultureInfo.InvariantCulture,
        "Name: {0}, Id: {1}",
        this.Name,
        this.VehicleTypeId);
    }

    #endregion Methods
  }
}