﻿namespace AutomobileMaintenance.DAL
{
  using AutoMapper;
  using AutomobileMaintenance.Entities;
  using AutomobileMaintenance.Models;
  using System.Collections.Generic;
  using System.Data.Entity.Migrations;
  using System.Linq;

  /// <summary>
  /// Maintenance Records Repository
  /// </summary>
  public class MaintenanceRecordsRepository : IMaintenanceRecordsRepository
  {
    #region Fields

    /// <summary>
    /// Vehicle Maintenance DbContext instance
    /// </summary>
    private readonly VehicleMaintenanceContext vehicleMaintenanceContext;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Constructor for dependency injection
    /// </summary>
    /// <param name="vehicleMaintenanceContext">vehicle Maintenance Context to use</param>
    public MaintenanceRecordsRepository(VehicleMaintenanceContext vehicleMaintenanceContext)
    {
      this.vehicleMaintenanceContext = vehicleMaintenanceContext;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// Add a maintenance record to the database
    /// </summary>
    /// <param name="entity">maintenance record to add</param>
    public void Add(MaintenanceRecordDto entity)
    {
      var maintenanceRecord = Mapper.Map<MaintenanceRecord>(entity);
      this.vehicleMaintenanceContext.MaintenanceRecords.Add(maintenanceRecord);
    }

    /// <summary>
    /// Delete a maintenance record from the database
    /// </summary>
    /// <param name="id">ID of the maintenance record to delete</param>
    public void Delete(int id)
    {
      if (this.vehicleMaintenanceContext.MaintenanceRecords.Any(x => x.MaintenanceRecordId == id))
      {
        var toDelete = this.vehicleMaintenanceContext.MaintenanceRecords.First(x => x.MaintenanceRecordId == id);
        this.vehicleMaintenanceContext.MaintenanceRecords.Remove(toDelete);
      }
    }

    /// <summary>
    /// <see cref="Get"/> the id for a maintenance record
    /// </summary>
    /// <param name="toFind">Maintenance Record Dto to Find</param>
    /// <returns>id if found, <see langword="null"/> otherwise</returns>
    public int? FindId(MaintenanceRecordDto toFind)
    {
      var id =
        this.vehicleMaintenanceContext.MaintenanceRecords?.FirstOrDefault(
          x =>
          x.MaintenanceTaskId == toFind.MaintenanceTaskId && x.Date == toFind.Date && x.Mileage == toFind.Mileage
          && x.VehicleId == toFind.VehicleId)?.MaintenanceRecordId;

      return id;
    }

    /// <summary>
    /// Get a maintenance record
    /// </summary>
    /// <param name="id">the ID of the maintenance record to retrieve</param>
    /// <returns>Maintenance record if it exists, null otherwise</returns>
    public MaintenanceRecordDto Get(int id)
    {
      var maintenanceRecord = this.vehicleMaintenanceContext.MaintenanceRecords?.FirstOrDefault(
        v => v.MaintenanceRecordId == id);
      return Mapper.Map<MaintenanceRecordDto>(maintenanceRecord);
    }

    /// <summary>
    /// Get all maintenance records
    /// </summary>
    /// <returns>collection of all maintenance records</returns>
    public IEnumerable<MaintenanceRecordDto> GetAll()
    {
      return Mapper.Map<IEnumerable<MaintenanceRecordDto>>(this.vehicleMaintenanceContext.MaintenanceRecords);
    }

    /// <summary>
    /// Update a maintenance record
    /// </summary>
    /// <param name="entity">Updated maintenance record</param>
    public void Update(MaintenanceRecordDto entity)
    {
      var maintenanceRecord = Mapper.Map<MaintenanceRecord>(entity);
      this.vehicleMaintenanceContext.MaintenanceRecords.AddOrUpdate(maintenanceRecord);
    }

    #endregion Methods
  }
}