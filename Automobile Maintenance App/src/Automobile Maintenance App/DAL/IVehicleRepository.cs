﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IVehicleRepository.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Vehicle Repository Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.DAL
{
  using System.Collections.Generic;

  using AutomobileMaintenance.Models;

  /// <summary>
  /// Vehicle Repository Interface
  /// </summary>
  public interface IVehicleRepository : IRepository<VehicleDto, int>
  {
    #region Methods

    /// <summary>
    /// Find the id of the vehicle
    /// </summary>
    /// <param name="toFind">Vehicle to find</param>
    /// <returns>vehicle id if it is in the database, <see langword="null"/> otherwise</returns>
    int? FindId(VehicleDto toFind);

    /// <summary>
    /// Get maintenance records for the specified <paramref name="id"/>
    /// </summary>
    /// <param name="id">Id of vehicle</param>
    /// <returns>collection of MaintenanceRecords</returns>
    IEnumerable<MaintenanceRecordDto> GetMaintenanceRecords(int id);

    #endregion Methods
  }
}