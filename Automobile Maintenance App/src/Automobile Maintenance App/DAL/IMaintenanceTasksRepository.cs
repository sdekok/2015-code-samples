﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMaintenanceTasksRepository.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the IMaintenanceTasksRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.DAL
{
  using AutomobileMaintenance.Models;

  /// <summary>
  /// The <see cref="MaintenanceTasksRepository"/> interface.
  /// </summary>
  public interface IMaintenanceTasksRepository : IRepository<MaintenanceTaskDto, int>
  {
    #region Methods

    /// <summary>
    /// Find the id of the vehicle
    /// </summary>
    /// <param name="toFind">Vehicle to find</param>
    /// <returns>vehicle id if it is in the database, <see langword="null"/> otherwise</returns>
    int? FindId(MaintenanceTaskDto toFind);

    #endregion Methods
  }
}