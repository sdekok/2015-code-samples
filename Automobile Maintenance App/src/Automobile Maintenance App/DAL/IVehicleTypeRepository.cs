﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IVehicleTypeRepository.cs" company="Stephen de Kok">
//   Copyright 2015 Stephen de Kok
// </copyright>
// <summary>
//   Repository Interface for accessing vehicle types
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.DAL
{
  using System.Collections.Generic;

  using AutomobileMaintenance.Models;

  /// <summary>
  /// Repository Interface for accessing vehicle types
  /// </summary>
  public interface IVehicleTypeRepository : IRepository<VehicleTypeDto, int>
  {
    #region Methods

    /// <summary>
    /// Add a maintenance task to the list of valid tasks for the specified vehicle type
    /// </summary>
    /// <param name="id">Id of vehicle type</param>
    /// <param name="maintenanceTaskId">Maintenance Task Id</param>
    void AddMaintenanceTask(int id, int maintenanceTaskId);

    /// <summary>
    /// Find the id of the vehicle
    /// </summary>
    /// <param name="toFind">Vehicle to find</param>
    /// <returns>vehicle id if it is in the database, <see langword="null"/> otherwise</returns>
    int? FindId(VehicleTypeDto toFind);

    /// <summary>
    /// Gets a list of all valid maintenance tasks
    /// </summary>
    /// <param name="id">Vehicle Type Id</param>
    /// <returns>Valid Maintenance Tasks for Vehicle Type</returns>
    ICollection<MaintenanceTaskDto> GetAllMaintenanceTasks(int id);

    /// <summary>
    /// Remove the MaintenanceTask from list of valid tasks for specified vehicle type
    /// </summary>
    /// <param name="id">Id of vehicle type</param>
    /// <param name="maintenanceTaskId">Maintenance Task Id</param>
    void RemoveMaintenanceTask(int id, int maintenanceTaskId);

    #endregion Methods
  }
}