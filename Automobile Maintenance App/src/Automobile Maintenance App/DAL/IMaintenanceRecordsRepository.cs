﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMaintenanceRecordsRepository.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   The <see cref="MaintenanceRecordsRepository" /> interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.DAL
{
  using AutomobileMaintenance.Models;

  /// <summary>
  /// The <see cref="MaintenanceRecordsRepository"/> interface.
  /// </summary>
  public interface IMaintenanceRecordsRepository : IRepository<MaintenanceRecordDto, int>
  {
    #region Methods

    /// <summary>
    /// Find the id of the vehicle
    /// </summary>
    /// <param name="toFind">Vehicle to find</param>
    /// <returns>vehicle id if it is in the database, <see langword="null"/> otherwise</returns>
    int? FindId(MaintenanceRecordDto toFind);

    #endregion Methods
  }
}