﻿namespace AutomobileMaintenance.DAL
{
  using AutoMapper;
  using AutomobileMaintenance.Entities;
  using AutomobileMaintenance.Models;
  using System;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Data.Entity.Migrations;
  using System.Linq;

  /// <summary>
  /// Vehicle Type Repository
  /// </summary>
  public class VehicleTypeRepository : IVehicleTypeRepository
  {
    #region Fields

    /// <summary>
    /// Vehicle Maintenance DbContext instance
    /// </summary>
    private readonly VehicleMaintenanceContext vehicleMaintenanceContext;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Constructor for dependency injection
    /// </summary>
    /// <param name="vehicleMaintenanceContext">vehicle Maintenance Context to use</param>
    public VehicleTypeRepository(VehicleMaintenanceContext vehicleMaintenanceContext)
    {
      this.vehicleMaintenanceContext = vehicleMaintenanceContext;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// <see cref="Add"/> a VehicleType
    /// </summary>
    /// <param name="entity">VehicleType Dto to add</param>
    public void Add(VehicleTypeDto entity)
    {
      var vehicleType = Mapper.Map<VehicleType>(entity);

      this.vehicleMaintenanceContext.VehicleTypes.Add(vehicleType);
    }

    /// <summary>
    /// Associate a maintenance task with the specified vehicle type
    /// </summary>
    /// <param name="id">Id of the vehicle type the task is to be added to</param>
    /// <param name="maintenanceTaskId">Id of the maintenance task to associate with specified vehicle type</param>
    public void AddMaintenanceTask(int id, int maintenanceTaskId)
    {
      var taskToAdd =
        this.vehicleMaintenanceContext.MaintenanceTasks.FirstOrDefault(x => x.MaintenanceTaskId == maintenanceTaskId);

      if (taskToAdd != null)
      {
        var query =
          this.vehicleMaintenanceContext.VehicleTypes.Where(x => x.VehicleTypeId == id)
            .Include(r => r.MaintenanceTasks);

        var vehicleTypeToModify = query.SingleOrDefault();

        vehicleTypeToModify?.MaintenanceTasks?.Add(taskToAdd);

        this.vehicleMaintenanceContext.VehicleTypes.AddOrUpdate(vehicleTypeToModify);
      }
    }

    /// <summary>
    /// Delete a vehicle type
    /// </summary>
    /// <param name="id">Id of the vehicle type to delete</param>
    public void Delete(int id)
    {
      var toDelete = this.vehicleMaintenanceContext.VehicleTypes.FirstOrDefault(x => x.VehicleTypeId == id);

      if (toDelete != null)
      {
        this.vehicleMaintenanceContext.VehicleTypes.Remove(toDelete);
      }
    }

    /// <summary>
    /// Find the id that matches the contents of a vehicle type
    /// </summary>
    /// <param name="toFind">vehicle type to find</param>
    /// <returns>vehicle type id if found, null otherwise</returns>
    public int? FindId(VehicleTypeDto toFind)
    {
      return
        this.vehicleMaintenanceContext.VehicleTypes.FirstOrDefault(x => x.VehicleTypeName == toFind.Name)?
          .VehicleTypeId;
    }

    /// <summary>
    /// <see cref="Get"/> the Vehicle Type
    /// </summary>
    /// <param name="id">VehicleType Id</param>
    /// <returns>Vehicle Type Data Transfer object that matches this <paramref name="id"/></returns>
    public VehicleTypeDto Get(int id)
    {
      try
      {
        return
          Mapper.Map<VehicleTypeDto>(this.vehicleMaintenanceContext.VehicleTypes.First(v => v.VehicleTypeId == id));
      }
      catch (InvalidOperationException)
      {
        return null;
      }
    }

    /// <summary>
    /// <see cref="Get"/> all Vehicle Types
    /// </summary>
    /// <returns>Collection of VehicleType DTO's</returns>
    public IEnumerable<VehicleTypeDto> GetAll()
    {
      IEnumerable<VehicleType> vehicles = this.vehicleMaintenanceContext.VehicleTypes;

      return Mapper.Map<IEnumerable<VehicleTypeDto>>(vehicles);
    }

    /// <summary>
    /// Get all maintenance tasks for a particular vehicle type
    /// </summary>
    /// <param name="id">Id of the vehicle type to get the maintenance tasks for</param>
    /// <returns>Collection of maintenance tasks</returns>
    public ICollection<MaintenanceTaskDto> GetAllMaintenanceTasks(int id)
    {
      var query =
        this.vehicleMaintenanceContext.VehicleTypes.Where(x => x.VehicleTypeId == id).Include(r => r.MaintenanceTasks);

      var databaseMaintenanceTasks = query?.SingleOrDefault()?.MaintenanceTasks;

      var maintenanceTasks = Mapper.Map<ICollection<MaintenanceTaskDto>>(databaseMaintenanceTasks);

      return maintenanceTasks;
    }

    /// <summary>
    /// Remove a maintenance task association from a vehicle type
    /// </summary>
    /// <param name="id">Id of the vehicle type to remove the association from</param>
    /// <param name="maintenanceTaskId">Id of the maintenance task to be dis-associated from the vehicle type</param>
    public void RemoveMaintenanceTask(int id, int maintenanceTaskId)
    {
      var vehicleType = this.vehicleMaintenanceContext.VehicleTypes.FirstOrDefault(v => v.VehicleTypeId == id);

      var toRemove = vehicleType?.MaintenanceTasks.FirstOrDefault(x => x.MaintenanceTaskId == maintenanceTaskId);

      if (toRemove == null)
      {
        return;
      }

      vehicleType.MaintenanceTasks.Remove(toRemove);

      this.vehicleMaintenanceContext.Entry(vehicleType).State = EntityState.Modified;
    }

    /// <summary>
    /// <see cref="Update"/> vehicle type
    /// </summary>
    /// <param name="entity">updated vehicle type</param>
    public void Update(VehicleTypeDto entity)
    {
      if (entity.VehicleTypeId == null)
      {
        return;
      }

      var vehicleType = entity.GetVehicleType();

      //vehicleType.MaintenanceTasks = this.vehicleMaintenanceContext.VehicleTypes.FirstOrDefault(x => x.VehicleTypeId == entity.VehicleTypeId).MaintenanceTasks;

      this.vehicleMaintenanceContext.VehicleTypes.AddOrUpdate(vehicleType);
    }

    #endregion Methods
  }
}