﻿namespace AutomobileMaintenance.DAL
{
  using AutoMapper;
  using AutomobileMaintenance.Entities;
  using AutomobileMaintenance.Models;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Data.Entity.Migrations;
  using System.Linq;

  /// <summary>
  /// Vehicle Repository
  /// </summary>
  public class VehicleRepository : IVehicleRepository
  {
    #region Fields

    /// <summary>
    /// Vehicle Maintenance DbContext instance
    /// </summary>
    private readonly VehicleMaintenanceContext vehicleMaintenanceContext;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Constructor for dependency injection
    /// </summary>
    /// <param name="vehicleMaintenanceContext">vehicle Maintenance Context to use</param>
    public VehicleRepository(VehicleMaintenanceContext vehicleMaintenanceContext)
    {
      this.vehicleMaintenanceContext = vehicleMaintenanceContext;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// <see cref="Add"/> a vehicle
    /// </summary>
    /// <param name="entity">Vehicle DTO to add</param>
    public void Add(VehicleDto entity)
    {
      var vehicle = Mapper.Map<Vehicle>(entity);

      this.vehicleMaintenanceContext.Vehicles.Add(vehicle);
    }

    /// <summary>
    /// <see cref="Delete"/> vehicle related to the provided vehicle id
    /// </summary>
    /// <param name="id">Vehicle Id</param>
    public void Delete(int id)
    {
      var vehicleToDelete = this.vehicleMaintenanceContext.Vehicles.FirstOrDefault(x => x.VehicleId == id);

      if (vehicleToDelete != null)
      {
        this.vehicleMaintenanceContext.Vehicles.Remove(vehicleToDelete);
      }
    }

    /// <summary>
    /// Checks if the vehicle is in the database and returns the id of it;
    /// </summary>
    /// <param name="toFind"></param>
    /// <returns></returns>
    public int? FindId(VehicleDto toFind)
    {
      int? id = this.vehicleMaintenanceContext.Vehicles?.FirstOrDefault(
        x => x.Name == toFind.Name && x.Make == toFind.Make && x.Model == toFind.Model && x.Year == toFind.Year)?
        .VehicleId;

      return id;
    }

    /// <summary>
    /// <see cref="Get"/> Vehicle
    /// </summary>
    /// <param name="id">id of vehicle to get</param>
    /// <returns>vehicle that matches the id, or null if not found</returns>
    public VehicleDto Get(int id)
    {
      return Mapper.Map<VehicleDto>(this.vehicleMaintenanceContext.Vehicles.FirstOrDefault(v => v.VehicleId == id));
    }

    /// <summary>
    /// <see cref="Get"/> all vehicles
    /// </summary>
    /// <returns>Collection of vehicle dto objects</returns>
    public IEnumerable<VehicleDto> GetAll()
    {
      IEnumerable<Vehicle> vehicles = this.vehicleMaintenanceContext.Vehicles;

      return Mapper.Map<IEnumerable<VehicleDto>>(vehicles);
    }

    /// <summary>
    /// Get maintenance records for the vehicle that matches the provided id
    /// </summary>
    /// <param name="id">Vehicle Id</param>
    /// <returns>Collection of maintenance records</returns>
    public IEnumerable<MaintenanceRecordDto> GetMaintenanceRecords(int id)
    {
      var query =
        this.vehicleMaintenanceContext.Vehicles.Where(v => v.VehicleId == id).Include(r => r.MaintenanceRecords);

      var vehicle = query.SingleOrDefault();

      var databaseMaintenanceRecords = vehicle?.MaintenanceRecords;

      var maintenanceRecords = Mapper.Map<ICollection<MaintenanceRecordDto>>(databaseMaintenanceRecords);

      return maintenanceRecords;
    }

    /// <summary>
    /// Update a vehicle with the information provided
    /// </summary>
    /// <param name="entity">vehicle information to update</param>
    public void Update(VehicleDto entity)
    {
      //Convert DTO to entity
      var vehicle = Mapper.Map<Vehicle>(entity);

      if (vehicle.VehicleId > 0)
      {
        this.vehicleMaintenanceContext.Vehicles.AddOrUpdate(vehicle);
      }
    }

    #endregion Methods
  }
}