﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUnitOfWork.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Implement the Unit of Work pattern
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.DAL
{
  using System;

  /// <summary>
  /// Implement the Unit of Work pattern
  /// </summary>
  public interface IUnitOfWork : IDisposable
  {
    #region Properties

    /// <summary>
    /// Gets Maintenance Records Repository
    /// </summary>
    IMaintenanceRecordsRepository MaintenanceRecordsRepository { get; }

    /// <summary>
    /// Gets MaintenanceTasks Repository
    /// </summary>
    IMaintenanceTasksRepository MaintenanceTasksRepository { get; }

    /// <summary>
    /// Gets VehicleSummary Repository
    /// </summary>
    IVehicleRepository VehicleRepository { get; }

    /// <summary>
    /// Gets VehicleSummary Repository
    /// </summary>
    IVehicleTypeRepository VehicleTypeRepository { get; }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Save Changes
    /// </summary>
    void SaveChanges();

    #endregion Methods
  }
}