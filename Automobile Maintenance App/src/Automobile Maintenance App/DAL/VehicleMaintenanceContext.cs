﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleMaintenanceContext.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   VehicleDTO Maintenance DataBase Context
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.DAL
{
  using System.Data.Common;
  using System.Data.Entity;

  using AutomobileMaintenance.Entities;

  /// <summary>
  /// VehicleDTO Maintenance DataBase Context
  /// </summary>
  public class VehicleMaintenanceContext : DbContext
  {
    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="VehicleMaintenanceContext"/> class. 
    /// Default Constructor
    /// </summary>
    public VehicleMaintenanceContext()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="VehicleMaintenanceContext"/> class. 
    /// Setup Database
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    public VehicleMaintenanceContext(DbConnection connection)
      : base(connection, true)
    {
    }

    #endregion Constructors

    #region Properties

    /// <summary>
    /// Gets or sets the <see cref="MaintenanceRecords"/> Table
    /// </summary>
    public virtual DbSet<MaintenanceRecord> MaintenanceRecords { get; set; }

    /// <summary>
    /// Gets or sets the MaintenanceTask Table
    /// </summary>
    public virtual DbSet<MaintenanceTask> MaintenanceTasks { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Vehicles"/> Table
    /// </summary>
    public virtual DbSet<Vehicle> Vehicles { get; set; }

    /// <summary>
    /// Gets or sets the EngineTypes Table
    /// </summary>
    public virtual DbSet<VehicleType> VehicleTypes { get; set; }

    #endregion Properties
  }
}