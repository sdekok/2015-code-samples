﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Generic class for basic functionality of repository classes
//   For implementing repository patterns
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.DAL
{
  using System.Collections.Generic;

  /// <summary>
  /// <para>Generic class for basic functionality of repository classes</para>
  /// <para>For implementing repository patterns</para>
  /// </summary>
  /// <typeparam name="TEntity">Entity Type</typeparam>
  /// <typeparam name="TKey">Key type</typeparam>
  public interface IRepository<TEntity, in TKey>
    where TEntity : class
  {
    #region Methods

    /// <summary>
    /// Save the <paramref name="entity"/> to the database
    /// </summary>
    /// <param name="entity">entity to add</param>
    void Add(TEntity entity);

    /// <summary>
    /// <see cref="Delete"/> the entity provided
    /// </summary>
    /// <param name="id">id of entity to delete</param>
    void Delete(int id);

    /// <summary>
    /// <see cref="Get"/> item
    /// </summary>
    /// <param name="id">Id of item to match</param>
    /// <returns>Entity retrieved from repository</returns>
    TEntity Get(TKey id);

    /// <summary>
    /// get all entities in this repository
    /// </summary>
    /// <returns>collection of entity's</returns>
    IEnumerable<TEntity> GetAll();

    /// <summary>
    /// <see cref="Update"/> the <paramref name="entity"/> in the database
    /// </summary>
    /// <param name="entity">updated entity</param>
    void Update(TEntity entity);

    #endregion Methods
  }
}