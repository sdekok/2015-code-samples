﻿namespace AutomobileMaintenance.DAL
{
  using AutoMapper;
  using AutomobileMaintenance.Entities;
  using AutomobileMaintenance.Models;
  using System.Collections.Generic;
  using System.Data.Entity.Migrations;
  using System.Linq;

  /// <summary>
  /// Maintenance task repository
  /// </summary>
  public class MaintenanceTasksRepository : IMaintenanceTasksRepository
  {
    #region Fields

    /// <summary>
    /// Vehicle Maintenance DbContext instance
    /// </summary>
    private readonly VehicleMaintenanceContext vehicleMaintenanceContext;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Constructor for dependency injection
    /// </summary>
    /// <param name="vehicleMaintenanceContext">vehicle Maintenance Context to use</param>
    public MaintenanceTasksRepository(VehicleMaintenanceContext vehicleMaintenanceContext)
    {
      this.vehicleMaintenanceContext = vehicleMaintenanceContext;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// Add maintenance task
    /// </summary>
    /// <param name="entity">maintenance task to add</param>
    public void Add(MaintenanceTaskDto entity)
    {
      var maintenanceTaskToAdd = Mapper.Map<MaintenanceTask>(entity);
      this.vehicleMaintenanceContext.MaintenanceTasks.Add(maintenanceTaskToAdd);
    }

    /// <summary>
    /// Delete maintenance task
    /// </summary>
    /// <param name="id">id of the maintenance task to be deleted</param>
    public void Delete(int id)
    {
      if (this.vehicleMaintenanceContext.MaintenanceTasks.Any(x => x.MaintenanceTaskId == id))
      {
        var toDelete = this.vehicleMaintenanceContext.MaintenanceTasks.First(x => x.MaintenanceTaskId == id);
        this.vehicleMaintenanceContext.MaintenanceTasks.Remove(toDelete);
      }
    }

    /// <summary>
    /// Find the id of a maintenance task
    /// </summary>
    /// <param name="toFind">maintenance task to find</param>
    /// <returns>id of the maintenance task if a match if found, null otherwise</returns>
    public int? FindId(MaintenanceTaskDto toFind)
    {
      var id =
        this.vehicleMaintenanceContext.MaintenanceTasks?.FirstOrDefault(
          x => x.MaintenanceDescription == toFind.Description && x.MaintenanceName == toFind.Name)
          ?.MaintenanceTaskId;

      return id;
    }

    /// <summary>
    /// Get maintenance task
    /// </summary>
    /// <param name="id">id of the maintenance task to retrieve</param>
    /// <returns>Maintenance task that matches the id if it exists, null otherwise</returns>
    public MaintenanceTaskDto Get(int id)
    {
      return
        Mapper.Map<MaintenanceTaskDto>(
          this.vehicleMaintenanceContext.MaintenanceTasks.FirstOrDefault(v => v.MaintenanceTaskId == id));
    }

    /// <summary>
    /// Get all maintenance tasks
    /// </summary>
    /// <returns>collection of maintenance tasks</returns>
    public IEnumerable<MaintenanceTaskDto> GetAll()
    {
      return Mapper.Map<IEnumerable<MaintenanceTaskDto>>(this.vehicleMaintenanceContext.MaintenanceTasks);
    }

    /// <summary>
    /// Update a maintenance task
    /// </summary>
    /// <param name="entity">updated maintenance task</param>
    public void Update(MaintenanceTaskDto entity)
    {
      var maintenanceTask = Mapper.Map<MaintenanceTask>(entity);

      var existingMaintenanceTask =
        this.vehicleMaintenanceContext.MaintenanceTasks.First(v => v.MaintenanceTaskId == entity.MaintenanceTaskId);

      maintenanceTask.VehicleTypes = existingMaintenanceTask.VehicleTypes;

      this.vehicleMaintenanceContext.MaintenanceTasks.AddOrUpdate(maintenanceTask);
    }

    #endregion Methods
  }
}