﻿namespace AutomobileMaintenance.DAL
{
  using System;

  /// <summary>
  /// Collection of Repositories
  /// </summary>
  public class RepositoryCollection : IUnitOfWork
  {
    #region Fields

    /// <summary>
    /// Vehicle Maintenance DBContext instance
    /// </summary>
    private readonly VehicleMaintenanceContext vehicleMaintenanceContext;

    /// <summary>
    /// Disposal state
    /// </summary>
    private bool disposed;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="RepositoryCollection"/> class. 
    /// Constructor for dependency injection
    /// </summary>
    /// <param name="vehicleMaintenanceContext">
    /// vehicle Maintenance Context to use
    /// </param>
    public RepositoryCollection(VehicleMaintenanceContext vehicleMaintenanceContext)
    {
      this.vehicleMaintenanceContext = vehicleMaintenanceContext;
      this.VehicleTypeRepository = new VehicleTypeRepository(this.vehicleMaintenanceContext);
      this.VehicleRepository = new VehicleRepository(vehicleMaintenanceContext);
      this.MaintenanceTasksRepository = new MaintenanceTasksRepository(vehicleMaintenanceContext);
      this.MaintenanceRecordsRepository = new MaintenanceRecordsRepository(vehicleMaintenanceContext);
    }

    #endregion Constructors

    #region Properties

    /// <summary>
    /// Gets the Maintenance Records Repository
    /// </summary>
    public IMaintenanceRecordsRepository MaintenanceRecordsRepository { get; }

    /// <summary>
    /// Gets the Maintenance Tasks Repository
    /// </summary>
    public IMaintenanceTasksRepository MaintenanceTasksRepository { get; }

    /// <summary>
    /// Gets the Vehicle Repository
    /// </summary>
    public IVehicleRepository VehicleRepository { get; }

    /// <summary>
    /// Gets the VehicleType Repository
    /// </summary>
    public IVehicleTypeRepository VehicleTypeRepository { get; }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Save Changes to the Database
    /// </summary>
    /// <exception cref="System.Data.Entity.Validation.DbEntityValidationException">
    /// The save was aborted because validation of entity property values failed.
    /// </exception>
    /// <exception cref="System.Data.Entity.Infrastructure.DbUpdateConcurrencyException">
    /// A database command did not affect the expected number of rows. This usually indicates an
    /// optimistic concurrency violation; that is, a row has been changed in the database since it
    /// was queried.
    /// </exception>
    /// <exception cref="System.Data.Entity.Infrastructure.DbUpdateException">
    /// An error occurred sending updates to the database.
    /// </exception>
    public void SaveChanges()
    {
      this.vehicleMaintenanceContext.SaveChanges();
    }

    #endregion Methods

    #region Disposing

    /// <summary>
    /// Public <see cref="Dispose"/> Method
    /// </summary>
    /// <exception cref="ArgumentNullException">vehicleMaintenanceContext is null. </exception>
    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// <see cref="Dispose"/> object
    /// </summary>
    /// <param name="disposing">dispose of this object</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!this.disposed)
      {
        if (disposing)
        {
          this.vehicleMaintenanceContext.Dispose();
        }
      }
      this.disposed = true;
    }

    #endregion Disposing
  }
}