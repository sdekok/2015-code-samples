﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the RouteConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance
{
  using System.Web.Mvc;
  using System.Web.Routing;

  /// <summary>
  /// The route config.
  /// </summary>
  public class RouteConfig
  {
    #region Methods

    /// <summary>
    /// Register <paramref name="routes"/>.
    /// </summary>
    /// <param name="routes">
    /// The routes.
    /// </param>
    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      routes.MapRoute(
        "Default",
        "{controller}/{action}/{id}",
        new { controller = "Home", action = "Index", id = UrlParameter.Optional });
    }

    #endregion Methods
  }
}