// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityConfig.cs" company="Stephen de kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Specifies the Unity configuration for the main container.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance
{
  using System;
  using System.Data.Entity.Core.EntityClient;

  using AutomobileMaintenance.DAL;

  using Microsoft.Practices.Unity;

  /// <summary>
  ///   Specifies the Unity configuration for the main Container.
  /// </summary>
  public class UnityConfig
  {
    #region Methods

    /// <summary>
    ///   Registers the type mappings with the Unity Container.
    /// </summary>
    /// <remarks>
    ///   There is no need to register concrete types such as controllers or API
    ///   controllers (unless you want to change the defaults), as Unity allows
    ///   resolving a concrete type even if it was not previously registered.
    /// </remarks>
    /// <param name="container">The unity Container to configure.</param>
    public static void RegisterTypes(IUnityContainer container)
    {
      // NOTE: To load from web.config uncomment the line below. Make sure to add a
      //       Microsoft.Practices.Unity.Configuration to the using statements. Container.LoadConfiguration();
      container.RegisterType<IUnitOfWork, RepositoryCollection>(new PerResolveLifetimeManager());
      container.RegisterType<IMaintenanceTasksRepository, MaintenanceTasksRepository>(new PerResolveLifetimeManager());
      container.RegisterType<IMaintenanceRecordsRepository, MaintenanceRecordsRepository>(new PerResolveLifetimeManager());
      container.RegisterType<IVehicleTypeRepository, VehicleTypeRepository>(new PerResolveLifetimeManager());
      container.RegisterType<IVehicleRepository, VehicleRepository>(new PerResolveLifetimeManager());

      container.RegisterType<VehicleMaintenanceContext>(new PerResolveLifetimeManager(), new InjectionConstructor());
    }

    #endregion Methods

    #region Unity Container

    /// <summary>
    /// The entity connection.
    /// </summary>
    private static readonly EntityConnection EntityConnection = new EntityConnection();

    /// <summary>
    /// The Container.
    /// </summary>
    private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(
      () =>
        {
          var container = new UnityContainer();
          RegisterTypes(container);
          return container;
        });

    /// <summary>
    /// Gets the configured Unity Container.
    /// </summary>
    /// <returns>
    /// The <see cref="IUnityContainer"/>.
    /// </returns>
    public static IUnityContainer GetConfiguredContainer()
    {
      return Container.Value;
    }

    #endregion Unity Container
  }
}