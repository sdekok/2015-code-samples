﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoMapperConfig.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Configure mapping
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance
{
  using System.Runtime.InteropServices;
  using System.Security.Cryptography.X509Certificates;

  using AutoMapper;

  using AutomobileMaintenance.Entities;
  using AutomobileMaintenance.Models;

  /// <summary>
  /// Configure mapping
  /// </summary>
  public static class AutoMapperConfig
  {
    #region Methods

    /// <summary>
    /// Create Auto Mapping between Entity POCO objects and DTO objects
    /// </summary>
    public static void RegisterMappings()
    {
      Mapper.CreateMap<Vehicle, VehicleDto>()
        .ForMember(dest => dest.VehicleTypeName, opts => opts.MapFrom(src => src.VehicleType.VehicleTypeName));

      Mapper.CreateMap<VehicleDto, Vehicle>();

      Mapper.CreateMap<VehicleType, VehicleTypeDto>()
        .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.VehicleTypeName));

      Mapper.CreateMap<VehicleTypeDto, VehicleType>()
        .ForMember(dest => dest.VehicleTypeName, opts => opts.MapFrom(src => src.Name));

      Mapper.CreateMap<MaintenanceTask, MaintenanceTaskDto>()
        .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.MaintenanceDescription))
        .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.MaintenanceName));

      Mapper.CreateMap<MaintenanceTaskDto, MaintenanceTask>()
        .ForMember(dest => dest.MaintenanceDescription, opts => opts.MapFrom(src => src.Description))
        .ForMember(dest => dest.MaintenanceName, opts => opts.MapFrom(src => src.Name));

      Mapper.CreateMap<MaintenanceRecord, MaintenanceRecordDto>();

      Mapper.CreateMap<MaintenanceRecordDto, MaintenanceRecord>();
    }

    #endregion Methods
  }
}