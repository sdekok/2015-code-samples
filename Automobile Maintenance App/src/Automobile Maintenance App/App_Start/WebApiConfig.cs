﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the WebApiConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance
{
  using System.Web.Http;
  using System.Web.Mvc;

  using AutomobileMaintenance.DAL;

  /// <summary>
  /// The web api config.
  /// </summary>
  public static class WebApiConfig
  {
    #region Methods

    /// <summary>
    /// Register HTTPConfiguration
    /// </summary>
    /// <param name="config">
    /// The <paramref name="config"/>.
    /// </param>
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services
      // config.DependencyResolver = new UnityResolver(UnityConfig.GetConfiguredContainer());
      // Web API routes
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
      
    }

    #endregion Methods
  }
}