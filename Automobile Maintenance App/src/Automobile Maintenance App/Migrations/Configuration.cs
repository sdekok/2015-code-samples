// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Configuration.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   This class sets up the initial database content after it has been created
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Migrations
{
  using System;
  using System.Collections.Generic;
  using System.Data.Entity.Migrations;

  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Entities;

  /// <summary>
  /// This class sets up the initial database content after it has been created
  /// </summary>
  internal sealed class Configuration : DbMigrationsConfiguration<VehicleMaintenanceContext>
  {
    #region Constructors

    public Configuration()
    {
      this.AutomaticMigrationsEnabled = false;
      this.ContextKey = "Automobile_Maintenance_App.DAL.VehicleMaintenanceContext";
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// The seed.
    /// </summary>
    /// <param name="context">
    /// The context.
    /// </param>
    protected override void Seed(VehicleMaintenanceContext context)
    {
      var oilChange = new MaintenanceTask
                        {
                          MaintenanceTaskId = 1,
                          MaintenanceDescription = "Change Oil and Filter",
                          MaintenanceName = "Oil Change"
                        };
      var rotateTires = new MaintenanceTask
                          {
                            MaintenanceTaskId = 2,
                            MaintenanceDescription = "Rotate Tires, Check Air Pressure",
                            MaintenanceName = "Rotate Tires"
                          };
      var changeWipers = new MaintenanceTask
                           {
                             MaintenanceTaskId = 3,
                             MaintenanceDescription = "Replace Wiper Blades",
                             MaintenanceName = "Replace Wipers"
                           };
      var addUrea = new MaintenanceTask
                      {
                        MaintenanceTaskId = 4,
                        MaintenanceDescription = "Add Urea",
                        MaintenanceName = "Add Urea"
                      };
      var fixSeats = new MaintenanceTask
                       {
                         MaintenanceTaskId = 5,
                         MaintenanceDescription = "Inspect seats for cuts and tears and repair",
                         MaintenanceName = "Fix Seats"
                       };

      context.MaintenanceTasks.AddOrUpdate(
        x => x.MaintenanceTaskId,
        oilChange,
        rotateTires,
        changeWipers,
        addUrea,
        fixSeats);

      context.SaveChanges();

      var gasolineVehicleType = new VehicleType
                                  {
                                    VehicleTypeId = 1,
                                    VehicleTypeName = "Gasoline",
                                    MaintenanceTasks =
                                      new List<MaintenanceTask> { oilChange, rotateTires, changeWipers }
                                  };

      var dieselTruckVehicleType = new VehicleType
                                     {
                                       VehicleTypeId = 2,
                                       VehicleTypeName = "Diesel Truck",
                                       MaintenanceTasks =
                                         new List<MaintenanceTask>
                                           {
                                             oilChange,
                                             rotateTires,
                                             changeWipers,
                                             addUrea
                                           }
                                     };

      var electicVehicleType = new VehicleType
                                 {
                                   VehicleTypeId = 3,
                                   VehicleTypeName = "Electric",
                                   MaintenanceTasks =
                                     new List<MaintenanceTask> { rotateTires, changeWipers }
                                 };

      var dieselBusVehicleType = new VehicleType
                                   {
                                     VehicleTypeId = 4,
                                     VehicleTypeName = "Diesel Bus",
                                     MaintenanceTasks =
                                       new HashSet<MaintenanceTask>
                                         {
                                           oilChange,
                                           rotateTires,
                                           changeWipers,
                                           addUrea,
                                           fixSeats
                                         }
                                   };

      context.VehicleTypes.AddOrUpdate(
        e => e.VehicleTypeId,
        gasolineVehicleType,
        dieselTruckVehicleType,
        electicVehicleType,
        dieselBusVehicleType);

      context.SaveChanges();

      var f150 = new Vehicle
                   {
                     VehicleType = gasolineVehicleType,
                     VehicleTypeId = gasolineVehicleType.VehicleTypeId,
                     Make = "Ford",
                     Model = "F150",
                     Year = 1999,
                     Name = "Red F150",
                     VehicleId = 1
                   };

      var nissanLeaf = new Vehicle
                         {
                           VehicleType = electicVehicleType,
                           VehicleTypeId = electicVehicleType.VehicleTypeId,
                           Make = "Nissan",
                           Model = "Leaf",
                           Name = "Nissan Leaf",
                           Year = 2015,
                           VehicleId = 2
                         };

      var schoolBus = new Vehicle
                        {
                          VehicleType = dieselBusVehicleType,
                          VehicleTypeId = dieselBusVehicleType.VehicleTypeId,
                          Make = "Bluebird",
                          Model = "Flat Nose",
                          Name = "Bluebird x-05",
                          Year = 2004,
                          VehicleId = 3
                        };

      context.Vehicles.AddOrUpdate(v => v.VehicleId, f150, nissanLeaf, schoolBus);

      context.SaveChanges();

      context.MaintenanceRecords.AddOrUpdate(
        mr => mr.MaintenanceRecordId,
        new MaintenanceRecord
          {
            MaintenanceRecordId = 1,
            MaintenanceTask = oilChange,
            MaintenanceTaskId = oilChange.MaintenanceTaskId,
            Date = new DateTime(2015, 11, 29, 07, 20, 00),
            Mileage = 524358,
            Notes = "Some metal shavings in the oil.  Should inspect for bearing wear.",
            Vehicle = schoolBus,
            VehicleId = schoolBus.VehicleId
          },
        new MaintenanceRecord
          {
            MaintenanceRecordId = 2,
            MaintenanceTask = fixSeats,
            MaintenanceTaskId = fixSeats.MaintenanceTaskId,
            Date = new DateTime(2015, 11, 29, 10, 57, 00),
            Mileage = 524358,
            Notes = "Apparently pencils puncture vinyl",
            Vehicle = schoolBus,
            VehicleId = schoolBus.VehicleId
          },
        new MaintenanceRecord
          {
            MaintenanceRecordId = 3,
            MaintenanceTask = rotateTires,
            MaintenanceTaskId = rotateTires.MaintenanceTaskId,
            Date = new DateTime(2015, 11, 29, 09, 57, 00),
            Mileage = 524358,
            Vehicle = nissanLeaf,
            VehicleId = nissanLeaf.VehicleId
          },
        new MaintenanceRecord
          {
            MaintenanceRecordId = 4,
            MaintenanceTask = oilChange,
            MaintenanceTaskId = oilChange.MaintenanceTaskId,
            Date = new DateTime(2015, 11, 29, 07, 20, 00),
            Mileage = 524358,
            Notes = "Oil still looked like new.  Can probably go longer next time.",
            Vehicle = f150,
            VehicleId = f150.VehicleId
          });

      context.SaveChanges();

      // This method will be called after migrating to the latest version.

      // You can use the DbSet<T>.AddOrUpdate() helper extension method to avoid creating duplicate
      // seed data. E.g.
      // 
      // context.People.AddOrUpdate( p => p.FullName, new Person { FullName = "Andrew Peters" }, new
      // Person { FullName = "Brice Lambson" }, new Person { FullName = "Rowan Miller" } );
    }

    #endregion Methods
  }
}