namespace AutomobileMaintenance.Migrations
{
  using System.Data.Entity.Migrations;

  public partial class Initial : DbMigration
  {
    #region Methods

    public override void Down()
    {
      this.DropForeignKey("dbo.Vehicles", "VehicleTypeId", "dbo.VehicleTypes");
      this.DropForeignKey("dbo.MaintenanceRecords", "VehicleId", "dbo.Vehicles");
      this.DropForeignKey("dbo.MaintenanceRecords", "MaintenanceTaskId", "dbo.MaintenanceTasks");
      this.DropForeignKey(
        "dbo.VehicleTypeMaintenanceTasks",
        "MaintenanceTask_MaintenanceTaskId",
        "dbo.MaintenanceTasks");
      this.DropForeignKey("dbo.VehicleTypeMaintenanceTasks", "VehicleType_VehicleTypeId", "dbo.VehicleTypes");
      this.DropIndex("dbo.VehicleTypeMaintenanceTasks", new[] { "MaintenanceTask_MaintenanceTaskId" });
      this.DropIndex("dbo.VehicleTypeMaintenanceTasks", new[] { "VehicleType_VehicleTypeId" });
      this.DropIndex("dbo.Vehicles", new[] { "VehicleTypeId" });
      this.DropIndex("dbo.MaintenanceRecords", new[] { "VehicleId" });
      this.DropIndex("dbo.MaintenanceRecords", new[] { "MaintenanceTaskId" });
      this.DropTable("dbo.VehicleTypeMaintenanceTasks");
      this.DropTable("dbo.Vehicles");
      this.DropTable("dbo.VehicleTypes");
      this.DropTable("dbo.MaintenanceTasks");
      this.DropTable("dbo.MaintenanceRecords");
    }

    public override void Up()
    {
      this.CreateTable(
        "dbo.MaintenanceRecords",
        c =>
        new
          {
            MaintenanceRecordId = c.Int(false, true),
            Date = c.DateTime(false),
            Mileage = c.Int(false),
            Notes = c.String(),
            MaintenanceTaskId = c.Int(false),
            VehicleId = c.Int(false)
          })
        .PrimaryKey(t => t.MaintenanceRecordId)
        .ForeignKey("dbo.MaintenanceTasks", t => t.MaintenanceTaskId, true)
        .ForeignKey("dbo.Vehicles", t => t.VehicleId, true)
        .Index(t => t.MaintenanceTaskId)
        .Index(t => t.VehicleId);

      this.CreateTable(
        "dbo.MaintenanceTasks",
        c =>
        new
          {
            MaintenanceTaskId = c.Int(false, true),
            MaintenanceName = c.String(),
            MaintenanceDescription = c.String()
          }).PrimaryKey(t => t.MaintenanceTaskId);

      this.CreateTable(
        "dbo.VehicleTypes",
        c => new { VehicleTypeId = c.Int(false, true), VehicleTypeName = c.String() }).PrimaryKey(t => t.VehicleTypeId);

      this.CreateTable(
        "dbo.Vehicles",
        c =>
        new
          {
            VehicleId = c.Int(false, true),
            Make = c.String(),
            Model = c.String(),
            Year = c.Int(false),
            Name = c.String(),
            VehicleTypeId = c.Int(false)
          })
        .PrimaryKey(t => t.VehicleId)
        .ForeignKey("dbo.VehicleTypes", t => t.VehicleTypeId, true)
        .Index(t => t.VehicleTypeId);

      this.CreateTable(
        "dbo.VehicleTypeMaintenanceTasks",
        c => new { VehicleType_VehicleTypeId = c.Int(false), MaintenanceTask_MaintenanceTaskId = c.Int(false) })
        .PrimaryKey(t => new { t.VehicleType_VehicleTypeId, t.MaintenanceTask_MaintenanceTaskId })
        .ForeignKey("dbo.VehicleTypes", t => t.VehicleType_VehicleTypeId, true)
        .ForeignKey("dbo.MaintenanceTasks", t => t.MaintenanceTask_MaintenanceTaskId, true)
        .Index(t => t.VehicleType_VehicleTypeId)
        .Index(t => t.MaintenanceTask_MaintenanceTaskId);
    }

    #endregion Methods
  }
}