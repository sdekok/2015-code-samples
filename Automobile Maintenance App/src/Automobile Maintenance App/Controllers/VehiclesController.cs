﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehiclesController.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the VehiclesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Controllers
{
  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;
  using System.Data.Entity.Infrastructure;
  using System.Linq;
  using System.Net;
  using System.Net.Http;
  using System.Web.Http;
  using System.Web.Http.Description;

  /// <summary>
  /// Controller to give api access to the vehicles class
  /// </summary>
  public class VehiclesController : ApiController
  {
    #region Fields

    /// <summary>
    /// Repository Collection
    /// </summary>
    private readonly IUnitOfWork repositoryCollection;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Constructor for dependence injection
    /// </summary>
    /// <param name="repositoryCollection">UnitOfWork Repository Collection</param>
    public VehiclesController(IUnitOfWork repositoryCollection)
    {
      this.repositoryCollection = repositoryCollection;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// Delete vehicle with specified id
    /// </summary>
    /// <example>DELETE: api/Vehicles/5</example>
    /// <param name="id">vehicle id</param>
    /// <returns><c>IHttpActionResult</c> with status</returns>
    [ResponseType(typeof(VehicleDto))]
    public IHttpActionResult DeleteVehicle(int id)
    {
      var vehicleDto = this.repositoryCollection.VehicleRepository.Get(id);
      if (vehicleDto == null)
      {
        return this.NotFound();
      }

      this.repositoryCollection.VehicleRepository.Delete(id);
      this.repositoryCollection.SaveChanges();

      return this.Ok(vehicleDto);
    }

    /// <summary>
    /// Get vehicle with specified id
    /// </summary>
    /// <example>GET: api/Vehicles/5</example>
    /// <param name="id"></param>
    /// <returns><c>IHttpActionResult</c> with vehicle</returns>
    [ResponseType(typeof(VehicleDto))]
    public IHttpActionResult GetVehicle(int id)
    {
      var vehicle = this.repositoryCollection.VehicleRepository.Get(id);
      if (vehicle == null)
      {
        return this.NotFound();
      }

      return this.Ok(vehicle);
    }

    /// <summary>
    /// Get all maintenance tasks for this vehicle
    /// </summary>
    /// <param name="id">vehicle id</param>
    /// <returns>Collection of maintenance records</returns>
    [Route("api/Vehicles/{id:int}/MaintenanceRecords")]
    public IQueryable<MaintenanceRecordDto> GetVehicleMaintenanceRecords(int id)
    {
      var vehicleMaintenanceRecords = this.repositoryCollection.VehicleRepository.GetMaintenanceRecords(id);

      return vehicleMaintenanceRecords?.AsQueryable();
    }

    /// <summary>
    /// Get all maintenance tasks for this vehicle
    /// </summary>
    /// <example>GET: api/Vehicles/5/MaintenanceTasks</example>
    /// <param name="id">vehicle id</param>
    /// <returns>Collection of maintenance tasks</returns>
    [HttpGet]
    [Route("api/Vehicles/{id:int}/MaintenanceTasks")]
    public IQueryable<MaintenanceTaskDto> GetVehicleMaintenanceTasks(int id)
    {
      var vehicle = this.repositoryCollection.VehicleRepository.Get(id);
     
      if (vehicle != null)
      {
        var vehicleMaintenanceTasks = this.repositoryCollection.VehicleTypeRepository.GetAllMaintenanceTasks(vehicle.VehicleTypeId);
        return vehicleMaintenanceTasks.AsQueryable();
      }
      else
      {
        return null;
      }
    }

    /// <summary>
    /// Get all vehicles
    /// </summary>
    /// <example>GET: api/Vehicles</example>
    /// <returns>Collection of vehicles</returns>
    public IQueryable<VehicleDto> GetVehicles()
    {
      return this.repositoryCollection.VehicleRepository.GetAll().AsQueryable();
    }

    /// <summary>
    /// Add vehicle
    /// </summary>
    /// <example>POST: api/Vehicles</example>
    /// <param name="vehicleDto">Vehicle</param>
    /// <returns><c>IHttpActionResult</c> with status and vehicle</returns>
    [ResponseType(typeof(VehicleDto))]
    public IHttpActionResult PostVehicle(VehicleDto vehicleDto)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      if (this.repositoryCollection.VehicleRepository.FindId(vehicleDto) != null)
      {
        return this.BadRequest("Vehicle already exists");
      }

      if (this.repositoryCollection.VehicleTypeRepository.Get(vehicleDto.VehicleTypeId) == null)
      {
        return this.BadRequest("Invalid vehicle type for this vehicle");
      }

      this.repositoryCollection.VehicleRepository.Add(vehicleDto);
      this.repositoryCollection.SaveChanges();

      //Find the id assigned

      var vehicleId = this.repositoryCollection.VehicleRepository.FindId(vehicleDto);

      if (vehicleId == null)
      {
        return this.InternalServerError(new CommitFailedException("Vehicle could not be added"));
      }

      vehicleDto.VehicleId = vehicleId;

      return this.CreatedAtRoute("DefaultApi", new { id = vehicleDto.VehicleId }, vehicleDto);
    }

    /// <summary>
    /// Handle HTTP options request
    /// </summary>
    /// <returns>HTTP Response Message</returns>
    [HttpOptions]
    public HttpResponseMessage Options()
    {
      return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
    }

    /// <summary>
    /// Update vehicle
    /// </summary>
    /// <example>PUT: api/Vehicles/5</example>
    /// <param name="id">vehicle id</param>
    /// <param name="vehicleDto">vehicle</param>
    /// <returns>IHTTPActionResult with status</returns>
    /// <exception cref="DbUpdateConcurrencyException">Vehicle could not be saved to database.</exception>
    [ResponseType(typeof(void))]
    public IHttpActionResult PutVehicle(int id, VehicleDto vehicleDto)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      if (id != vehicleDto.VehicleId)
      {
        return this.BadRequest();
      }

      if (this.repositoryCollection.VehicleRepository.Get(id) == null)
      {
        return this.BadRequest("Vehicle was not found.");
      }

      if (this.repositoryCollection.VehicleTypeRepository.Get(vehicleDto.VehicleTypeId) == null)
      {
        return this.BadRequest("Vehicle type is not a valid vehicle type");
      }

      this.repositoryCollection.VehicleRepository.Update(vehicleDto);

      try
      {
        this.repositoryCollection.SaveChanges();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!this.VehicleExists(id))
        {
          return this.NotFound();
        }
        throw;
      }

      return this.StatusCode(HttpStatusCode.NoContent);
    }

    /// <summary>
    /// Dispose method
    /// </summary>
    /// <param name="disposing"><paramref name="disposing"/></param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.repositoryCollection.Dispose();
      }
      base.Dispose(disposing);
    }

    /// <summary>
    /// Helper function to check if the vehicle exists in the database
    /// </summary>
    /// <param name="id">vehicle id</param>
    /// <returns><see langword="true"/> if vehicle exists, false otherwise</returns>
    private bool VehicleExists(int id)
    {
      return this.repositoryCollection.VehicleRepository.GetAll().Any(x => x.VehicleId == id);
    }

    #endregion Methods
  }
}