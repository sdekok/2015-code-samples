﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleTypesController.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the VehicleTypesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Controllers
{
  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;
  using System.Data.Entity.Infrastructure;
  using System.Linq;
  using System.Net;
  using System.Net.Http;
  using System.Web.Http;
  using System.Web.Http.Description;

  /// <summary>
  /// Vehicle Types Controller
  /// </summary>
  public class VehicleTypesController : ApiController
  {
    #region Fields

    /// <summary>
    /// Repository Collection
    /// </summary>
    private readonly IUnitOfWork repositoryCollection;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Vehicle types controller
    /// </summary>
    /// <param name="repositoryCollection"></param>
    public VehicleTypesController(IUnitOfWork repositoryCollection)
    {
      this.repositoryCollection = repositoryCollection;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// Delete a maintenance task
    /// </summary>
    /// <param name="id">Vehicle Id</param>
    /// <param name="maintenanceTaskId">Vehicle Id</param>
    /// <returns>Result including deleted maintenance task if successfully removed</returns>
    [HttpDelete]
    [Route("api/VehicleTypes/{id:int}/MaintenanceTasks/{maintenanceTaskId:int}")]
    [ResponseType(typeof(MaintenanceTaskDto))]
    public IHttpActionResult DeleteMaintenanceTask(int id, int maintenanceTaskId)
    {
      if (this.repositoryCollection.VehicleTypeRepository.Get(id) == null)
      {
        return this.NotFound();
      }

      var maintenanceTask =
        this.repositoryCollection.VehicleTypeRepository.GetAllMaintenanceTasks(id)?
          .First(x => x.MaintenanceTaskId == maintenanceTaskId);

      if (maintenanceTask == null)
      {
        return this.NotFound();
      }

      this.repositoryCollection.VehicleTypeRepository.RemoveMaintenanceTask(id, maintenanceTaskId);
      this.repositoryCollection.SaveChanges();

      return this.Ok(maintenanceTask);
    }

    // DELETE: api/VehicleTypes/5
    /// <summary>
    /// Delete vehicle type
    /// </summary>
    /// <param name="id">vehicle id to delete</param>
    /// <returns>status and vehicle type if successfully removed</returns>
    [HttpDelete]
    [ResponseType(typeof(VehicleTypeDto))]
    public IHttpActionResult DeleteVehicleType(int id)
    {
      var vehicleType = this.repositoryCollection.VehicleTypeRepository.Get(id);
      if (vehicleType == null)
      {
        return this.NotFound();
      }

      this.repositoryCollection.VehicleTypeRepository.Delete(id);
      this.repositoryCollection.SaveChanges();

      return this.Ok(vehicleType);
    }

    /// <summary>
    /// Get maintenance tasks
    /// </summary>
    /// <param name="id">vehicle id</param>
    /// <returns>collection of vehicle maintenance tasks</returns>
    [HttpGet]
    [Route("api/VehicleTypes/{id:int}/MaintenanceTasks")]
    public IQueryable<MaintenanceTaskDto> GetMaintenanceTasks(int id)
    {
      var vehicleMaintenanceTasks = this.repositoryCollection.VehicleTypeRepository.GetAllMaintenanceTasks(id);

      return vehicleMaintenanceTasks.AsQueryable();
    }

    /// <summary>
    /// Get Vehicle Type
    /// </summary>
    /// <param name="id"></param>
    /// <example>GET: api/VehicleTypes/5</example>
    /// <returns>Vehicle type</returns>
    [ResponseType(typeof(VehicleTypeDto))]
    public IHttpActionResult GetVehicleType(int id)
    {
      var vehicleType = this.repositoryCollection.VehicleTypeRepository.Get(id);
      if (vehicleType == null)
      {
        return this.NotFound();
      }

      return this.Ok(vehicleType);
    }

    /// <summary>
    /// Get all vehicle types
    /// </summary>
    /// <example>POST: api/VehicleTypes</example>
    /// <returns>Collection of all vehicle types</returns>
    public IQueryable<VehicleTypeDto> GetVehicleTypes()
    {
      return this.repositoryCollection.VehicleTypeRepository.GetAll().AsQueryable();
    }

    /// <summary>
    /// Add maintenance task to vehicle type
    /// </summary>
    /// <param name="id">Vehicle Type Id</param>
    /// <param name="maintenanceTaskId">Maintenance Task Id</param>
    /// <returns><c>IHttpActionResult</c></returns>
    [HttpPost]
    [Route("api/VehicleTypes/{id:int}/MaintenanceTasks/{maintenanceTaskId:int}")]
    public IHttpActionResult PostMaintenanceTask(int id, int maintenanceTaskId)
    {
      if (this.repositoryCollection.VehicleTypeRepository.Get(id) == null)
      {
        return this.NotFound();
      }

      var maintenanceTask = this.repositoryCollection.MaintenanceTasksRepository.Get(maintenanceTaskId);

      if (maintenanceTask == null)
      {
        return this.BadRequest("No maintenance task with this maintenance task id was found");
      }

      if (
        this.repositoryCollection.VehicleTypeRepository.GetAllMaintenanceTasks(id)?
          .FirstOrDefault(x => x.MaintenanceTaskId == maintenanceTaskId) != null)
      {
        return this.BadRequest("Maintenance task is already associated with this vehicle");
      }

      this.repositoryCollection.VehicleTypeRepository.AddMaintenanceTask(id, maintenanceTaskId);
      this.repositoryCollection.SaveChanges();

      return this.StatusCode(HttpStatusCode.Created);
    }

    /// <summary>
    /// Add Vehicle Type
    /// </summary>
    /// <example>POST: api/VehicleTypes</example>
    /// <param name="vehicleType">Vehicle Type to add</param>
    /// <returns><c>IHttpActionResult</c> with status</returns>
    [ResponseType(typeof(VehicleTypeDto))]
    public IHttpActionResult PostVehicleType(VehicleTypeDto vehicleType)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      if (this.repositoryCollection.VehicleTypeRepository.FindId(vehicleType) != null)
      {
        return this.BadRequest("Item already exists");
      }

      this.repositoryCollection.VehicleTypeRepository.Add(vehicleType);
      this.repositoryCollection.SaveChanges();

      int? newId = this.repositoryCollection?.VehicleTypeRepository?.FindId(vehicleType);
      if (newId > 0)
      {
        vehicleType.VehicleTypeId = newId;
      }
      else
      {
        return this.InternalServerError(new DbUpdateException("Failed to save new vehicle type"));
      }

      return this.CreatedAtRoute("DefaultApi", new { id = newId }, vehicleType);
    }

    /// <summary>
    /// Handle HTTP options request
    /// </summary>
    /// <returns>HTTP Response Message</returns>
    [HttpOptions]
    public HttpResponseMessage Options()
    {
      return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
    }

    /// <summary>
    /// Handle HTTP options request
    /// </summary>
    /// <returns>HTTP Response Message</returns>
    [HttpOptions]
    [Route("api/VehicleTypes/{id:int}/MaintenanceTasks/{maintenanceTaskId:int}")]
    public HttpResponseMessage Options(int id, int maintenanceTaskId)
    {
      return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
    }

    /// <summary>
    /// Update the vehicle type
    /// </summary>
    /// <example>PUT: api/VehicleTypes/5</example>
    /// <param name="id">vehicle type id</param>
    /// <param name="vehicleType">vehicle type object</param>
    /// <returns><c>IHttpActionResult</c> with status</returns>
    /// <exception cref="DbUpdateConcurrencyException">Failed to update database.</exception>
    [ResponseType(typeof(void))]
    public IHttpActionResult PutVehicleType(int id, VehicleTypeDto vehicleType)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      if (id != vehicleType.VehicleTypeId)
      {
        return this.BadRequest();
      }

      if (this.repositoryCollection.VehicleTypeRepository.Get(id) == null)
      {
        return this.BadRequest("Vehicle type could not be found");
      }

      this.repositoryCollection.VehicleTypeRepository.Update(vehicleType);

      try
      {
        this.repositoryCollection.SaveChanges();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!this.VehicleTypeExists(id))
        {
          return this.NotFound();
        }
        throw;
      }

      return this.StatusCode(HttpStatusCode.NoContent);
    }

    /// <summary>
    /// Dispose method
    /// </summary>
    /// <param name="disposing"><paramref name="disposing"/></param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.repositoryCollection.Dispose();
      }
      base.Dispose(disposing);
    }

    /// <summary>
    /// Helper method that determines if the vehicle type exists.
    /// </summary>
    /// <param name="id"></param>
    /// <returns><see langword="true"/> if it exists, false otherwise</returns>
    private bool VehicleTypeExists(int id)
    {
      return this.repositoryCollection.VehicleTypeRepository.GetAll().Count(e => e.VehicleTypeId == id) > 0;
    }

    #endregion Methods
  }
}