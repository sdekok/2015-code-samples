﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="Stephen de Kok">
//   Copyright 2016 Stephen de Kok
// </copyright>
// <summary>
//   Controller to load the home page
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Controllers
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics.CodeAnalysis;
  using System.Linq;
  using System.Web;
  using System.Web.Mvc;

  /// <summary>
  /// Controller to load the home page
  /// </summary>
  public class HomeController : Controller
    {
      /// <summary>
      /// Loads the index html page.
      /// </summary>
      /// <returns>path to index.html</returns>
      public ActionResult Index()
        {
      var result = new FilePathResult("~/index.html", "text/html");
      return result;
    }
    }
}