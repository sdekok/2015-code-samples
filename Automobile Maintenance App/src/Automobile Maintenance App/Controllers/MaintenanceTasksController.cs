﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceTasksController.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Defines the MaintenanceTasksController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Controllers
{
  using System.Data.Entity.Infrastructure;
  using System.Diagnostics.CodeAnalysis;
  using System.Linq;
  using System.Net;
  using System.Net.Http;
  using System.Web.Http;
  using System.Web.Http.Description;

  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;

  /// <summary>
  /// The maintenance tasks controller.
  /// </summary>
  public class MaintenanceTasksController : ApiController
  {
    #region Fields

    /// <summary>
    /// The repository collection.
    /// </summary>
    private readonly IUnitOfWork repositoryCollection;

    #endregion Fields

    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="MaintenanceTasksController"/> class. 
    /// Repository Collection
    /// </summary>
    /// <param name="repositoryCollection">Repository Collection</param>
    public MaintenanceTasksController(IUnitOfWork repositoryCollection)
    {
      this.repositoryCollection = repositoryCollection;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// The delete maintenance task.
    /// </summary>
    /// <param name="id">
    /// The id.
    /// </param>
    /// <example>DELETE: api/MaintenanceTasks/5</example>
    /// <returns>
    /// The <see cref="IHttpActionResult"/>.
    /// </returns>
    [ResponseType(typeof(MaintenanceTaskDto))]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IHttpActionResult DeleteMaintenanceTask(int id)
    {
      var maintenanceTask = this.repositoryCollection.MaintenanceTasksRepository.Get(id);
      if (maintenanceTask == null)
      {
        return this.NotFound();
      }

      this.repositoryCollection.MaintenanceTasksRepository.Delete(id);
      this.repositoryCollection.SaveChanges();

      return this.Ok(maintenanceTask);
    }

    /// <summary>
    /// The get maintenance task.
    /// </summary>
    /// <example>Get: api/MaintenanceTasks/5</example>
    /// <param name="id">
    /// The id.
    /// </param>
    /// <returns>
    /// The <see cref="IHttpActionResult"/>.
    /// </returns>
    [ResponseType(typeof(MaintenanceTaskDto))]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IHttpActionResult GetMaintenanceTask(int id)
    {
      var maintenanceTask = this.repositoryCollection.MaintenanceTasksRepository.Get(id);
      if (maintenanceTask == null)
      {
        return this.NotFound();
      }

      return this.Ok(maintenanceTask);
    }

    /// <summary>
    /// Get all maintenance tasks.
    /// </summary>
    /// <example>GET: api/MaintenanceTasks</example>
    /// <returns>
    /// The <see cref="IQueryable"/> collection of all maintenance tasks.
    /// </returns>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IQueryable<MaintenanceTaskDto> GetMaintenanceTasks()
    {
      return this.repositoryCollection.MaintenanceTasksRepository.GetAll().AsQueryable();
    }

    /// <summary>
    /// Add a maintenance task.
    /// </summary>
    /// <example>POST: api/MaintenanceTasks</example>
    /// <param name="maintenanceTask">
    /// The maintenance task to add.
    /// </param>
    /// <returns>
    /// The <see cref="IHttpActionResult"/>.
    /// </returns>
    [ResponseType(typeof(MaintenanceTaskDto))]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IHttpActionResult PostMaintenanceTask(MaintenanceTaskDto maintenanceTask)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      if (maintenanceTask.MaintenanceTaskId != null && maintenanceTask.MaintenanceTaskId >= 0)
      {
        if (this.repositoryCollection.MaintenanceTasksRepository.Get((int)maintenanceTask.MaintenanceTaskId) != null)
        {
          return this.BadRequest("Maintenance task with this id already exists.  Could not add.");
        }
      }

      this.repositoryCollection.MaintenanceTasksRepository.Add(maintenanceTask);
      this.repositoryCollection.SaveChanges();

      var maintenanceTaskId = this.repositoryCollection.MaintenanceTasksRepository.FindId(maintenanceTask);

      if (maintenanceTaskId == null)
      {
        return this.InternalServerError(new CommitFailedException("Vehicle could not be added"));
      }

      maintenanceTask.MaintenanceTaskId = maintenanceTaskId;

      return this.CreatedAtRoute("DefaultApi", new { id = maintenanceTask.MaintenanceTaskId }, maintenanceTask);
    }

    /// <summary>
    /// Handle http options request
    /// </summary>
    /// <returns>Http Response Message</returns>
    [HttpOptions]
    public HttpResponseMessage Options()
    {
      return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
    }

    /// <summary>
    /// Update a maintenance task.
    /// </summary>
    /// <example>PUT: api/MaintenanceTasks/5</example>
    /// <param name="id">
    /// The maintenance task id.
    /// </param>
    /// <param name="maintenanceTask">
    /// The maintenance task.
    /// </param>
    /// <returns>
    /// The <see cref="IHttpActionResult"/>.
    /// </returns>
    /// <exception cref="DbUpdateConcurrencyException">Could not update the database.</exception>
    [ResponseType(typeof(void))]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IHttpActionResult PutMaintenanceTask(int id, MaintenanceTaskDto maintenanceTask)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      if (id != maintenanceTask.MaintenanceTaskId)
      {
        return this.BadRequest();
      }

      this.repositoryCollection.MaintenanceTasksRepository.Update(maintenanceTask);

      try
      {
        this.repositoryCollection.SaveChanges();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!this.MaintenanceTaskExists(id))
        {
          return this.NotFound();
        }

        throw;
      }

      return this.StatusCode(HttpStatusCode.NoContent);
    }

    /// <summary>
    /// <see cref="Dispose"/> of the repository collection
    /// </summary>
    /// <param name="disposing">disposing flag</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.repositoryCollection.Dispose();
      }

      base.Dispose(disposing);
    }

    /// <summary>
    /// Check if one or more maintenance tasks exists
    /// </summary>
    /// <param name="id">MaintenanceTask Id</param>
    /// <returns><see langword="true"/> if task exist, <see langword="false"/> otherwise.</returns>
    private bool MaintenanceTaskExists(int id)
    {
      return this.repositoryCollection.MaintenanceTasksRepository.GetAll().Count(e => e.MaintenanceTaskId == id) > 0;
    }

    #endregion Methods
  }
}