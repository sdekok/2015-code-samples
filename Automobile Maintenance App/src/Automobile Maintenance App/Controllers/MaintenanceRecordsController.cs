﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceRecordsController.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Provide api for accessing the database
//   Provides a rest interface via WebApi2
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Controllers
{
  using System.Data.Entity.Infrastructure;
  using System.Diagnostics.CodeAnalysis;
  using System.Linq;
  using System.Net;
  using System.Net.Http;
  using System.Web.Http;
  using System.Web.Http.Description;

  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;

  /// <summary>
  /// Provide a
  /// Provides a rest <c>interface</c> for maintenance records via WebApi2
  /// </summary>
  [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
  public class MaintenanceRecordsController : ApiController
  {
    #region Fields

    /// <summary>
    /// The repository collection.
    /// </summary>
    private readonly IUnitOfWork repositoryCollection;

    #endregion Fields

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="MaintenanceRecordsController"/> class. 
    /// </summary>
    /// <param name="repositoryCollection">
    /// UnitOFWork implementation to use.
    /// </param>
    public MaintenanceRecordsController(IUnitOfWork repositoryCollection)
    {
      this.repositoryCollection = repositoryCollection;
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// Delete a maintenance record
    /// </summary>
    /// <example>DELETE: api/MaintenanceRecords/5</example>
    /// <param name="id">MaintenanceRecordId to delete</param>
    /// <returns>deleted maintenance record</returns>
    [ResponseType(typeof(MaintenanceRecordDto))]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IHttpActionResult DeleteMaintenanceRecord(int id)
    {
      var maintenanceRecord = this.repositoryCollection.MaintenanceRecordsRepository.Get(id);
      if (maintenanceRecord == null)
      {
        return this.NotFound();
      }

      this.repositoryCollection.MaintenanceRecordsRepository.Delete(id);
      this.repositoryCollection.SaveChanges();

      return this.Ok(maintenanceRecord);
    }

    /// <summary>
    /// Get maintenance record for provided id
    /// </summary>
    /// <example>GET: api/MaintenanceRecords/5</example>
    /// <param name="id">Maintenance Record Id</param>
    /// <returns><c>IHttpActionResult</c></returns>
    [HttpGet]
    [ResponseType(typeof(MaintenanceRecordDto))]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IHttpActionResult GetMaintenanceRecord(int id)
    {
      var maintenanceRecord = this.repositoryCollection.MaintenanceRecordsRepository.Get(id);
      if (maintenanceRecord == null)
      {
        return this.NotFound();
      }

      return this.Ok(maintenanceRecord);
    }

    /// <summary>
    /// Get all maintenance records
    /// </summary>
    /// <example>GET: api/MaintenanceRecords</example>
    /// <returns>Maintenance Records</returns>
    [HttpGet]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IQueryable<MaintenanceRecordDto> GetMaintenanceRecords()
    {
      return this.repositoryCollection.MaintenanceRecordsRepository.GetAll().AsQueryable();
    }

    /// <summary>
    /// Add new maintenance record
    /// </summary>
    /// <param name="maintenanceRecord">Maintenance record to add</param>
    /// <returns>success or failure response</returns>
    [HttpPost]
    [ResponseType(typeof(MaintenanceRecordDto))]
    public IHttpActionResult PostMaintenanceRecord(MaintenanceRecordDto maintenanceRecord)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      // If Maintenance record is a maintenance task that is not in the maintenance tasks for the vehicle, return it as a bad request
      if (!this.HasValidMaintenanceTask(maintenanceRecord))
      {
        return this.BadRequest("Maintenance type is not valid for this vehicle");
      }

      this.repositoryCollection.MaintenanceRecordsRepository.Add(maintenanceRecord);
      this.repositoryCollection.SaveChanges();

      maintenanceRecord.MaintenanceRecordId =
        this.repositoryCollection.MaintenanceRecordsRepository.FindId(maintenanceRecord);

      return this.CreatedAtRoute("DefaultApi", new { id = maintenanceRecord.MaintenanceRecordId }, maintenanceRecord);
    }

    /// <summary>
    /// Handle http options request
    /// </summary>
    /// <returns>Http Response Message</returns>
    [HttpOptions]
    public HttpResponseMessage Options()
    {
      return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
    }

    /// <summary>
    /// Update maintenance record with provided information
    /// </summary>
    /// <example>PUT: api/MaintenanceRecords/5</example>
    /// <param name="id">Id of the record to update</param>
    /// <param name="maintenanceRecord">updated record information</param>
    /// <returns>Success or failure status</returns>
    /// <exception cref="DbUpdateConcurrencyException">Failed to update database.</exception>
    [HttpPut]
    [ResponseType(typeof(void))]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public IHttpActionResult PutMaintenanceRecord(int id, MaintenanceRecordDto maintenanceRecord)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      if (id != maintenanceRecord.MaintenanceRecordId)
      {
        return this.BadRequest();
      }

      // If Maintenance record is a maintenance task that is not in the maintenance tasks for the vehicle, return it as a bad request
      if (!this.HasValidMaintenanceTask(maintenanceRecord))
      {
        return this.BadRequest("Maintenance type is not valid for this vehicle");
      }

      this.repositoryCollection.MaintenanceRecordsRepository.Update(maintenanceRecord);

      try
      {
        this.repositoryCollection.SaveChanges();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!this.MaintenanceRecordExists(id))
        {
          return this.NotFound();
        }

        throw;
      }

      return this.StatusCode(HttpStatusCode.NoContent);
    }

    /// <summary>
    /// Dispose method
    /// </summary>
    /// <param name="disposing">dispose of <see langword="this"/> class</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.repositoryCollection.Dispose();
      }

      base.Dispose(disposing);
    }

    /// <summary>
    /// Helper function to determine if the maintenance record's maintenance type exists in the maintenance tasks database
    /// </summary>
    /// <param name="maintenanceRecord">Maintenance record to validate</param>
    /// <returns><see langword="true"/> if referenced maintenance task exists, false otherwise</returns>
    private bool HasValidMaintenanceTask(MaintenanceRecordDto maintenanceRecord)
    {
      var vehicleTypeId = this.repositoryCollection.VehicleRepository.Get(maintenanceRecord.VehicleId).VehicleTypeId;

      var validMaintenanceTypes = this.repositoryCollection.VehicleTypeRepository.GetAllMaintenanceTasks(
        vehicleTypeId);

      if (validMaintenanceTypes.Count(x => x.MaintenanceTaskId == maintenanceRecord.MaintenanceTaskId) == 0)
      {
        return false;
      }

      return true;
    }

    /// <summary>
    /// Check if maintenance record exists
    /// </summary>
    /// <param name="id">Maintenance Record id</param>
    /// <returns><see langword="true"/> if id is found, false otherwise</returns>
    private bool MaintenanceRecordExists(int id)
    {
      return this.repositoryCollection.MaintenanceRecordsRepository.GetAll().Count(e => e.MaintenanceRecordId == id) > 0;
    }

    #endregion Methods
  }
}