@echo off

SET SOLUTIONPATH=%~dp0%~1
SET MSBUILDDIR=C:\Program Files (x86)\MSBuild\14.0\Bin\

IF NOT EXIST "%MSBUILDDIR%MSBuild.exe" echo "Recommended MsBuild Not Found"

IF EXIST "%MSBUILDDIR%MSBuild.exe" goto build

echo "Getting MsBuild from Registry"

reg.exe query "HKLM\SOFTWARE\Microsoft\MSBuild\ToolsVersions\4.0" /v MSBuildToolsPath > nul 2>&1
if ERRORLEVEL 1 goto MissingMSBuildRegistry

for /f "skip=2 tokens=2,*" %%A in ('reg.exe query "HKLM\SOFTWARE\Microsoft\MSBuild\ToolsVersions\4.0" /v MSBuildToolsPath') do SET MSBUILDDIR=%%B

IF NOT EXIST %MSBUILDDIR%nul goto MissingMSBuildToolsPath
IF NOT EXIST %MSBUILDDIR%msbuild.exe goto MissingMSBuildExe


:build 
SET MSBUILDDIR=C:\Program Files (x86)\MSBuild\14.0\Bin\
"%MSBUILDDIR%msbuild.exe" /version

"%MSBUILDDIR%msbuild.exe" "%SOLUTIONPATH%\Automobile Maintenance App.sln" /p:Configuration=Debug

goto:eof
::ERRORS
::---------------------
:MissingMSBuildRegistry
echo Cannot obtain path to MSBuild tools from registry
goto:eof
:MissingMSBuildToolsPath
echo The MSBuild tools path from the registry '%MSBUILDDIR%' does not exist
goto:eof
:MissingMSBuildExe
echo The MSBuild executable could not be found at '%MSBUILDDIR%'
goto:eof