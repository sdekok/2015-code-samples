﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 
/// </summary>
namespace AutomobileMaintenance.Tests
{
  using System.Net.Http;
  using System.Web.Http;
  using System.Web.Http.Controllers;
  using System.Web.Http.Dispatcher;
  using System.Web.Http.Hosting;
  using System.Web.Http.Routing;

  /// <summary>
  /// 
  /// </summary>
  public class RouteTester
  {
    private HttpConfiguration config;
    private HttpRequestMessage request;
    private IHttpRouteData routeData;
    private IHttpControllerSelector controllerSelector;
    private HttpControllerContext controllerContext;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="conf"></param>
    /// <param name="req"></param>
    public RouteTester(HttpConfiguration conf, HttpRequestMessage req)
    {
      this.config = conf;
      this.request = req;
      this.routeData = config.Routes.GetRouteData(request);
      this.request.Properties[HttpPropertyKeys.HttpRouteDataKey] = routeData;
      this.controllerSelector = new DefaultHttpControllerSelector(config);
      this.controllerContext = new HttpControllerContext(config, routeData, request);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string GetActionName()
    {
      if (controllerContext.ControllerDescriptor == null)
      {
        GetControllerType();
      }

      var actionSelector = new ApiControllerActionSelector();
      var descriptor = actionSelector.SelectAction(controllerContext);

      return descriptor.ActionName;
    }

    /// <summary>
    /// Get the controller type
    /// </summary>
    /// <returns>Get the </returns>
    public Type GetControllerType()
    {
      var descriptor = this.controllerSelector.SelectController(this.request);
      this.controllerContext.ControllerDescriptor = descriptor;
      return descriptor.ControllerType;
    }
  }
}
