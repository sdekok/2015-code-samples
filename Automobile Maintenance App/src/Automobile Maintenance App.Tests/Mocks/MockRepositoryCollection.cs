﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockRepositoryCollection.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Setup standard mock context for testing
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Mocks
{
  using System;
  using System.Collections.Generic;
  using System.Linq;

  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;

  using Moq;

  /// <summary>
  /// Setup standard mock context for testing
  /// </summary>
  public static class MockRepositoryCollection
  {
    #region Properties

    /// <summary>
    /// Gets the maintenance records.
    /// </summary>
    public static List<MaintenanceRecordDto> MaintenanceRecords
    {
      get
      {
        var maintenanceRecordList = new List<MaintenanceRecordDto>
                                      {
                                        new MaintenanceRecordDto
                                          {
                                            MaintenanceRecordId = 1,
                                            MaintenanceTaskId = 1,
                                            Date =
                                              new DateTime(
                                              2015,
                                              11,
                                              29,
                                              07,
                                              20,
                                              00),
                                            Mileage = 524358,
                                            Notes =
                                              "Some metal shavings in the oil.  Should inspect for bearing wear.",
                                            VehicleId = 3
                                          },
                                        new MaintenanceRecordDto
                                          {
                                            MaintenanceRecordId = 2,
                                            MaintenanceTaskId = 5,
                                            Date =
                                              new DateTime(
                                              2015,
                                              11,
                                              29,
                                              10,
                                              57,
                                              00),
                                            Mileage = 524358,
                                            Notes =
                                              "Apparently pencils puncture vinyl",
                                            VehicleId = 3
                                          },
                                        new MaintenanceRecordDto
                                          {
                                            MaintenanceRecordId = 3,
                                            MaintenanceTaskId = 2,
                                            Date =
                                              new DateTime(
                                              2015,
                                              11,
                                              29,
                                              09,
                                              57,
                                              00),
                                            Mileage = 524358,
                                            VehicleId = 2
                                          },
                                        new MaintenanceRecordDto
                                          {
                                            MaintenanceRecordId = 4,
                                            MaintenanceTaskId = 1,
                                            Date =
                                              new DateTime(
                                              2015,
                                              11,
                                              29,
                                              07,
                                              20,
                                              00),
                                            Mileage = 524358,
                                            Notes =
                                              "Oil still looked like new.  Can probably go longer next time.",
                                            VehicleId = 1
                                          }
                                      };
        return maintenanceRecordList;
      }
    }

    /// <summary>
    /// Gets the maintenance tasks.
    /// </summary>
    public static List<MaintenanceTaskDto> MaintenanceTasks
    {
      get
      {
        var oilChange = new MaintenanceTaskDto
                          {
                            MaintenanceTaskId = 1,
                            Description = "Change Oil and Filter",
                            Name = "Oil Change"
                          };
        var rotateTires = new MaintenanceTaskDto
                            {
                              MaintenanceTaskId = 2,
                              Description = "Rotate Tires, Check Air Pressure",
                              Name = "Rotate Tires"
                            };
        var changeWipers = new MaintenanceTaskDto
                             {
                               MaintenanceTaskId = 3,
                               Description = "Replace Wiper Blades",
                               Name = "Replace Wipers"
                             };
        var addUrea = new MaintenanceTaskDto
                        {
                          MaintenanceTaskId = 4,
                          Description = "Add Urea",
                          Name = "Add Urea"
                        };
        var fixSeats = new MaintenanceTaskDto
                         {
                           MaintenanceTaskId = 5,
                           Description = "Inspect seats for cuts and tears and repair",
                           Name = "Fix Seats"
                         };

        return new List<MaintenanceTaskDto> { oilChange, rotateTires, changeWipers, addUrea, fixSeats };
      }
    }

    /// <summary>
    /// Gets the vehicles.
    /// </summary>
    public static List<VehicleDto> Vehicles
    {
      get
      {
        var f150 = new VehicleDto
                     {
                       VehicleTypeId = 1,
                       Make = "Ford",
                       Model = "F150",
                       Year = 1999,
                       Name = "Red F150",
                       VehicleId = 1
                     };

        var nissanLeaf = new VehicleDto
                           {
                             VehicleTypeId = 3,
                             Make = "Nissan",
                             Model = "Leaf",
                             Name = "Nissan Leaf",
                             Year = 2015,
                             VehicleId = 2
                           };

        var schoolBus = new VehicleDto
                          {
                            VehicleTypeId = 4,
                            Make = "Bluebird",
                            Model = "Flat Nose",
                            Name = "Bluebird x-05",
                            Year = 2004,
                            VehicleId = 3
                          };

        var vehicleList = new List<VehicleDto> { f150, nissanLeaf, schoolBus };

        return vehicleList;
      }
    }

    /// <summary>
    /// Gets the vehicle types.
    /// </summary>
    public static List<VehicleTypeDto> VehicleTypes
    {
      get
      {
        var gasolineVehicleType = new VehicleTypeDto { VehicleTypeId = 1, Name = "Gasoline" };

        var dieselTruckVehicleType = new VehicleTypeDto { VehicleTypeId = 2, Name = "Diesel Truck" };

        var electicVehicleType = new VehicleTypeDto { VehicleTypeId = 3, Name = "Electric" };

        var dieselBusVehicleType = new VehicleTypeDto { VehicleTypeId = 4, Name = "Diesel Bus" };

        var vehicleTypeList = new List<VehicleTypeDto>
                                {
                                  gasolineVehicleType,
                                  dieselTruckVehicleType,
                                  electicVehicleType,
                                  dieselBusVehicleType
                                };
        return vehicleTypeList;
      }
    }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Get Full Mock Context
    /// </summary>
    /// <returns>Mock Context</returns>
    public static Mock<IUnitOfWork> GetMockRepositoryCollection()
    {
      return SetupMock();
    }

    /// <summary>
    /// Setup the default context mock for testing.
    /// </summary>
    /// <returns>
    /// The <see cref="Mock"/>.
    /// </returns>
    private static Mock<IUnitOfWork> SetupMock()
    {
      var maintenanceTaskList = MaintenanceTasks;

      var maintenanceTaskMockSet = new Mock<IMaintenanceTasksRepository>();
      maintenanceTaskMockSet.Setup(x => x.GetAll()).Returns(maintenanceTaskList.AsEnumerable());

      var vehicleTypeList = VehicleTypes;

      var vehicleTypeMockSet = new Mock<IVehicleTypeRepository>();
      vehicleTypeMockSet.Setup(x => x.GetAll()).Returns(vehicleTypeList.AsEnumerable());

      var vehicleList = Vehicles;

      var vehicleMockSet = new Mock<IVehicleRepository>();
      vehicleMockSet.Setup(x => x.GetAll()).Returns(vehicleList.AsEnumerable());

      var maintenanceRecordList = MaintenanceRecords;

      var maintenanceRecordMockSet = new Mock<IMaintenanceRecordsRepository>();
      maintenanceRecordMockSet.Setup(x => x.GetAll()).Returns(maintenanceRecordList.AsEnumerable());
      var defaultMockContext = new Mock<IUnitOfWork>();
      defaultMockContext.Setup(c => c.MaintenanceTasksRepository).Returns(maintenanceTaskMockSet.Object);
      defaultMockContext.Setup(c => c.MaintenanceRecordsRepository).Returns(maintenanceRecordMockSet.Object);
      defaultMockContext.Setup(c => c.VehicleTypeRepository).Returns(vehicleTypeMockSet.Object);
      defaultMockContext.Setup(c => c.VehicleRepository).Returns(vehicleMockSet.Object);

      return defaultMockContext;
    }

    #endregion Methods
  }
}