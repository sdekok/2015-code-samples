﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockContextSetup.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Setup standard mock context for testing
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Mocks
{
  using System;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Linq;

  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Entities;

  using Moq;

  /// <summary>
  /// Setup standard mock context for testing
  /// </summary>
  public static class MockContextSetup
  {
    #region Methods

    /// <summary>
    /// Gets the mock context
    /// </summary>
    /// <returns>mock vehicle maintenance context</returns>
    public static Mock<VehicleMaintenanceContext> GetMockContext()
    {
      return SetupMock();
    }

    /// <summary>
    /// Create the mock of a DBSet from a list of objects
    /// </summary>
    /// <typeparam name="T">Type of <see langword="object"/></typeparam>
    /// <param name="itemsList">list of type <typeparamref name="T"/></param>
    /// <returns>Mock DBSet of type T</returns>
    private static Mock<DbSet<T>> CreateMockSet<T>(ref List<T> itemsList) where T : class
    {
      var queryableList = itemsList.AsQueryable();
      var mockSet = new Mock<DbSet<T>>();
      mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryableList.Provider);
      mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryableList.Expression);

      mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryableList.ElementType);
      mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(queryableList.GetEnumerator());

      return mockSet;
    }

    /// <summary>
    /// Setup the default context mock for testing.
    /// </summary>
    /// <returns>
    /// The <see cref="Mock"/>.
    /// </returns>
    private static Mock<VehicleMaintenanceContext> SetupMock()
    {
      Mock<VehicleMaintenanceContext> defaultMockContext;

      var oilChange = new MaintenanceTask
                        {
                          MaintenanceTaskId = 1,
                          MaintenanceDescription = "Change Oil and Filter",
                          MaintenanceName = "Oil Change"
                        };
      var rotateTires = new MaintenanceTask
                          {
                            MaintenanceTaskId = 2,
                            MaintenanceDescription = "Rotate Tires, Check Air Pressure",
                            MaintenanceName = "Rotate Tires"
                          };
      var changeWipers = new MaintenanceTask
                           {
                             MaintenanceTaskId = 3,
                             MaintenanceDescription = "Replace Wiper Blades",
                             MaintenanceName = "Replace Wipers"
                           };
      var addUrea = new MaintenanceTask
                      {
                        MaintenanceTaskId = 4,
                        MaintenanceDescription = "Add Urea",
                        MaintenanceName = "Add Urea"
                      };
      var fixSeats = new MaintenanceTask
                       {
                         MaintenanceTaskId = 5,
                         MaintenanceDescription = "Inspect seats for cuts and tears and repair",
                         MaintenanceName = "Fix Seats"
                       };

      var maintenanceTaskList = new List<MaintenanceTask> { oilChange, rotateTires, changeWipers, addUrea, fixSeats };

      var maintenanceTaskMockSet = CreateMockSet(ref maintenanceTaskList);

      var gasolineVehicleType = new VehicleType { VehicleTypeId = 1, VehicleTypeName = "Gasoline" };

      var dieselTruckVehicleType = new VehicleType { VehicleTypeId = 2, VehicleTypeName = "Diesel Truck" };

      var electicVehicleType = new VehicleType { VehicleTypeId = 3, VehicleTypeName = "Electric" };

      var dieselBusVehicleType = new VehicleType { VehicleTypeId = 4, VehicleTypeName = "Diesel Bus" };

      var vehicleTypeList = new List<VehicleType>
                              {
                                gasolineVehicleType,
                                dieselTruckVehicleType,
                                electicVehicleType,
                                dieselBusVehicleType
                              };

      var vehicleTypeMockSet = CreateMockSet(ref vehicleTypeList);

      var f150 = new Vehicle
                   {
                     VehicleType = gasolineVehicleType,
                     VehicleTypeId = gasolineVehicleType.VehicleTypeId,
                     Make = "Ford",
                     Model = "F150",
                     Year = 1999,
                     Name = "Red F150",
                     VehicleId = 1
                   };

      var nissanLeaf = new Vehicle
                         {
                           VehicleType = electicVehicleType,
                           VehicleTypeId = electicVehicleType.VehicleTypeId,
                           Make = "Nissan",
                           Model = "Leaf",
                           Name = "Nissan Leaf",
                           Year = 2015,
                           VehicleId = 2
                         };

      var schoolBus = new Vehicle
                        {
                          VehicleType = dieselBusVehicleType,
                          VehicleTypeId = dieselBusVehicleType.VehicleTypeId,
                          Make = "Bluebird",
                          Model = "Flat Nose",
                          Name = "Bluebird x-05",
                          Year = 2004,
                          VehicleId = 3
                        };

      var vehicleList = new List<Vehicle> { f150, nissanLeaf, schoolBus };

      var vehicleMockSet = CreateMockSet(ref vehicleList);

      var maintenanceRecordList = new List<MaintenanceRecord>
                                    {
                                      new MaintenanceRecord
                                        {
                                          MaintenanceRecordId = 1,
                                          MaintenanceTask = oilChange,
                                          MaintenanceTaskId =
                                            oilChange.MaintenanceTaskId,
                                          Date =
                                            new DateTime(
                                            2015,
                                            11,
                                            29,
                                            07,
                                            20,
                                            00),
                                          Mileage = 524358,
                                          Notes =
                                            "Some metal shavings in the oil.  Should inspect for bearing wear.",
                                          Vehicle = schoolBus,
                                          VehicleId = schoolBus.VehicleId
                                        },
                                      new MaintenanceRecord
                                        {
                                          MaintenanceRecordId = 2,
                                          MaintenanceTask = fixSeats,
                                          MaintenanceTaskId =
                                            fixSeats.MaintenanceTaskId,
                                          Date =
                                            new DateTime(
                                            2015,
                                            11,
                                            29,
                                            10,
                                            57,
                                            00),
                                          Mileage = 524358,
                                          Notes =
                                            "Apparently pencils puncture vinyl",
                                          Vehicle = schoolBus,
                                          VehicleId = schoolBus.VehicleId
                                        },
                                      new MaintenanceRecord
                                        {
                                          MaintenanceRecordId = 3,
                                          MaintenanceTask = rotateTires,
                                          MaintenanceTaskId =
                                            rotateTires.MaintenanceTaskId,
                                          Date =
                                            new DateTime(
                                            2015,
                                            11,
                                            29,
                                            09,
                                            57,
                                            00),
                                          Mileage = 524358,
                                          Vehicle = nissanLeaf,
                                          VehicleId = nissanLeaf.VehicleId
                                        },
                                      new MaintenanceRecord
                                        {
                                          MaintenanceRecordId = 4,
                                          MaintenanceTask = oilChange,
                                          MaintenanceTaskId =
                                            oilChange.MaintenanceTaskId,
                                          Date =
                                            new DateTime(
                                            2015,
                                            11,
                                            29,
                                            07,
                                            20,
                                            00),
                                          Mileage = 524358,
                                          Notes =
                                            "Oil still looked like new.  Can probably go longer next time.",
                                          Vehicle = f150,
                                          VehicleId = f150.VehicleId
                                        }
                                    };

      var maintenanceRecordMockSet = CreateMockSet(ref maintenanceRecordList);
      defaultMockContext = new Mock<VehicleMaintenanceContext>();
      defaultMockContext.Setup(c => c.MaintenanceTasks).Returns(maintenanceTaskMockSet.Object);
      defaultMockContext.Setup(c => c.MaintenanceRecords).Returns(maintenanceRecordMockSet.Object);
      defaultMockContext.Setup(c => c.VehicleTypes).Returns(vehicleTypeMockSet.Object);
      defaultMockContext.Setup(c => c.Vehicles).Returns(vehicleMockSet.Object);

      return defaultMockContext;
    }

    #endregion Methods
  }
}