﻿namespace AutomobileMaintenance.Tests.DAL
{
  using System.IO;
  using System.Reflection;

  using AutomobileMaintenance.DAL;

  using Effort;
  using Effort.DataLoaders;

  /// <summary>
  /// Common setup functions for repository tests
  /// </summary>
  public static class RepositoryTestCollectionSetup
  {
    #region Methods

    /// <summary>
    /// Get repository without data loaded
    /// </summary>
    /// <returns>repository with empty data</returns>
    public static RepositoryCollection GetEmptyRepository()
    {
      var connection = DbConnectionFactory.CreateTransient();
      var maintenanceContext = new VehicleMaintenanceContext(connection);
      var repositoryCollection = new RepositoryCollection(maintenanceContext);
      return repositoryCollection;
    }

    /// <summary>
    /// Get repository with data loaded
    /// </summary>
    /// <returns>repository with data loaded</returns>
    public static RepositoryCollection GetLoadedRepository()
    {
      //Setup Database and Repository
      IDataLoader loader =
        new CsvDataLoader(
          Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + @"\..\..\EffortDataBaseContent\"));
      var connection = DbConnectionFactory.CreateTransient(loader);
      var maintenanceContext = new VehicleMaintenanceContext(connection);
      var repositoryCollection = new RepositoryCollection(maintenanceContext);
      return repositoryCollection;
    }

    #endregion Methods
  }
}