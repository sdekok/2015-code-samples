﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleRepositoryTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Vehicle Repository Tests
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.DAL
{
  using System.Linq;

  using AutomobileMaintenance;
  using AutomobileMaintenance.Models;

  using NUnit.Framework;

  /// <summary>
  /// Vehicle Repository Tests
  /// </summary>
  [TestFixture]
  public class VehicleRepositoryTests
  {
    /// <summary>
    /// Perform tasks that need to be done before tests are run
    /// </summary>
    [SetUp]
    public void SetupTests()
    {
      AutoMapperConfig.RegisterMappings();
    }

    /// <summary>
    /// Verify that Vehicles can be added successfully
    /// </summary>
    [Test]
    public void AddTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var vehicleToAdd = new VehicleDto
                           {
                             VehicleId = null,
                             Make = "Dodge",
                             Model = "Ram 150",
                             Name = "Ram 150",
                             VehicleTypeId = 1,
                             VehicleTypeName = null,
                             Year = 1986
                           };

      repositories.VehicleRepository.Add(vehicleToAdd);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var id = repositories.VehicleRepository.FindId(vehicleToAdd);
      Assert.NotNull(id);
      Assert.AreEqual(6, id);
    }

    /// <summary>
    /// Verify that items are deleted correctly
    /// </summary>
    [Test]
    public void DeleteTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var count = repositories.VehicleRepository.GetAll().Count();
      repositories.VehicleRepository.Delete(1);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      Assert.AreEqual(count - 1, repositories.VehicleRepository.GetAll().Count());

      repositories.VehicleRepository.Delete(1);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      Assert.AreEqual(count - 1, repositories.VehicleRepository.GetAll().Count());
    }

    /// <summary>
    /// Verify that the id of an existing item can be determined
    /// </summary>
    [Test]
    public void FindIdTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();
      var vehicleToFind = repositories.VehicleRepository.Get(1);

      vehicleToFind.VehicleId = null;

      Assert.AreEqual(1, repositories.VehicleRepository.FindId(vehicleToFind));

      vehicleToFind.VehicleId = 99;

      Assert.AreEqual(1, repositories.VehicleRepository.FindId(vehicleToFind));
    }

    /// <summary>
    /// Verify that All Vehicles can be gathered from the database
    /// </summary>
    [Test]
    public void GetAllTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      Assert.AreEqual(5, repositories.VehicleRepository.GetAll().Count());
    }

    /// <summary>
    /// Verify that a collection of maintenance records is returned for a particular vehicle
    /// </summary>
    [Test]
    public void GetMaintenanceRecordsTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();
      Assert.IsTrue(repositories.VehicleRepository.GetMaintenanceRecords(1).Count() > 0);
    }

    /// <summary>
    /// Verify that a particular vehicle can be returned
    /// </summary>
    [Test]
    public void GetTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var vehicleType = repositories.VehicleRepository.Get(1);

      // Assert
      Assert.IsNotNull(vehicleType);
      Assert.AreEqual(1, vehicleType.VehicleTypeId);
    }

    /// <summary>
    /// Verify that a particular vehicle can be returned
    /// </summary>
    [Test]
    public void GetTestReturnsNullWhenItemDoesNotExist()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var vehicleType = repositories.VehicleRepository.Get(99);

      // Assert
      Assert.IsNull(vehicleType);
    }

    /// <summary>
    /// Verify that items are updated successfully
    /// </summary>
    [Test]
    public void UpdateTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var vehicleToUpdate = repositories.VehicleRepository.Get(1);

      vehicleToUpdate.Name = "Test Vehicle";

      repositories.VehicleRepository.Update(vehicleToUpdate);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      Assert.AreEqual("Test Vehicle", repositories.VehicleRepository.Get(1).Name);

      vehicleToUpdate.VehicleId = null;
      vehicleToUpdate.Name = "Test Vehicle 23";

      repositories.VehicleRepository.Update(vehicleToUpdate);
      Assert.AreEqual("Test Vehicle", repositories.VehicleRepository.Get(1).Name);
    }
  }
}