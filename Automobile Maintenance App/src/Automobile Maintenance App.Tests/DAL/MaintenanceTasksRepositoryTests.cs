﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceTasksRepositoryTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Maintenance Tasks Repository Tests
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.DAL
{
  using System.Linq;

  using AutomobileMaintenance;
  using AutomobileMaintenance.Models;

  using NUnit.Framework;

  /// <summary>
  /// Maintenance Tasks Repository Tests
  /// </summary>
  [TestFixture]
  public class MaintenanceTasksRepositoryTests
  {
    /// <summary>
    /// Setup tasks that apply for all tests
    /// </summary>
    [SetUp]
    public void SetupTests()
    {
      AutoMapperConfig.RegisterMappings();
    }

    /// <summary>
    /// Verify that a maintenance task can be added
    /// </summary>
    [Test]
    public void AddTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var maintenanceTaskDto = new MaintenanceTaskDto
                                 {
                                   MaintenanceTaskId = null,
                                   Description = "Run around car 3 times",
                                   Name = "Exercise"
                                 };

      repositories.MaintenanceTasksRepository.Add(maintenanceTaskDto);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var id = repositories.MaintenanceTasksRepository.FindId(maintenanceTaskDto);

      Assert.IsNotNull(id);
      Assert.AreEqual(6, id);
    }

    /// <summary>
    /// Verify that a maintenance task can be deleted
    /// </summary>
    [Test]
    public void DeleteTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var count = repositories.MaintenanceTasksRepository.GetAll().Count();
      var vehicleAssignedCount = repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count;

      repositories.MaintenanceTasksRepository.Delete(1);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      Assert.AreEqual(count - 1, repositories.MaintenanceTasksRepository.GetAll().Count());
      Assert.AreEqual(vehicleAssignedCount - 1, repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count);
    }

    /// <summary>
    /// Verify that an id can be found for a specific database
    /// </summary>
    [Test]
    public void FindIdTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var existingMaintenanceTask = repositories.MaintenanceTasksRepository.Get(1);

      existingMaintenanceTask.MaintenanceTaskId = null;

      Assert.AreEqual(1, repositories.MaintenanceTasksRepository.FindId(existingMaintenanceTask));
    }

    /// <summary>
    /// Verify that all maintenance tasks are successfully retrieved
    /// </summary>
    [Test]
    public void GetAllTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var count = repositories.MaintenanceTasksRepository.GetAll().Count();

      Assert.AreEqual(5, count);
    }

    /// <summary>
    /// Verify a specific maintenance task is successfully retrieved
    /// </summary>
    [Test]
    public void GetTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var maintenanceTask = repositories.MaintenanceTasksRepository.Get(1);

      // Assert
      Assert.IsNotNull(maintenanceTask);
    }

    /// <summary>
    /// Verify a specific maintenance task returns null when not in database
    /// </summary>
    [Test]
    public void GetTestReturnsNullWhenItemDoesNotExist()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var maintenanceTask = repositories.MaintenanceTasksRepository.Get(99);

      // Assert
      Assert.IsNull(maintenanceTask);
    }

    /// <summary>
    /// verify that a maintenance task can be updated
    /// </summary>
    [Test]
    public void UpdateTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var maintenanceTask = repositories.MaintenanceTasksRepository.Get(1);

      maintenanceTask.Description = "Test Description";

      repositories.MaintenanceTasksRepository.Update(maintenanceTask);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var updatedTaskDescription = repositories.MaintenanceTasksRepository.Get(1).Description;

      Assert.AreEqual("Test Description", updatedTaskDescription);
    }
  }
}