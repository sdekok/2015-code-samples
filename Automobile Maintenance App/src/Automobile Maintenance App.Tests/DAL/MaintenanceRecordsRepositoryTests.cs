﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceRecordsRepositoryTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Verify that the Maintenance Repository Tests are working correctly
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.DAL
{
  using System;
  using System.Linq;

  using AutomobileMaintenance;
  using AutomobileMaintenance.Models;

  using NUnit.Framework;

  /// <summary>
  /// Verify that the Maintenance Repository Tests are working correctly
  /// </summary>
  [TestFixture]
  public class MaintenanceRecordsRepositoryTests
  {
    /// <summary>
    /// Setup that needs to be done before any tests are run
    /// </summary>
    [SetUp]
    public void SetupTests()
    {
      AutoMapperConfig.RegisterMappings();
    }

    /// <summary>
    /// Test the add method
    /// </summary>
    [Test]
    public void AddTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var maintenanceRecord = new MaintenanceRecordDto
                                {
                                  Date = new DateTime(2015, 12, 05, 12, 0, 0),
                                  MaintenanceRecordId = null,
                                  MaintenanceTaskId = 1,
                                  Mileage = 254350,
                                  Notes = "This is interesting",
                                  VehicleId = 1
                                };

      repositories.MaintenanceRecordsRepository.Add(maintenanceRecord);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var id = repositories.MaintenanceRecordsRepository.FindId(maintenanceRecord);

      // Assert
      Assert.IsTrue(id != null && id > 0);
    }

    /// <summary>
    /// Verify that maintenance records can be deleted
    /// </summary>
    [Test]
    public void DeleteTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var initialCount = repositories.MaintenanceRecordsRepository.GetAll().Count();

      repositories.MaintenanceRecordsRepository.Delete(2);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var updatedCount = repositories.MaintenanceRecordsRepository.GetAll().Count();

      Assert.AreEqual(initialCount - 1, updatedCount);
    }

    /// <summary>
    /// Verify that the id can be found for a given maintenance record
    /// </summary>
    [Test]
    public void FindIdTest()
    {
      var repositoryCollection = RepositoryTestCollectionSetup.GetLoadedRepository();
      var maintenanceRecord = repositoryCollection.MaintenanceRecordsRepository.Get(1);

      maintenanceRecord.MaintenanceRecordId = 0;

      var id = repositoryCollection.MaintenanceRecordsRepository.FindId(maintenanceRecord);
      Assert.AreEqual(1, id);

      maintenanceRecord.MaintenanceRecordId = null;

      id = repositoryCollection.MaintenanceRecordsRepository.FindId(maintenanceRecord);
      Assert.AreEqual(1, id);

      maintenanceRecord.MaintenanceRecordId = 2041;

      id = repositoryCollection.MaintenanceRecordsRepository.FindId(maintenanceRecord);
      Assert.AreEqual(1, id);
    }

    /// <summary>
    /// Test the GetAll Method
    /// </summary>
    [Test]
    public void GetAllTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var maintenanceItems = repositories.MaintenanceRecordsRepository.GetAll();

      // Assert
      Assert.AreEqual(4, maintenanceItems.Count());
    }

    /// <summary>
    /// Test the get method
    /// </summary>
    [Test]
    public void GetTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var maintenanceRecord = repositories.MaintenanceRecordsRepository.Get(1);

      // Assert
      Assert.IsNotNull(maintenanceRecord);
    }

    /// <summary>
    /// Test the get method
    /// </summary>
    [Test]
    public void GetTestReturnsNullWhenItemDoesNotExist()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var maintenanceRecord = repositories.MaintenanceRecordsRepository.Get(99);

      // Assert
      Assert.IsNull(maintenanceRecord);
    }

    /// <summary>
    /// Verify that maintenance records can be updated
    /// </summary>
    [Test]
    public void UpdateTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var maintenanceRecord = repositories.MaintenanceRecordsRepository.Get(1);

      var updatedDate = new DateTime(2015, 06, 13, 7, 0, 0);

      maintenanceRecord.Date = updatedDate;
      repositories.MaintenanceRecordsRepository.Update(maintenanceRecord);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var updatedMaintenanceRecord = repositories.MaintenanceRecordsRepository.Get(1);

      Assert.AreEqual(updatedDate, updatedMaintenanceRecord.Date);
    }
  }
}