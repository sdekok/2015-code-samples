﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleTypeRepositoryTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Test Vehicle Type Repository
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.DAL
{
  using System.Linq;

  using AutomobileMaintenance;
  using AutomobileMaintenance.Models;

  using NUnit.Framework;

  /// <summary>
  /// Test Vehicle Type Repository
  /// </summary>
  [TestFixture]
  public class VehicleTypeRepositoryTests
  {
    /// <summary>
    /// Perform setup tasks that need to be done before running tests
    /// </summary>
    [SetUp]
    public void SetupTests()
    {
      AutoMapperConfig.RegisterMappings();
    }

    /// <summary>
    /// Verify that a maintenance task that already exists can be added to a vehicle type
    /// </summary>
    [Test]
    public void AddMaintenanceTaskTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      repositories.VehicleTypeRepository.AddMaintenanceTask(1, 5);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });
      var maintenanceTaskCount = repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count;
      Assert.AreEqual(4, maintenanceTaskCount);
    }

    /// <summary>
    /// Verify that a new vehicle can be added
    /// </summary>
    [Test]
    public void AddTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      var vehicleType = new VehicleTypeDto { VehicleTypeId = null, Name = "Magic School Bus" };

      repositories.VehicleTypeRepository.Add(vehicleType);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var id = repositories.VehicleTypeRepository.FindId(vehicleType);

      Assert.NotNull(id);
      Assert.AreEqual(5, id);
    }

    /// <summary>
    /// Verify that a vehicle can be deleted successfully
    /// </summary>
    [Test]
    public void DeleteTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();
      var count = repositories.VehicleTypeRepository.GetAll().Count();

      repositories.VehicleTypeRepository.Delete(1);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      Assert.AreEqual(count - 1, repositories.VehicleTypeRepository.GetAll().Count());

      // Repeat to verify that once it's gone, it doesn't delete something else
      repositories.VehicleTypeRepository.Delete(1);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      Assert.AreEqual(count - 1, repositories.VehicleTypeRepository.GetAll().Count());
    }

    /// <summary>
    /// Verify that the id can be determined for a given vehicle type
    /// </summary>
    [Test]
    public void FindIdTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();
      var vehicleToFind = repositories.VehicleTypeRepository.Get(1);
      vehicleToFind.VehicleTypeId = null;

      Assert.AreEqual(1, repositories.VehicleTypeRepository.FindId(vehicleToFind));
    }

    /// <summary>
    /// Verify that the collection of maintenance tasks valid for a vehicle type can be retrieved
    /// </summary>
    [Test]
    public void GetAllMaintenanceTasksTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();
      var maintenanceTaskCount = repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count;
      Assert.AreEqual(3, maintenanceTaskCount);
    }

    /// <summary>
    /// Verify that collection of all vehicles is correctly returned
    /// </summary>
    [Test]
    public void GetAllTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      Assert.AreEqual(4, repositories.VehicleTypeRepository.GetAll().Count());
    }

    /// <summary>
    /// Verify that correct vehicle is returned
    /// </summary>
    [Test]
    public void GetTest()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var vehicleType = repositories.VehicleTypeRepository.Get(1);

      // Assert
      Assert.IsNotNull(vehicleType);
      Assert.AreEqual(1, vehicleType.VehicleTypeId);
    }

    /// <summary>
    /// Verify that correct vehicle is returned
    /// </summary>
    [Test]
    public void GetTestReturnsNullWhenItemDoesNotExist()
    {
      // Setup Database and Repository
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();

      // Run
      var vehicleType = repositories.VehicleTypeRepository.Get(7);

      Assert.IsNull(vehicleType);
    }

    /// <summary>
    /// Verify that a maintenance task can be removed from a vehicle type
    /// </summary>
    [Test]
    public void RemoveMaintenanceTaskTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();
      var maintenanceTaskCount = repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count;

      // Delete maintenance task
      repositories.VehicleTypeRepository.RemoveMaintenanceTask(1, 1);
      Assert.AreEqual(maintenanceTaskCount - 1, repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count);

      // Repeat deletion to verify that no errors are thrown
      repositories.VehicleTypeRepository.RemoveMaintenanceTask(1, 1);
      Assert.AreEqual(maintenanceTaskCount - 1, repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count);
    }

    /// <summary>
    /// Verify that a vehicle can be updated successfully
    /// </summary>
    [Test]
    public void UpdateTest()
    {
      var repositories = RepositoryTestCollectionSetup.GetLoadedRepository();
      var vehicleTypeToUpdate = repositories.VehicleTypeRepository.Get(1);

      vehicleTypeToUpdate.Name = "Magic School Bus 2";
      var maintenanceTasks = repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1);

      repositories.VehicleTypeRepository.Update(vehicleTypeToUpdate);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      Assert.AreEqual("Magic School Bus 2", repositories.VehicleTypeRepository.Get(1).Name);
      Assert.AreEqual(maintenanceTasks.Count(), repositories.VehicleTypeRepository.GetAllMaintenanceTasks(1).Count());

      var newVehicleType = new VehicleTypeDto { VehicleTypeId = null, Name = "Magic School Bus" };

      repositories.VehicleTypeRepository.Update(newVehicleType);
      Assert.DoesNotThrow(delegate { repositories.SaveChanges(); });

      var id = repositories.VehicleTypeRepository.FindId(newVehicleType);

      Assert.IsNull(id);
    }
  }
}