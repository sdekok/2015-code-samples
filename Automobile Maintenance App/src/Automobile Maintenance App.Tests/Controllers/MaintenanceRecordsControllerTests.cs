﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceRecordsControllerTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Maintenance Records Controller Tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Controllers
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Web.Http.Results;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;

  using Moq;

  using MyTested.WebApi;

  using NUnit.Framework;

  /// <summary>
  /// Maintenance Records Controller Tests.
  /// </summary>
  [TestFixture]
  public class MaintenanceRecordsControllerTests
  {
    /// <summary>
    /// Setup tasks that are required before running any tests
    /// </summary>
    [SetUp]
    public void SetUpTests()
    {
      this.maintenanceRecords = new List<MaintenanceRecordDto>();
      this.maintenanceRecords.Add(
        new MaintenanceRecordDto
          {
            Date = new DateTime(2014, 02, 20, 6, 2, 0),
            MaintenanceTaskId = 1,
            MaintenanceRecordId = 1,
            Mileage = 70000,
            Notes = "This is a note",
            VehicleId = 1
          });
    }

    /// <summary>
    /// The maintenance records.
    /// </summary>
    private List<MaintenanceRecordDto> maintenanceRecords;

    /// <summary>
    /// Add Maintenance Record
    /// </summary>
    [Test]
    public void AddMaintenanceRecordTest()
    {
      var collectionMock = new Mock<IUnitOfWork>();

      var mock = new Mock<IMaintenanceRecordsRepository>();
      var vehicleMock = new Mock<IVehicleRepository>();
      var vehicleTypeMock = new Mock<IVehicleTypeRepository>();

      var existing = new MaintenanceRecordDto
                       {
                         Date = new DateTime(2014, 02, 20, 6, 2, 0),
                         MaintenanceTaskId = 1,
                         MaintenanceRecordId = 1,
                         Mileage = 70000,
                         Notes = "This is a note",
                         VehicleId = 1
                       };

      var toAdd = new MaintenanceRecordDto
                    {
                      Date = new DateTime(2015, 02, 20, 6, 2, 0),
                      MaintenanceTaskId = 2,
                      Mileage = 70000,
                      Notes = "Second Maintenance Event",
                      VehicleId = 1
                    };

      var invalidMaintenanceTask = new MaintenanceRecordDto
                                     {
                                       Date = new DateTime(2015, 02, 20, 6, 2, 0),
                                       MaintenanceTaskId = 3,
                                       Mileage = 70000,
                                       Notes = "Second Maintenance Event",
                                       VehicleId = 1
                                     };

      var vehicle = new VehicleDto
                      {
                        VehicleId = 1,
                        VehicleTypeId = 1,
                        Make = "Ford",
                        Model = "F150",
                        Year = 2015,
                        Name = "Ford F150"
                      };

      var validMaintenanceTasks = new List<MaintenanceTaskDto>();

      validMaintenanceTasks.Add(
        new MaintenanceTaskDto
          {
            Description = "Good Maintenance",
            Name = "fixer",
            MaintenanceTaskId = 1
          });

      validMaintenanceTasks.Add(
        new MaintenanceTaskDto
          {
            MaintenanceTaskId = 2,
            Name = "upper",
            Description = "Bad Maintenance"
          });

      vehicleTypeMock.Setup(x => x.GetAllMaintenanceTasks(1)).Returns(validMaintenanceTasks);

      vehicleMock.Setup(x => x.Get(1)).Returns(vehicle);

      mock.Setup(x => x.Get(1)).Returns(existing);
      mock.Setup(x => x.Add(toAdd));
      mock.Setup(x => x.FindId(toAdd)).Returns(2);

      collectionMock.Setup(x => x.MaintenanceRecordsRepository).Returns(mock.Object);
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleMock.Object);
      collectionMock.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeMock.Object);

      var maintenanceRecordController = new MaintenanceRecordsController(collectionMock.Object);

      var result = maintenanceRecordController.PostMaintenanceRecord(invalidMaintenanceTask);

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);

      result = maintenanceRecordController.PostMaintenanceRecord(toAdd);

      Assert.IsInstanceOf<CreatedAtRouteNegotiatedContentResult<MaintenanceRecordDto>>(result);
    }

    /// <summary>
    /// Test record is deleted successfully
    /// </summary>
    [Test]
    public void DeleteMaintenanceRecordTest()
    {
      var collectionMock = new Mock<IUnitOfWork>();

      var mock = new Mock<IMaintenanceRecordsRepository>();

      var toReturn = new MaintenanceRecordDto
                       {
                         Date = new DateTime(2014, 02, 20, 6, 2, 0),
                         MaintenanceTaskId = 1,
                         MaintenanceRecordId = 1,
                         Mileage = 70000,
                         Notes = "This is a note",
                         VehicleId = 1
                       };

      MaintenanceRecordDto nullReturn = null;

      mock.Setup(x => x.Get(1)).Returns(toReturn);
      mock.Setup(x => x.Get(2)).Returns(nullReturn);

      mock.Setup(x => x.Delete(1));
      mock.Setup(x => x.Delete(2));

      collectionMock.Setup(x => x.MaintenanceRecordsRepository).Returns(mock.Object);

      var maintenanceRecordController = new MaintenanceRecordsController(collectionMock.Object);

      var result = maintenanceRecordController.DeleteMaintenanceRecord(1);

      Assert.DoesNotThrow(delegate { mock.Verify(x => x.Get(1)); });
      Assert.DoesNotThrow(delegate { mock.Verify(x => x.Delete(1)); });
      Assert.DoesNotThrow(delegate { collectionMock.Verify(x => x.SaveChanges()); });

      Assert.IsInstanceOf<OkNegotiatedContentResult<MaintenanceRecordDto>>(result);
    }

    /// <summary>
    /// Test invalid record is not deleted
    /// </summary>
    [Test]
    public void DeleteMaintenanceRecordTestFailure()
    {
      var collectionMock = new Mock<IUnitOfWork>();

      var mock = new Mock<IMaintenanceRecordsRepository>();

      var toReturn = new MaintenanceRecordDto
                       {
                         Date = new DateTime(2014, 02, 20, 6, 2, 0),
                         MaintenanceTaskId = 1,
                         MaintenanceRecordId = 1,
                         Mileage = 70000,
                         Notes = "This is a note",
                         VehicleId = 1
                       };

      MaintenanceRecordDto nullReturn = null;

      mock.Setup(x => x.Get(1)).Returns(toReturn);
      mock.Setup(x => x.Get(2)).Returns(nullReturn);

      mock.Setup(x => x.Delete(1));
      mock.Setup(x => x.Delete(2));

      collectionMock.Setup(x => x.MaintenanceRecordsRepository).Returns(mock.Object);

      var maintenanceRecordController = new MaintenanceRecordsController(collectionMock.Object);

      var result = maintenanceRecordController.DeleteMaintenanceRecord(2);

      Assert.DoesNotThrow(delegate { mock.Verify(x => x.Get(2)); });
      Assert.Throws<MockException>(delegate { mock.Verify(x => x.Delete(2)); });

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Test that all all items are successfully returned
    /// </summary>
    [Test]
    public void GetAllMaintenanceRecordsTest()
    {
      var collectionMock = new Mock<IUnitOfWork>();

      var mock = new Mock<IMaintenanceRecordsRepository>();

      mock.Setup(x => x.GetAll()).Returns(this.maintenanceRecords);

      collectionMock.Setup(x => x.MaintenanceRecordsRepository).Returns(mock.Object);

      var maintenanceRecordController = new MaintenanceRecordsController(collectionMock.Object);

      var result = maintenanceRecordController.GetMaintenanceRecords();

      Assert.DoesNotThrow(delegate { mock.Verify(x => x.GetAll()); });
      Assert.AreEqual(this.maintenanceRecords.AsQueryable(), result);
    }

    /// <summary>
    /// Test that the particular item requested is returned correctly
    /// </summary>
    [Test]
    public void GetMaintenanceRecordTest()
    {
      var collectionMock = new Mock<IUnitOfWork>();

      var mock = new Mock<IMaintenanceRecordsRepository>();

      var toReturn = new MaintenanceRecordDto
                       {
                         Date = new DateTime(2014, 02, 20, 6, 2, 0),
                         MaintenanceTaskId = 1,
                         MaintenanceRecordId = 1,
                         Mileage = 70000,
                         Notes = "This is a note",
                         VehicleId = 1
                       };

      mock.Setup(x => x.Get(1)).Returns(toReturn);

      collectionMock.Setup(x => x.MaintenanceRecordsRepository).Returns(mock.Object);

      var maintenanceRecordController = new MaintenanceRecordsController(collectionMock.Object);

      var result = maintenanceRecordController.GetMaintenanceRecord(1);

      Assert.IsInstanceOf<OkNegotiatedContentResult<MaintenanceRecordDto>>(result);

      var content = result as OkNegotiatedContentResult<MaintenanceRecordDto>;

      Assert.AreEqual(toReturn, content?.Content);
    }

    /// <summary>
    /// Test that if the item is not found, it returns a not found result
    /// </summary>
    [Test]
    public void GetMaintenanceRecordTestNotFound()
    {
      var collectionMock = new Mock<IUnitOfWork>();

      var mock = new Mock<IMaintenanceRecordsRepository>();

      collectionMock.Setup(x => x.MaintenanceRecordsRepository).Returns(mock.Object);

      var maintenanceRecordController = new MaintenanceRecordsController(collectionMock.Object);

      MaintenanceRecordDto nullReturn = null;

      mock.Setup(x => x.Get(1)).Returns(nullReturn);

      var result = maintenanceRecordController.GetMaintenanceRecord(1);
      Assert.DoesNotThrow(delegate {mock.Verify(x => x.Get(1));});
      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Verify that maintenance records can be updated successfully
    /// </summary>
    [Test]
    public void UpdateMaintenanceRecordTest()
    {
      var collectionMock = new Mock<IUnitOfWork>();

      var mock = new Mock<IMaintenanceRecordsRepository>();
      var vehicleMock = new Mock<IVehicleRepository>();
      var vehicleTypeMock = new Mock<IVehicleTypeRepository>();

      var toReturn = new MaintenanceRecordDto
                       {
                         Date = new DateTime(2014, 02, 20, 6, 2, 0),
                         MaintenanceTaskId = 1,
                         MaintenanceRecordId = 1,
                         Mileage = 70000,
                         Notes = "This is a note",
                         VehicleId = 1
                       };

      var vehicle = new VehicleDto
                      {
                        VehicleId = 1,
                        VehicleTypeId = 1,
                        Make = "Ford",
                        Model = "F150",
                        Year = 2015,
                        Name = "Ford F150"
                      };

      var validMaintenanceTasks = new List<MaintenanceTaskDto>();

      validMaintenanceTasks.Add(
        new MaintenanceTaskDto
          {
            Description = "Good Maintenance",
            Name = "fixer",
            MaintenanceTaskId = 1
          });

      validMaintenanceTasks.Add(
        new MaintenanceTaskDto
          {
            MaintenanceTaskId = 2,
            Name = "upper",
            Description = "Bad Maintenance"
          });

      vehicleTypeMock.Setup(x => x.GetAllMaintenanceTasks(1)).Returns(validMaintenanceTasks);

      vehicleMock.Setup(x => x.Get(1)).Returns(vehicle);

      var updatedRecord = toReturn;
      updatedRecord.MaintenanceTaskId = 2;

      mock.Setup(x => x.Get(1)).Returns(toReturn);
      mock.Setup(x => x.Update(updatedRecord));

      collectionMock.Setup(x => x.MaintenanceRecordsRepository).Returns(mock.Object);
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleMock.Object);
      collectionMock.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeMock.Object);

      var maintenanceRecordController = new MaintenanceRecordsController(collectionMock.Object);

      var result = maintenanceRecordController.PutMaintenanceRecord(2, updatedRecord);

      Assert.IsInstanceOf<BadRequestResult>(result);

      result = maintenanceRecordController.PutMaintenanceRecord(1, updatedRecord);

      Assert.IsInstanceOf<StatusCodeResult>(result);

      updatedRecord.MaintenanceTaskId = 3;
      result = maintenanceRecordController.PutMaintenanceRecord(1, updatedRecord);
      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }
  }
}