﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceTasksControllerTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Tests for the MaintenanceTasks Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Controllers
{
  using System.Linq;
  using System.Web.Http.Results;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;
  using AutomobileMaintenance.Tests.Mocks;

  using Moq;

  using NUnit.Framework;

  /// <summary>
  /// Tests for the MaintenanceTasks Controller
  /// </summary>
  [TestFixture]
  public class MaintenanceTasksControllerTests
  {
    /// <summary>
    /// Verify that Maintenance Task Controller can successfully delete a task if it exists in the dtabase
    /// </summary>
    [Test]
    public void DeleteMaintenanceTaskTest()
    {
      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();

      MaintenanceTaskDto nullReturn = null;

      maintenanceTasksRepository.Setup(x => x.Get(1)).Returns(nullReturn);
      maintenanceTasksRepository.Setup(x => x.Get(2)).Returns(MockRepositoryCollection.MaintenanceTasks[1]);
      maintenanceTasksRepository.Setup(x => x.Delete(1));
      maintenanceTasksRepository.Setup(x => x.Delete(2));

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.DeleteMaintenanceTask(2);

      Assert.DoesNotThrow(delegate { maintenanceTasksRepository.Verify(x => x.Get(2)); });
      Assert.DoesNotThrow(delegate { maintenanceTasksRepository.Verify(x => x.Delete(2)); });
      Assert.DoesNotThrow(delegate { repositoryCollection.Verify(x => x.SaveChanges()); });

      Assert.IsInstanceOf<OkNegotiatedContentResult<MaintenanceTaskDto>>(result);
    }

    /// <summary>
    /// Verify that the controller returns a NotFoundResult when trying to delete an item that does not exist
    /// </summary>
    [Test]
    public void DeleteMaintenanceTaskTestReturnsNotFoundIfItemDoesNotExist()
    {
      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();

      MaintenanceTaskDto nullReturn = null;

      maintenanceTasksRepository.Setup(x => x.Get(1)).Returns(nullReturn);
      maintenanceTasksRepository.Setup(x => x.Get(2)).Returns(MockRepositoryCollection.MaintenanceTasks[1]);
      maintenanceTasksRepository.Setup(x => x.Delete(1));
      maintenanceTasksRepository.Setup(x => x.Delete(2));

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.DeleteMaintenanceTask(1);

      Assert.DoesNotThrow(delegate { maintenanceTasksRepository.Verify(x => x.Get(1)); });
      Assert.Throws<MockException>(delegate { maintenanceTasksRepository.Verify(x => x.Delete(1)); });

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Verify that MaintenanceTasks can be retrieved successfully
    /// </summary>
    [Test]
    public void GetMaintenanceTasksTest()
    {
      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.GetMaintenanceTasks();

      Assert.IsNotNull(result);
      Assert.IsInstanceOf<IQueryable<MaintenanceTaskDto>>(result);
      Assert.AreEqual(MockRepositoryCollection.MaintenanceTasks.Count, result.Count());
    }

    /// <summary>
    /// Verify that a particular maintenance task can be retrieved successfully
    /// </summary>
    [Test]
    public void GetMaintenanceTaskTest()
    {
      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTasksRepository.Setup(x => x.Get(1)).Returns(MockRepositoryCollection.MaintenanceTasks[0]);

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.GetMaintenanceTask(1);

      Assert.IsInstanceOf<OkNegotiatedContentResult<MaintenanceTaskDto>>(result);

      var content = (result as OkNegotiatedContentResult<MaintenanceTaskDto>).Content;

      Assert.AreEqual(MockRepositoryCollection.MaintenanceTasks[0], content);

      result = maintenanceTasks.GetMaintenanceTask(2);

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Verify that a maintenance task controller returns not found result when requested item is not in database
    /// </summary>
    [Test]
    public void GetMaintenanceTaskTestReturnsNotFoundResultWhenItemDoesNotExist()
    {
      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTasksRepository.Setup(x => x.Get(1)).Returns(MockRepositoryCollection.MaintenanceTasks[0]);

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.GetMaintenanceTask(2);

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Verify that maintenance tasks can be added successfully
    /// </summary>
    [Test]
    public void PostMaintenanceTaskTest()
    {
      var toUpdate = MockRepositoryCollection.MaintenanceTasks[1];
      toUpdate.Name = "Test Name";

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTasksRepository.Setup(x => x.Get(2)).Returns(MockRepositoryCollection.MaintenanceTasks[1]);

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      toUpdate.MaintenanceTaskId = null;
      var result = maintenanceTasks.PostMaintenanceTask(toUpdate);

      Assert.IsInstanceOf<CreatedAtRouteNegotiatedContentResult<MaintenanceTaskDto>>(result);
    }

    /// <summary>
    /// Verify that maintenance task is not added if it already exists
    /// </summary>
    [Test]
    public void PostMaintenanceTaskTestFailsIfTaskAlreadyExists()
    {
      var toUpdate = MockRepositoryCollection.MaintenanceTasks[1];
      toUpdate.Name = "Test Name";

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTasksRepository.Setup(x => x.Get(2)).Returns(MockRepositoryCollection.MaintenanceTasks[1]);

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.PostMaintenanceTask(toUpdate);

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that maintenance tasks can be saved successfully
    /// </summary>
    [Test]
    public void PutMaintenanceTaskTest()
    {
      var toUpdate = MockRepositoryCollection.MaintenanceTasks[1];
      toUpdate.Name = "Test Name";

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTasksRepository.Setup(x => x.Get(2)).Returns(MockRepositoryCollection.MaintenanceTasks[1]);

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.PutMaintenanceTask(3, toUpdate);

      Assert.IsInstanceOf<BadRequestResult>(result);

      result = maintenanceTasks.PutMaintenanceTask(2, toUpdate);

      Assert.IsInstanceOf<StatusCodeResult>(result);
    }

    /// <summary>
    /// Verify that maintenance task controller returns a BadRequestResult when the object and parameter id's don't match
    /// </summary>
    [Test]
    public void PutMaintenanceTaskTestFailsWithIdMismatch()
    {
      var toUpdate = MockRepositoryCollection.MaintenanceTasks[1];
      toUpdate.Name = "Test Name";

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var maintenanceTasksRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTasksRepository.Setup(x => x.Get(2)).Returns(MockRepositoryCollection.MaintenanceTasks[1]);

      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTasksRepository.Object);

      var maintenanceTasks = new MaintenanceTasksController(repositoryCollection.Object);

      var result = maintenanceTasks.PutMaintenanceTask(3, toUpdate);

      Assert.IsInstanceOf<BadRequestResult>(result);
    }
  }
}