﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleTypesControllerTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Vehicle Type Controller Tests
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Controllers
{
  using System.Linq;
  using System.Web.Http.Results;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;
  using AutomobileMaintenance.Tests.Mocks;

  using Moq;

  using NUnit.Framework;

  /// <summary>
  /// Vehicle Type Controller Tests
  /// </summary>
  [TestFixture]
  public class VehicleTypesControllerTests
  {
    /// <summary>
    /// Verify that a vehicle type will not be added if it already exists.
    /// </summary>
    /// <param name="newVehicleTypeId"></param>
    [TestCase(0)]
    [TestCase(null)]
    [TestCase(1)]
    [TestCase(99)]
    public void PostVehicleTypeFailureItemAlreadyExists(int? newVehicleTypeId)
    {
      var itemToRequest = 1;
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      var newVehicleType = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      newVehicleType.VehicleTypeId = newVehicleTypeId;

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      var id = 1;

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(id);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PostVehicleType(newVehicleType);

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that a vehicle type will be added if it does not already exist
    /// </summary>
    /// <param name="newVehicleTypeId"></param>
    [TestCase(0)]
    [TestCase(null)]
    [TestCase(1)]
    [TestCase(99)]
    public void PostVehicleTypeTestSuccess(int? newVehicleTypeId)
    {
      var itemToRequest = 1;
      var testName = "Test Name";
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      var newVehicleType = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      newVehicleType.Name = testName;
      newVehicleType.VehicleTypeId = newVehicleTypeId;

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      var id = 0;

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(
        delegate
          {
            if (id == 0)
            {
              id++;
              return null;
            }
            return id++;
          });

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PostVehicleType(newVehicleType);

      Assert.IsInstanceOf<CreatedAtRouteNegotiatedContentResult<VehicleTypeDto>>(result);
    }

    /// <summary>
    /// Verify that a maintenance tasks will not be removed if the it is not already associated with the vehicle type
    /// </summary>
    [Test]
    public void DeleteMaintenanceTaskTestFailureMaintenanceTaskIsNotAssociated()
    {
      var itemToDelete = 1;
      VehicleTypeDto toDelete = null;

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToDelete)).Returns(toDelete);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));

      vehicleTypeRepository.Setup(x => x.GetAllMaintenanceTasks(It.IsAny<int>()))
        .Returns(MockRepositoryCollection.MaintenanceTasks);

      vehicleTypeRepository.Setup(x => x.RemoveMaintenanceTask(itemToDelete, It.IsAny<int>()));

      var maintenanceTaskRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTaskRepository.Setup(x => x.Get(It.IsAny<int>())).Returns(null as MaintenanceTaskDto);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PostMaintenanceTask(itemToDelete, 2);

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Verify that the vehicle maintenance task can be deleted successfully from a vehicle type
    /// </summary>
    [Test]
    public void DeleteMaintenanceTaskTestSuccess()
    {
      var itemToDelete = 1;
      var toDelete = MockRepositoryCollection.VehicleTypes[itemToDelete - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToDelete)).Returns(toDelete);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(toDelete.VehicleTypeId);

      vehicleTypeRepository.Setup(x => x.GetAllMaintenanceTasks(It.IsAny<int>()))
        .Returns(MockRepositoryCollection.MaintenanceTasks);

      vehicleTypeRepository.Setup(x => x.RemoveMaintenanceTask(itemToDelete, It.IsAny<int>()));

      var maintenanceTaskRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTaskRepository.Setup(x => x.Get(It.IsAny<int>())).Returns(null as MaintenanceTaskDto);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.DeleteMaintenanceTask(itemToDelete, 2);

      Assert.IsInstanceOf<OkNegotiatedContentResult<MaintenanceTaskDto>>(result);
    }

    /// <summary>
    /// Verify that a vehicle type controller will return a not found status if vehicle type does not exist
    /// </summary>
    [Test]
    public void DeleteVehicleTypeTestFailureDoesNotExist()
    {
      var itemToDelete = 1;
      var toDelete = MockRepositoryCollection.VehicleTypes[itemToDelete - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToDelete)).Returns(null as VehicleTypeDto);
      vehicleTypeRepository.Setup(x => x.Delete(It.IsAny<int>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(null as int?);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.DeleteVehicleType(itemToDelete);

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Verify that a vehicle type is successfully deleted if the vehicle exists
    /// </summary>
    [Test]
    public void DeleteVehicleTypeTestSuccess()
    {
      var itemToDelete = 1;
      var toDelete = MockRepositoryCollection.VehicleTypes[itemToDelete - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToDelete)).Returns(toDelete);
      vehicleTypeRepository.Setup(x => x.Delete(It.IsAny<int>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(null as int?);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.DeleteVehicleType(itemToDelete);

      Assert.IsInstanceOf<OkNegotiatedContentResult<VehicleTypeDto>>(result);
    }

    /// <summary>
    /// Verify that a object containing all associated maintenance tasks can be retrieved for a vehicle type
    /// </summary>
    [Test]
    public void GetMaintenanceTasksTest()
    {
      var vehicleTypeList = MockRepositoryCollection.VehicleTypes;
      var maintenanceTaskList = MockRepositoryCollection.MaintenanceTasks.Where(x => x.MaintenanceTaskId < 3).ToList();

      var vehicleTypeRespository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRespository.Setup(x => x.GetAllMaintenanceTasks(It.IsAny<int>())).Returns(maintenanceTaskList);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRespository.Object);

      var controller = new VehicleTypesController(repositoryCollection.Object);

      Assert.AreEqual(maintenanceTaskList.AsQueryable(), controller.GetMaintenanceTasks(1));
    }

    /// <summary>
    /// Verify Vehicle Types test returns collection
    /// </summary>
    [Test]
    public void GetVehicleTypesTest()
    {
      var vehicleTypeList = MockRepositoryCollection.VehicleTypes;
      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var controller = new VehicleTypesController(repositoryCollection.Object);

      Assert.AreEqual(vehicleTypeList.AsQueryable(), controller.GetVehicleTypes());
    }

    /// <summary>
    /// Verify that the vehicle type controller returns a not found result
    /// when trying to delete a vehicle type that does not exist
    /// </summary>
    [Test]
    public void GetVehicleTypeTestFailure()
    {
      var itemToRequest = 1;
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.GetVehicleType(itemToRequest + 1);

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// verify that a specified vehicle type is returned if it exists
    /// </summary>
    [Test]
    public void GetVehicleTypeTestSuccess()
    {
      var itemToRequest = 1;
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.GetVehicleType(itemToRequest);

      Assert.IsInstanceOf<OkNegotiatedContentResult<VehicleTypeDto>>(result);
      Assert.AreEqual(toReturn, (result as OkNegotiatedContentResult<VehicleTypeDto>).Content);
    }

    /// <summary>
    /// Verify that maintenance tasks are added successfully
    /// </summary>
    [Test]
    public void PostMaintenanceTaskTest()
    {
      var itemToRequest = 1;
      var testName = "Test Name";
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      var newVehicleType = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      newVehicleType.Name = testName;
      newVehicleType.VehicleTypeId = 0;

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      var id = 0;

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(
        delegate
          {
            if (id == 0)
            {
              id++;
              return null;
            }
            return id++;
          });

      vehicleTypeRepository.Setup(x => x.AddMaintenanceTask(itemToRequest, It.IsAny<int>()));

      var maintenanceTaskRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTaskRepository.Setup(x => x.Get(It.IsAny<int>())).Returns(MockRepositoryCollection.MaintenanceTasks[1]);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);
      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTaskRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PostMaintenanceTask(itemToRequest, 2);

      Assert.IsInstanceOf<StatusCodeResult>(result);
    }

    /// <summary>
    /// Verify that maintenance tasks are not added to a vehicle type if the task
    /// already exists
    /// </summary>
    [Test]
    public void PostMaintenanceTaskTestFailureAlreadyExists()
    {
      var itemToRequest = 1;
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      var newVehicleType = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(newVehicleType.VehicleTypeId);

      vehicleTypeRepository.Setup(x => x.GetAllMaintenanceTasks(It.IsAny<int>()))
        .Returns(MockRepositoryCollection.MaintenanceTasks);

      vehicleTypeRepository.Setup(x => x.AddMaintenanceTask(itemToRequest, It.IsAny<int>()));

      var maintenanceTaskRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTaskRepository.Setup(x => x.Get(It.IsAny<int>())).Returns(MockRepositoryCollection.MaintenanceTasks[1]);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);
      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTaskRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PostMaintenanceTask(itemToRequest, 2);

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that maintenance task is not associated when maintenance task does not exist
    /// </summary>
    [Test]
    public void PostMaintenanceTaskTestMaintenanceTaskDoesNotExist()
    {
      var itemToRequest = 1;
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      var newVehicleType = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(newVehicleType.VehicleTypeId);

      vehicleTypeRepository.Setup(x => x.GetAllMaintenanceTasks(It.IsAny<int>()))
        .Returns(MockRepositoryCollection.MaintenanceTasks);

      vehicleTypeRepository.Setup(x => x.AddMaintenanceTask(itemToRequest, It.IsAny<int>()));

      var maintenanceTaskRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTaskRepository.Setup(x => x.Get(It.IsAny<int>())).Returns(null as MaintenanceTaskDto);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);
      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTaskRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PostMaintenanceTask(itemToRequest, 2);

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that maintenance tasks cannot be added to a vehilce type that does not exist
    /// </summary>
    [Test]
    public void PostMaintenanceTaskTestVehicleTypeDoesNotExist()
    {
      var itemToRequest = 1;
      VehicleTypeDto toReturn = null;
      var newVehicleType = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Add(It.IsAny<VehicleTypeDto>()));
      vehicleTypeRepository.Setup(x => x.FindId(It.IsAny<VehicleTypeDto>())).Returns(newVehicleType.VehicleTypeId);

      vehicleTypeRepository.Setup(x => x.GetAllMaintenanceTasks(It.IsAny<int>()))
        .Returns(MockRepositoryCollection.MaintenanceTasks);

      vehicleTypeRepository.Setup(x => x.AddMaintenanceTask(itemToRequest, It.IsAny<int>()));

      var maintenanceTaskRepository = new Mock<IMaintenanceTasksRepository>();
      maintenanceTaskRepository.Setup(x => x.Get(It.IsAny<int>())).Returns(MockRepositoryCollection.MaintenanceTasks[1]);

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);
      repositoryCollection.Setup(x => x.MaintenanceTasksRepository).Returns(maintenanceTaskRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PostMaintenanceTask(itemToRequest, 2);

      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// verify that a vehilce type that does not exist cannot be updated
    /// </summary>
    [Test]
    public void PutVehicleTypeTestFailureItemDoesNotExist()
    {
      var itemToRequest = 1;
      var testName = "Test Name";
      VehicleTypeDto toReturn = null;
      var updated = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      updated.Name = testName;

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Update(It.IsAny<VehicleTypeDto>()));

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PutVehicleType(itemToRequest, updated);

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that a a bad request error code is returned if parameter id and
    /// object id's do not match
    /// </summary>
    [Test]
    public void PutVehicleTypeTestFailureItemIdsDoNotMatch()
    {
      var itemToRequest = 1;
      var testName = "Test Name";
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      var updated = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      updated.Name = testName;

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Update(It.IsAny<VehicleTypeDto>()));

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PutVehicleType(itemToRequest + 1, updated);

      Assert.IsInstanceOf<BadRequestResult>(result);
    }

    /// <summary>
    /// Verify that a vehile type can be successfully updated
    /// </summary>
    [Test]
    public void PutVehicleTypeTestSuccess()
    {
      var itemToRequest = 1;
      var testName = "Test Name";
      var toReturn = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      var updated = MockRepositoryCollection.VehicleTypes[itemToRequest - 1];
      updated.Name = testName;

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleTypeRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);
      vehicleTypeRepository.Setup(x => x.Update(It.IsAny<VehicleTypeDto>()));

      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();
      repositoryCollection.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehicleTypesController = new VehicleTypesController(repositoryCollection.Object);

      var result = vehicleTypesController.PutVehicleType(itemToRequest, updated);

      Assert.IsInstanceOf<StatusCodeResult>(result);
    }
  }
}