﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehiclesControllerTests.cs" company="Stephen de Kok">
//   Copyright (c) 2015 Stephen de Kok
// </copyright>
// <summary>
//   Tests for the Vehicles Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Controllers
{
  using System.Linq;
  using System.Web.Http.Results;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.DAL;
  using AutomobileMaintenance.Models;
  using AutomobileMaintenance.Tests.Mocks;

  using Moq;

  using NUnit.Framework;

  /// <summary>
  /// Test Vehicles Controller
  /// </summary>
  [TestFixture]
  public class VehiclesControllerTests
  {
    /// <summary>
    /// Verify that vehicles with invalid vehicle types don't succeed
    /// </summary>
    /// <param name="vehicleTypeId">VehicleTypeId to test with</param>
    [TestCase(0)]
    [TestCase(80)]
    public void PostVehicleFailureVehicleTypeIdInvalid(int vehicleTypeId)
    {
      var vehicleToAdd = new VehicleDto
                           {
                             VehicleId = null,
                             Make = "Dodge",
                             Model = "Ram 150",
                             Name = "Ram 150",
                             VehicleTypeId = vehicleTypeId,
                             VehicleTypeName = null,
                             Year = 1986
                           };

      var id = 0;

      var vehicleRepository = new Mock<IVehicleRepository>();
      vehicleRepository.Setup(x => x.FindId(It.IsAny<VehicleDto>())).Returns(
        delegate
          {
            if (id == 0)
            {
              id++;
              return null;
            }
            return id++;
          });

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();
      vehicleTypeRepository.Setup(x => x.Get(vehicleToAdd.VehicleTypeId))
        .Returns(
          MockRepositoryCollection.VehicleTypes.FirstOrDefault(x => x.VehicleTypeId == vehicleToAdd.VehicleTypeId));

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Act

      var result = vehiclesController.PostVehicle(vehicleToAdd);

      //Assert
      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that a vehicle cannot be updated if the vehicle type does not exist
    /// </summary>
    [TestCase(99)]
    [TestCase(0)]
    public void PutVehicleTestFailureOnIllegalVehicleType(int badVehicleTypeId)
    {
      //Arrange
      var itemToRequest = 1;

      var vehicleToReturn = MockRepositoryCollection.Vehicles[itemToRequest - 1];
      var vehicleToUpdate = MockRepositoryCollection.Vehicles[itemToRequest - 1];
      vehicleToUpdate.VehicleTypeId = badVehicleTypeId;

      var vehicleRepository = new Mock<IVehicleRepository>();
      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleRepository.Setup(x => x.Get(itemToRequest)).Returns(vehicleToReturn);
      vehicleRepository.Setup(x => x.Update(It.IsAny<VehicleDto>()));
      vehicleTypeRepository.Setup(x => x.Get(vehicleToReturn.VehicleTypeId));

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.PutVehicle(itemToRequest, vehicleToUpdate);

      //Assert

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that Vehicle Controller returns a not found result when trying to delete a vehicle that does not exist
    /// </summary>
    [Test]
    public void DeleteVehicleTestFailureVehicleDoesNotExist()
    {
      var vehicleToDelete = MockRepositoryCollection.Vehicles[0];
      VehicleDto nullVehicle = null;

      var vehicleRepository = new Mock<IVehicleRepository>();
      vehicleRepository.Setup(x => x.FindId(It.IsAny<VehicleDto>())).Returns(vehicleToDelete.VehicleId);
      vehicleRepository.Setup(x => x.Get(vehicleToDelete.VehicleTypeId)).Returns(nullVehicle);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Act

      var result = vehiclesController.DeleteVehicle(0);

      //Assert
      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Verify that controller will successfully delete a vehicle if it is found
    /// </summary>
    [Test]
    public void DeleteVehicleTestSuccess()
    {
      var vehicleToDelete = MockRepositoryCollection.Vehicles[0];

      var vehicleRepository = new Mock<IVehicleRepository>();
      vehicleRepository.Setup(x => x.FindId(It.IsAny<VehicleDto>())).Returns(vehicleToDelete.VehicleId);
      vehicleRepository.Setup(x => x.Get(vehicleToDelete.VehicleTypeId)).Returns(vehicleToDelete);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Act

      var result = vehiclesController.DeleteVehicle((int)vehicleToDelete.VehicleId);

      //Assert
      Assert.IsInstanceOf<OkNegotiatedContentResult<VehicleDto>>(result);
    }

    /// <summary>
    /// Test that vehicle records are correctly returned
    /// </summary>
    [Test]
    public void GetVehicleMaintenanceRecordsTest()
    {
      //Arrange
      var itemToRequest = 1;
      var vehicleToReturn = MockRepositoryCollection.Vehicles[itemToRequest - 1];
      var maintenanceTaskList =
        MockRepositoryCollection.MaintenanceRecords?.Where(x => x.VehicleId == itemToRequest).ToList();

      var vehicleRepository = new Mock<IVehicleRepository>();

      vehicleRepository.Setup(x => x.Get(itemToRequest)).Returns(vehicleToReturn);
      vehicleRepository.Setup(x => x.GetMaintenanceRecords(itemToRequest)).Returns(maintenanceTaskList);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.GetVehicleMaintenanceRecords(itemToRequest);

      //Assert

      Assert.AreEqual(maintenanceTaskList.AsQueryable(), result);
    }

    /// <summary>
    /// Verify that vehicle maintenance tasks are correctly returned for the vehicle
    /// </summary>
    [Test]
    public void GetVehicleMaintenanceTasksTest()
    {
      //Arrange
      var itemToRequest = 1;
      var vehicleToReturn = MockRepositoryCollection.Vehicles[itemToRequest - 1];
      var maintenanceTaskList = MockRepositoryCollection.MaintenanceTasks;

      var vehicleRepository = new Mock<IVehicleRepository>();
      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleRepository.Setup(x => x.Get(itemToRequest)).Returns(vehicleToReturn);
      vehicleTypeRepository.Setup(x => x.GetAllMaintenanceTasks(vehicleToReturn.VehicleTypeId))
        .Returns(maintenanceTaskList);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);
      collectionMock.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.GetVehicleMaintenanceTasks(itemToRequest);

      //Assert

      Assert.AreEqual(maintenanceTaskList.AsQueryable(), result);
    }

    /// <summary>
    /// Verify that a object containing all vehicle is retured
    /// </summary>
    [Test]
    public void GetVehiclesTest()
    {
      var repositoryCollection = MockRepositoryCollection.GetMockRepositoryCollection();

      var controller = new VehiclesController(repositoryCollection.Object);

      Assert.AreEqual(3, controller.GetVehicles().Count());
    }

    /// <summary>
    /// Test that the vehicle test fails correctly if it is not found
    /// </summary>
    [Test]
    public void GetVehicleTestFailure()
    {
      //Arrange
      var itemToRequest = 1;
      VehicleDto toReturn = null;

      var vehicleRepository = new Mock<IVehicleRepository>();

      vehicleRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.GetVehicle(itemToRequest);

      //Assert
      Assert.IsInstanceOf<NotFoundResult>(result);
    }

    /// <summary>
    /// Test that getting a vehicle that exists is successful
    /// </summary>
    [Test]
    public void GetVehicleTestSuccess()
    {
      //Arrange
      var itemToRequest = 1;
      var toReturn = MockRepositoryCollection.Vehicles[itemToRequest - 1];

      var vehicleRepository = new Mock<IVehicleRepository>();

      vehicleRepository.Setup(x => x.Get(itemToRequest)).Returns(toReturn);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.GetVehicle(itemToRequest);

      var content = result as OkNegotiatedContentResult<VehicleDto>;

      //Assert

      Assert.AreEqual(toReturn, content.Content);
    }

    /// <summary>
    /// Verify that vehicles that already exist don't succeed
    /// </summary>
    [Test]
    public void PostVehicleFailureIdAlreadyExists()
    {
      var vehicleToAdd = new VehicleDto
                           {
                             VehicleId = null,
                             Make = "Dodge",
                             Model = "Ram 150",
                             Name = "Ram 150",
                             VehicleTypeId = 1,
                             VehicleTypeName = null,
                             Year = 1986
                           };

      var id = 1;

      var vehicleRepository = new Mock<IVehicleRepository>();
      vehicleRepository.Setup(x => x.FindId(It.IsAny<VehicleDto>())).Returns(id);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);
      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Act

      var result = vehiclesController.PostVehicle(vehicleToAdd);

      //Assert
      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that items are successfully added
    /// </summary>
    [Test]
    public void PostVehicleTestSuccess()
    {
      //Arrange

      var vehicleToAdd = new VehicleDto
                           {
                             VehicleId = null,
                             Make = "Dodge",
                             Model = "Ram 150",
                             Name = "Ram 150",
                             VehicleTypeId = 1,
                             VehicleTypeName = null,
                             Year = 1986
                           };

      var id = 0;

      var vehicleRepository = new Mock<IVehicleRepository>();
      vehicleRepository.Setup(x => x.FindId(It.IsAny<VehicleDto>())).Returns(
        delegate
          {
            if (id == 0)
            {
              id++;
              return null;
            }
            return id;
          });

      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();
      vehicleTypeRepository.Setup(x => x.Get(vehicleToAdd.VehicleTypeId))
        .Returns(
          MockRepositoryCollection.VehicleTypes.FirstOrDefault(x => x.VehicleTypeId == vehicleToAdd.VehicleTypeId));

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);
      collectionMock.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Act

      var result = vehiclesController.PostVehicle(vehicleToAdd);

      //Assert
      Assert.IsInstanceOf<CreatedAtRouteNegotiatedContentResult<VehicleDto>>(result);
    }

    /// <summary>
    /// Verify that failure occurs if vehicle id to update and vehicle id in VehicleDto object don't match
    /// </summary>
    [Test]
    public void PutVehicleTestFailureOnVehicleIdsNotMatching()
    {
      //Arrange
      var itemToRequest = 1;

      var vehicleToReturn = MockRepositoryCollection.Vehicles[itemToRequest - 1];
      var vehicleToUpdate = MockRepositoryCollection.Vehicles[itemToRequest - 1];

      var vehicleRepository = new Mock<IVehicleRepository>();
      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleRepository.Setup(x => x.Get(itemToRequest)).Returns(vehicleToReturn);
      vehicleTypeRepository.Setup(x => x.Get(vehicleToReturn.VehicleTypeId));

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.PutVehicle(itemToRequest + 1, vehicleToUpdate);

      //Assert

      Assert.IsInstanceOf<BadRequestResult>(result);
    }

    /// <summary>
    /// Verify that a vehicle that doesn't already exist will return an error
    /// </summary>
    [Test]
    public void PutVehicleTestFailureOnVehicleNotAlreadyPresent()
    {
      //Arrange
      var itemToRequest = 1;

      VehicleDto vehicleToReturn = null;
      var vehicleToUpdate = MockRepositoryCollection.Vehicles[itemToRequest];

      var vehicleRepository = new Mock<IVehicleRepository>();
      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleRepository.Setup(x => x.Get(itemToRequest)).Returns(vehicleToReturn);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.PutVehicle((int)vehicleToUpdate.VehicleId, vehicleToUpdate);

      //Assert

      Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
    }

    /// <summary>
    /// Verify that a vehicle can be update successfully
    /// </summary>
    [Test]
    public void PutVehicleTestSuccess()
    {
      //Arrange
      var itemToRequest = 1;
      var testMake = "test";

      var vehicleToReturn = MockRepositoryCollection.Vehicles[itemToRequest - 1];
      var vehicleToUpdate = MockRepositoryCollection.Vehicles[itemToRequest - 1];
      vehicleToUpdate.Make = testMake;

      var vehicleType =
        MockRepositoryCollection.VehicleTypes.FirstOrDefault(x => x.VehicleTypeId == vehicleToUpdate.VehicleId);

      var vehicleRepository = new Mock<IVehicleRepository>();
      var vehicleTypeRepository = new Mock<IVehicleTypeRepository>();

      vehicleRepository.Setup(x => x.Get(vehicleToReturn.VehicleId ?? 1)).Returns(vehicleToReturn);
      vehicleRepository.Setup(x => x.Update(It.IsAny<VehicleDto>()));
      vehicleTypeRepository.Setup(x => x.Get(vehicleToUpdate.VehicleTypeId)).Returns(vehicleType);

      var collectionMock = MockRepositoryCollection.GetMockRepositoryCollection();
      collectionMock.Setup(x => x.VehicleRepository).Returns(vehicleRepository.Object);
      collectionMock.Setup(x => x.VehicleTypeRepository).Returns(vehicleTypeRepository.Object);

      var vehiclesController = new VehiclesController(collectionMock.Object);

      //Execute
      var result = vehiclesController.PutVehicle((int)vehicleToUpdate.VehicleId, vehicleToUpdate);

      //Assert

      Assert.IsInstanceOf<StatusCodeResult>(result);
    }
  }
}