﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceRecordsControllerShould.cs" company="Stephen de Kok">
//   Copyright 2016 Stephen de Kok
// </copyright>
// <summary>
//   Defines the MaintenanceRecordsControllerShould type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Routes
{
  using System.Net.Http;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.Models;

  using MyTested.WebApi;

  using NUnit.Framework;

  /// <summary>
  /// Defines what the maintenance records controller should do
  /// </summary>
  [TestFixture]
  // ReSharper disable TestClassNameSuffixWarning
  public class MaintenanceRecordsControllerShould
    // ReSharper restore TestClassNameSuffixWarning
  {

    /// <summary>
    /// Setup that needs to be done before any tests are run
    /// </summary>
    [SetUp]
    public void SetupTests()
    {
      AutoMapperConfig.RegisterMappings();
    }

    /// <summary>
    /// Verify that a collection of maintenance records when get is called without a parameter
    /// </summary>
    [Test]
    public void RouteGetWithoutParamtersCorrectly()
    {
      MyWebApi
        .Routes()
        .ShouldMap("api/MaintenanceRecords")
        .WithHttpMethod(HttpMethod.Get)
        .To<MaintenanceRecordsController>(c => c.GetMaintenanceRecords())
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that get with a parameter returns an ok status with the object
    /// </summary>
    [Test]
    public void RouteGetWithParameterCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("api/MaintenanceRecords/1")
        .WithHttpMethod(HttpMethod.Get)
        .To<MaintenanceRecordsController>(x => x.GetMaintenanceRecord(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that post returns the created object and returns a created status
    /// </summary>
    [Test]
    public void RoutePostMessagesCorrectly()
    {
      string json = @"{""VehicleId"": 1, ""Notes"": ""This is bad"", ""Mileage"": 555325, ""Date"": ""1/1/2016 07:20:00"", ""MaintenanceTaskId"": 1}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(MaintenanceRecordDto));
      MaintenanceRecordDto record = result as MaintenanceRecordDto;

      MyWebApi
        .Routes()
        .ShouldMap("api/MaintenanceRecords")
        .WithHttpMethod(HttpMethod.Post)
        .And()
        .WithJsonContent(json)
        .To<MaintenanceRecordsController>(x => x.PostMaintenanceRecord(record))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that when put is called, it returns without an error
    /// </summary>
    [Test]
    public void RoutePutMessagesCorrectly()
    {
      string json = @"{""MaintenanceRecordId"":1,""VehicleId"": 1, ""Notes"": ""This is bad"", ""Mileage"": 555325, ""Date"": ""1/1/2016 07:20:00"", ""MaintenanceTaskId"": 1}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(MaintenanceRecordDto));
      MaintenanceRecordDto record = result as MaintenanceRecordDto;

      MyWebApi
        .Routes()
        .ShouldMap("api/MaintenanceRecords/1")
        .WithHttpMethod(HttpMethod.Put)
        .And()
        .WithJsonContent(json)
        .To<MaintenanceRecordsController>(x => x.PutMaintenanceRecord(1, record))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verity that when get messages are sent, they route correctly
    /// </summary>
    [Test]
    public void RouteDeleteMessagesCorrectly()
    {
      MyWebApi
        .Routes()
        .ShouldMap("api/MaintenanceRecords/1")
        .WithHttpMethod(HttpMethod.Delete)
        .To<MaintenanceRecordsController>(x => x.DeleteMaintenanceRecord(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }
  }
}
