﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehicleTypesControllerShould.cs" company="Stephen de Kok">
//   Copyright 2016 Stephen de Kok
// </copyright>
// <summary>
//   Vehicle Controller routing tests
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AutomobileMaintenance.Tests.Routes
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Net.Http;
  using System.Text;
  using System.Threading.Tasks;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.Models;

  using MyTested.WebApi;

  using NUnit.Framework;

  /// <summary>
  /// Vehicle controller routing tests
  /// </summary>
  [TestFixture]
  // ReSharper disable once TestClassNameSuffixWarning
  public class VehicleTypesControllerShould
  {
    /// <summary>
    /// Verify that get request without parameters is routed correctly
    /// </summary>
    [Test]
    public void RouteGetRequestWithoutParametersCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes")
        .WithHttpMethod(HttpMethod.Get)
        .To<VehicleTypesController>(c => c.GetVehicleTypes())
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that get request with parameters is routed correctly
    /// </summary>
    [Test]
    public void RouteGetRequestWithParametersCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes/1")
        .WithHttpMethod(HttpMethod.Get)
        .To<VehicleTypesController>(c => c.GetVehicleType(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that get maintenance tasks request is routed correctly
    /// </summary>
    [Test]
    public void RouteGetMaintenanceTasksRequestCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes/1/MaintenanceTasks")
        .WithHttpMethod(HttpMethod.Get)
        .To<VehicleTypesController>(c => c.GetMaintenanceTasks(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that post requests are routed correctly
    /// </summary>
    [Test]
    public void RoutePostRequestCorrectly()
    {
      string json =
        @"{""Name"": ""Test Name""}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(VehicleTypeDto));
      VehicleTypeDto vehicleTypeDto = result as VehicleTypeDto;
      

      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes")
        .WithHttpMethod(HttpMethod.Post)
        .WithJsonContent(json)
        .To<VehicleTypesController>(c => c.PostVehicleType(vehicleTypeDto))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that maintenance tasks post requests are routed correctly
    /// </summary>
    [Test]
    public void RoutePostMaintenanceTaskCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes/1/MaintenanceTasks/2")
        .WithHttpMethod(HttpMethod.Post)
        .To<VehicleTypesController>(c => c.PostMaintenanceTask(1, 2))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that put requests are routed correctly
    /// </summary>
    [Test]
    public void RoutePutRequestCorrectly()
    {
      string json =
        @"{""VehicleTypeId"": 1, ""Name"": ""Test Name""}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(VehicleTypeDto));
      VehicleTypeDto vehicleTypeDto = result as VehicleTypeDto;

      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes/1")
        .WithHttpMethod(HttpMethod.Put)
        .WithJsonContent(json)
        .To<VehicleTypesController>(c => c.PutVehicleType(1, vehicleTypeDto))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }
    
    /// <summary>
    /// Verify that delete requests are routed correctly
    /// </summary>
    [Test]
    public void RouteDeleteRequestCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes/1")
        .WithHttpMethod(HttpMethod.Delete)
        .To<VehicleTypesController>(c => c.DeleteVehicleType(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that maintenance tasks delete requests are routed correctly
    /// </summary>
    [Test]
    public void RouteDeleteMaintenanceTaskRequestCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/VehicleTypes/1/MaintenanceTasks/2")
        .WithHttpMethod(HttpMethod.Delete)
        .To<VehicleTypesController>(c => c.DeleteMaintenanceTask(1, 2))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }
  }
}
