﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomobileMaintenance.Tests.Routes
{
  using System.Net.Http;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.Models;

  using MyTested.WebApi;

  using NUnit.Framework;

  /// <summary>
  /// Rout Testing for the maintenance tasks controller
  /// </summary>
  [TestFixture]
  // ReSharper disable once TestClassNameSuffixWarning
  public class MaintenanceTasksControllerShould
  {
    /// <summary>
    /// Verify that get requests are routed correctly
    /// </summary>
    [Test]
    public void RouteGetRequestWithoutParametersCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/MaintenanceTasks")
        .WithHttpMethod(HttpMethod.Get)
        .To<MaintenanceTasksController>(c => c.GetMaintenanceTasks())
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that get requests with parameters are routed correctly
    /// </summary>
    [Test]
    public void RouteGetRequestWithParametersCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/MaintenanceTasks/1")
        .WithHttpMethod(HttpMethod.Get)
        .To<MaintenanceTasksController>(c => c.GetMaintenanceTask(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that post requests are routed correctly
    /// </summary>
    [Test]
    public void RoutePostRequestCorrectly()
    {
      string json = @"{""Description"": ""Test Description"",  ""Name"": ""Test Name""}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(MaintenanceTaskDto));
      MaintenanceTaskDto record = result as MaintenanceTaskDto;

      MyWebApi.Routes()
        .ShouldMap("/api/MaintenanceTasks/")
        .WithHttpMethod(HttpMethod.Post)
        .And()
        .WithJsonContent(json)
        .To<MaintenanceTasksController>(c => c.PostMaintenanceTask(record))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that put requests are routed correctly
    /// </summary>
    [Test]
    public void RoutePutRequestCorrectly()
    {
      string json = @"{""MaintenanceId"": 1, ""Description"": ""Test Description"",  ""Name"": ""Test Name""}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(MaintenanceTaskDto));
      MaintenanceTaskDto record = result as MaintenanceTaskDto;

      MyWebApi.Routes()
        .ShouldMap("/api/MaintenanceTasks/1")
        .WithHttpMethod(HttpMethod.Put)
        .And()
        .WithJsonContent(json)
        .To<MaintenanceTasksController>(c => c.PutMaintenanceTask(1, record))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that delete requests are routed correctly
    /// </summary>
    [Test]
    public void RouteDeleteRequestCorrectly()
    {
      MyWebApi.Routes()
        .ShouldMap("/api/MaintenanceTasks/1")
        .WithHttpMethod(HttpMethod.Delete)
        .To<MaintenanceTasksController>(c => c.DeleteMaintenanceTask(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }
  }
}
