﻿namespace AutomobileMaintenance.Tests.Routes
{
  using System.Net.Http;

  using AutomobileMaintenance.Controllers;
  using AutomobileMaintenance.Models;

  using MyTested.WebApi;

  using NUnit.Framework;

  /// <summary>
  /// Route test for the vehicles controller
  /// </summary>
  [TestFixture]
  // ReSharper disable once TestClassNameSuffixWarning
  public class VehiclesControllerShould
  {
    /// <summary>
    /// Verify that vehicles controller routes get request correctly
    /// </summary>
    [Test]
    public void RouteGetRequestWithoutParametersCorrectly()
    {
      MyWebApi
        .Routes()
        .ShouldMap("/api/Vehicles")
        .WithHttpMethod(HttpMethod.Get)
        .To<VehiclesController>(c => c.GetVehicles())
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify get request with parameters routes correctly
    /// </summary>
    [Test]
    public void RouteGetRequestWithParametersCorrectly()
    {
      MyWebApi
        .Routes()
        .ShouldMap("/api/Vehicles/1")
        .WithHttpMethod(HttpMethod.Get)
        .To<VehiclesController>(c => c.GetVehicle(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify maintenance tasks get request is routed correctly
    /// </summary>
    [Test]
    public void RouteGetMaintenanceTasksCorrectly()
    {
      MyWebApi
        .Routes()
        .ShouldMap("/api/Vehicles/1/MaintenanceTasks")
        .WithHttpMethod(HttpMethod.Get)
        .To<VehiclesController>(c => c.GetVehicleMaintenanceTasks(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify maintenance records get request is routed correctly
    /// </summary>
    [Test]
    public void RouteGetMaintenanceRecordsCorrectly()
    {
      MyWebApi
        .Routes()
        .ShouldMap("/api/Vehicles/1/MaintenanceRecords")
        .WithHttpMethod(HttpMethod.Get)
        .To<VehiclesController>(c => c.GetVehicleMaintenanceRecords(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that post request is routed correctly
    /// </summary>
    [Test]
    public void RoutePostRequestCorrectly()
    {
      string json =
        @"{""VehicleTypeId"": 1,  ""Year"": 2014, ""Make"": ""Test Make"", ""Model"": ""Test Model"", ""Name"": ""Test Name""}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(VehicleDto));
      VehicleDto vehicleDto = result as VehicleDto;

      MyWebApi.Routes()
        .ShouldMap("/api/Vehicles")
        .WithHttpMethod(HttpMethod.Post)
        .WithJsonContent(json)
        .To<VehiclesController>(c => c.PostVehicle(vehicleDto))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify that model state is invalid if <c>json</c> is not complete
    /// </summary>
    [Test]
    public void RoutePostRequestCorrectlyWithInvalidJson()
    {
      string json =
        @"{""VehicleTypeId"": 1,  ""Make"": ""Test Make"", ""Model"": ""Test Model"", ""Name"": ""Test Name""}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(VehicleDto));
      VehicleDto vehicleDto = result as VehicleDto;

      MyWebApi.Routes()
        .ShouldMap("/api/Vehicles")
        .WithHttpMethod(HttpMethod.Post)
        .WithJsonContent(json)
        .To<VehiclesController>(c => c.PostVehicle(vehicleDto))
        .AndAlso()
        .ToInvalidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify put request is routed correctly
    /// </summary>
    [Test]
    public void RoutePutRequestCorrectly()
    {
      string json = @"{""VehicleId"": 1, ""VehicleTypeId"": 1, ""Year"": 2014, ""Make"": ""Test Make"", ""Model"": ""Test Model"", ""Name"": ""Test Name""}";
      dynamic result = System.Web.Helpers.Json.Decode(json, typeof(VehicleDto));
      VehicleDto vehicleDto = result as VehicleDto;

      MyWebApi
        .Routes()
        .ShouldMap("/api/Vehicles/1")
        .WithHttpMethod(HttpMethod.Put)
        .WithJsonContent(json)
        .To<VehiclesController>(c => c.PutVehicle(1, vehicleDto))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }

    /// <summary>
    /// Verify delete request is routed correctly
    /// </summary>
    public void RouteDeleteRequestCorrectly()
    {
      MyWebApi
        .Routes()
        .ShouldMap("/api/Vehicles")
        .WithHttpMethod(HttpMethod.Delete)
        .To<VehiclesController>(c => c.DeleteVehicle(1))
        .AndAlso()
        .ToValidModelState()
        .AndAlso()
        .ToNoHandler();
    }
  }
}
