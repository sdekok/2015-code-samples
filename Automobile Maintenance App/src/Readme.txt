This project requires the following to be installed:
.NET 4.6 or newer (https://www.microsoft.com/en-us/download/details.aspx?id=48130)
Sql Server 2014 LocalDb (https://msdn.microsoft.com/en-us/sqlserver2014express.aspx.  Choose SqlLocalDB from the list of download options after following instructions to log in/sign up)
Visual Studio 2015 or Microsoft Build Tools 2015 (https://www.microsoft.com/en-us/download/details.aspx?id=48159)

To build the solution for testing or to run unit tests against run build.bat or the following commands

msbuild "Automobile Maintenance App.sln" /p:Configuration=Debug

or for a release build:
msbuild "Automobile Maintenance App.sln" /p:Configuration=Release

If build.bat is not succeeding, ensure that the MSBUILDPATH variable in build.bat is pointing to the location of the msbuild that was installed by visual studio or microsoft build tools.

To run the tests:

1. Ensure nunit 3 is installed from http://nunit.org/index.php?p=download.
2. Edit run_nunit_tests.bat and update the console variable with the path to where nunit3-console.exe is installed.
3. Run run_nunit_tests.bat.

The test results will be saved to TestResult.xml.

This can be converted to an html document using tools like NUNIT-HTML-Report-Generator from https://github.com/JatechUK/NUnit-HTML-Report-Generator

To setup in IIS:
1. Create a new website in IIS.
2. Download and extract the contents of Automobile Maintenance App Deployment Package (See Downloads) to the root directory for this new website
3. Restart the website.

Alternatively, for a custom build
1. Create a new website in IIS
2. Copy the following to the root directory for this website:
ApiDemoTests
App_Data
App_Start
bin
bower_components
js
partials
templates
Views
index.html
web.config
Global.asax
3. Start/Restart the website



